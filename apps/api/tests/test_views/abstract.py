from rest_framework.serializers import Serializer
from django.db import models

from ddf import G


class ReadonlyOperationsTestsMixin:
    url_base: str = None
    test_model: models.Model = None
    serializer_class: Serializer = None

    def test_list(self):
        G(self.test_model, n=10)

        result = self.client.get(self.url_base)
        self.assertEqual(result.data, self.serializer_class(
            self.test_model.objects.all(), many=True).data)

    def test_retrieve(self):
        id = 5
        G(self.test_model, id=id)
        G(self.test_model, id=6)

        result = self.client.get(f'{self.url_base}{id}/')

        expected = self.serializer_class(
            self.test_model.objects.filter(id=id).first()).data
        self.assertEqual(result.data, expected)


class AssertUtils:
    def assertResponseEquals(self, url, queryset, serializer_class=None):
        response = self.client.get(url)
        serializer = serializer_class if serializer_class is not None else self.serializer_class
        expected_data = serializer(queryset, many=True).data
        self.assertEqual(response.data, expected_data)
