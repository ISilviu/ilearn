
from http import HTTPStatus
from rest_framework.test import APITestCase
from rest_framework import status

from apps.api.models.discipline import Discipline
from .abstract import ReadonlyOperationsTestsMixin
from ddf import G

from apps.api.models.test import Test, TestQuestion, TestStatistics
from apps.api.models.question import CODE_ANSWER_TYPE_ID, MULTIPLE_ANSWERS_TYPE_ID, SINGLE_ANSWER_TYPE_ID, Question, QuestionDifficulty, QuestionAnswerType
from apps.api.serializers.test import ReadonlyTestSerializer, TestQuestionsCountSerializer
from django.db.models import Count


class ScoreTestsMixin:
    url_base: str = None
    test_model = None
    serializer_class = None

    def test_score_order(self):
        [discipline, question1, question2] = self._add_data(
            SINGLE_ANSWER_TYPE_ID)

        test = G(Test, name='Some test', discipline=discipline)

        G(TestQuestion, test=test, question=question2, order_number=1)
        G(TestQuestion, test=test, question=question1, order_number=2)

        response = self.client.post(
            f'{self.url_base}{test.pk}/score/', data={}, format='json')
        for index, question in enumerate([question2, question1]):
            self.assertEqual(question.content,
                             response.data['questions'][index]['content'])

    def test_score_single_answer(self):
        [discipline, question1, question2] = self._add_data(
            SINGLE_ANSWER_TYPE_ID)

        test = G(Test, name='Some test', discipline=discipline,
                 questions=[question1, question2])

        EXPECTED_RESULT = {
            'correct': 1,
            'wrong': 1,
            'total': 2,
            'score': 50,
            'questions': [
                {
                    'content': question1.content,
                },
                {
                    'content': question2.content,
                },
            ],
        }

        def assert_test_result(answers, expected):
            self._assert_test_result(test.pk, answers, expected)

        assert_test_result({'0': 'B', '1': 'D'}, {
            **EXPECTED_RESULT,
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': False},
                        {'content': 'B', 'correct': False, 'picked': True},
                        {'content': 'C', 'correct': False, 'picked': False}
                    ]
                },
                {
                    'content': question2.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': True},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ]
                },
            ],
        })
        assert_test_result({'0': None, '1': 'D'}, {
            **EXPECTED_RESULT,
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': False},
                        {'content': 'B', 'correct': False, 'picked': False},
                        {'content': 'C', 'correct': False, 'picked': False}
                    ]
                },
                {
                    'content': question2.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': True},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ]
                },
            ],
        })
        assert_test_result({'1': 'D'}, {
            **EXPECTED_RESULT,
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': False},
                        {'content': 'B', 'correct': False, 'picked': False},
                        {'content': 'C', 'correct': False, 'picked': False}
                    ],
                },
                {
                    'content': question2.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': True},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ],
                },
            ],
        })
        assert_test_result({'0': 'A', '1': 'D'}, {
            'correct': 2,
            'wrong': 0,
            'total': 2,
            'score': 100,
            'questions': [question1.content, question2.content],
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': True},
                        {'content': 'B', 'correct': False, 'picked': False},
                        {'content': 'C', 'correct': False, 'picked': False}
                    ],
                },
                {
                    'content': question2.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': True},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ],
                },
            ],
        })
        assert_test_result({}, {
            'correct': 0,
            'wrong': 2,
            'total': 2,
            'score': 0,
            'questions': [question1.content, question2.content],
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': False},
                        {'content': 'B', 'correct': False, 'picked': False},
                        {'content': 'C', 'correct': False, 'picked': False}
                    ],
                },
                {
                    'content': question2.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': False},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ],
                },
            ],
        })

    def test_score_multiple_choices(self):
        difficulty = G(QuestionDifficulty, id=1, difficulty='Expert')
        answer_type = G(QuestionAnswerType,
                        id=MULTIPLE_ANSWERS_TYPE_ID)

        question1 = G(Question, difficulty=difficulty, answer_type=answer_type, answers=[
                      'A', 'B'], possible_answers=['A', 'B', 'C'], content='First question?')

        question2 = G(Question, difficulty=difficulty, answer_type=answer_type, answers=[
                      'D'], possible_answers=['D', 'F', 'G'], content='Second question?')

        test = G(Test, name='Some test',
                 questions=[question1, question2])

        self._assert_test_result(test.pk, {
            '0': ['A', 'B'],
            '1': ['D'],
        }, {
            'correct': 2,
            'wrong': 0,
            'total': 2,
            'score': 100,
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 2,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': True},
                        {'content': 'B', 'correct': True, 'picked': True},
                        {'content': 'C', 'correct': False, 'picked': False}
                    ],
                },
                {
                    'content': question2.content,
                    'difficulty': 1,
                    'answer_type': 2,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': True},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ],
                }
            ],
        })

    def test_score_multiple_answers(self):
        [discipline, question1, _] = self._add_data(MULTIPLE_ANSWERS_TYPE_ID)
        [_, _, question3] = self._add_data(SINGLE_ANSWER_TYPE_ID)

        test = G(Test, name='Some test', discipline=discipline,
                 questions=[question1, question3])

        self._assert_test_result(test.pk, {
            '0': ['A'],
            '1': 'D',
        }, {
            'correct': 2,
            'wrong': 0,
            'total': 2,
            'score': 100,
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 2,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': True},
                        {'content': 'B', 'correct': False, 'picked': False},
                        {'content': 'C', 'correct': False, 'picked': False}
                    ],
                },
                {
                    'content': question3.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': True},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ],
                }
            ],
        })

        self._assert_test_result(test.pk, {
            '0': ['A', 'B'],
            '1': 'D',
        }, {
            'correct': 1,
            'wrong': 1,
            'total': 2,
            'score': 50,
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 2,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': True},
                        {'content': 'B', 'correct': False, 'picked': True},
                        {'content': 'C', 'correct': False, 'picked': False}
                    ],
                },
                {
                    'content':  question3.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': True},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ],
                }
            ],
        })

        self._assert_test_result(test.pk, {
            '0': ['A', 'B'],
            '1': 'C',
        }, {
            'correct': 0,
            'wrong': 2,
            'total': 2,
            'score': 0,
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 2,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': True},
                        {'content': 'B', 'correct': False, 'picked': True},
                        {'content': 'C', 'correct': False, 'picked': False}]
                },
                {
                    'content': question3.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': False},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ],
                }
            ],
        })

        self._assert_test_result(test.pk, {
            '0': [],
            '1': 'C',
        }, {
            'correct': 0,
            'wrong': 2,
            'total': 2,
            'score': 0,
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 2,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': False},
                        {'content': 'B', 'correct': False, 'picked': False},
                        {'content': 'C', 'correct': False, 'picked': False}]
                },
                {
                    'content': question3.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': False},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ],
                }
            ],
        })

        self._assert_test_result(test.pk, {
            '0': 'A',
            '1': 'C',
        }, {
            'correct': 0,
            'wrong': 2,
            'total': 2,
            'score': 0,
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 2,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': False},
                        {'content': 'B', 'correct': False, 'picked': False},
                        {'content': 'C', 'correct': False, 'picked': False}
                    ]
                },
                {
                    'content': question3.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': False},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ],
                }
            ],
        })

        self._assert_test_result(test.pk, {
            '0': None,
            '1': 'C',
        }, {
            'correct': 0,
            'wrong': 2,
            'total': 2,
            'score': 0,
            'questions': [
                {
                    'content': question1.content,
                    'difficulty': 1,
                    'answer_type': 2,
                    'answers': [
                        {'content': 'A', 'correct': True, 'picked': False},
                        {'content': 'B', 'correct': False, 'picked': False},
                        {'content': 'C', 'correct': False, 'picked': False}
                    ]
                },
                {
                    'content': question3.content,
                    'difficulty': 1,
                    'answer_type': 1,
                    'answers': [
                        {'content': 'D', 'correct': True, 'picked': False},
                        {'content': 'F', 'correct': False, 'picked': False},
                        {'content': 'G', 'correct': False, 'picked': False}
                    ]
                },
            ],
        })

    def test_score_code_execution_errors(self):
        difficulty = G(QuestionDifficulty, id=1, difficulty='Expert')
        answer_type = G(QuestionAnswerType,
                        id=CODE_ANSWER_TYPE_ID)

        question1 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='true', possible_answers=["'kayak'"], content='Palindrome test?')
        discipline = G(Discipline, name='OOP')
        test = G(Test, name='Some test', discipline=discipline)
        G(TestQuestion, test=test, question=question1, order_number=1)
        G(TestQuestion, test=test, question=question1, order_number=2)

        response = self.client.post(f'{self.url_base}{test.pk}/score/', data={
            'answers': {
                '0': 'invalid syntax',
                '1': 'def exercise(x):\nreturn x'
            },
            'practice': True,
        }, format='json').data

        self.assertEqual(response['correct'], 0)
        self.assertEqual(response['score'], 0)

    def test_unsafe_operations(self):
        difficulty = G(QuestionDifficulty, id=1, difficulty='Expert')
        answer_type = G(QuestionAnswerType,
                        id=CODE_ANSWER_TYPE_ID)

        question1 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='true', possible_answers=["'kayak'"], content='Palindrome test?')

        test = G(Test, name='Some test')
        G(TestQuestion, test=test, question=question1, order_number=1)

        solution = 'def solution(x):\n\timport os\n\treturn x == x[::-1]'
        response = self.client.post(f'{self.url_base}{test.pk}/score/', data={
            'answers': {
                '0': solution,
            },
            'practice': True,
        }, format='json').data

        self.assertEqual(response['correct'], 0)
        self.assertEqual(response['wrong'], 1)

    def test_score_answers_bool_converted_properly(self):
        difficulty = G(QuestionDifficulty, id=1, difficulty='Expert')
        answer_type = G(QuestionAnswerType,
                        id=CODE_ANSWER_TYPE_ID)

        question1 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='true', possible_answers=["'kayak'"], content='Palindrome test?')
        question2 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='false', possible_answers=["'xyz'"], content='Palindrome test?')
        question3 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='True', possible_answers=["'kayak'"], content='Palindrome test?')
        question4 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='False', possible_answers=["'xyz'"], content='Palindrome test?')
        question5 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='xyz', possible_answers=["'xyz'"], content='Palindrome test?')

        test_boolean = G(Test, name='Some test')

        G(TestQuestion, test=test_boolean, question=question1, order_number=1)
        G(TestQuestion, test=test_boolean, question=question2, order_number=2)
        G(TestQuestion, test=test_boolean, question=question3, order_number=3)
        G(TestQuestion, test=test_boolean, question=question4, order_number=4)
        G(TestQuestion, test=test_boolean, question=question5, order_number=5)

        solution = 'def solution(x):\n\treturn x == x[::-1]'
        response = self.client.post(f'{self.url_base}{test_boolean.pk}/score/', data={
            'answers': {
                '0': solution,
                '1': solution,
                '2': solution,
                '3': solution,
                '4': solution,
            },
            'practice': True,
        }, format='json').data

        self.assertEqual(response['correct'], 4)
        self.assertEqual(response['wrong'], 1)

    def test_score_answers_int_conversion(self):
        difficulty = G(QuestionDifficulty, id=1, difficulty='Expert')
        answer_type = G(QuestionAnswerType,
                        id=CODE_ANSWER_TYPE_ID)

        question1 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='7', possible_answers=['3', '4'], content='Palindrome test?')
        question2 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='abcd', possible_answers=['3', '4'], content='Palindrome test?')
        discipline = G(Discipline, name='OOP')
        test = G(Test, name='Some test', discipline=discipline)
        G(TestQuestion, test=test, question=question1, order_number=1)
        G(TestQuestion, test=test, question=question2, order_number=2)
        solution = 'def solution(x,y):\n\treturn x + y'
        response = self.client.post(f'{self.url_base}{test.pk}/score/', data={
            'answers': {
                '0': solution,
                '1': solution,
            },
            'practice': True,
        }, format='json').data

        self.assertEqual(response['correct'], 1)
        self.assertEqual(response['wrong'], 1)

    def test_score_answers_float_conversion(self):
        difficulty = G(QuestionDifficulty, id=1, difficulty='Expert')
        answer_type = G(QuestionAnswerType,
                        id=CODE_ANSWER_TYPE_ID)

        question1 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='8', possible_answers=['3.4', '4.6'], content='Palindrome test?')
        question2 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='7.4', possible_answers=['3.2', '4.2'], content='Palindrome test?')
        question3 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='abcd', possible_answers=['3.2', '4.2'], content='Palindrome test?')
        discipline = G(Discipline, name='OOP')
        test = G(Test, name='Some test', discipline=discipline)
        G(TestQuestion, test=test, question=question1, order_number=1)
        G(TestQuestion, test=test, question=question2, order_number=2)
        G(TestQuestion, test=test, question=question3, order_number=3)
        solution = 'def solution(x,y):\n\treturn x + y'
        response = self.client.post(f'{self.url_base}{test.pk}/score/', data={
            'answers': {
                '0': solution,
                '1': solution,
                '2': solution,
            },
            'practice': True,
        }, format='json').data

        self.assertEqual(response['correct'], 2)
        self.assertEqual(response['wrong'], 1)

    def test_score_code(self):
        difficulty = G(QuestionDifficulty, id=1, difficulty='Expert')
        answer_type = G(QuestionAnswerType,
                        id=CODE_ANSWER_TYPE_ID)

        question1 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='true', possible_answers=["'kayak'"], content='Palindrome test?')

        question2 = G(Question, difficulty=difficulty, answer_type=answer_type,
                      answers='20', possible_answers=['5', '15'], content='Sum test')

        discipline = G(Discipline, name='OOP')

        test = G(Test, name='Some test', discipline=discipline)

        G(TestQuestion, test=test, question=question2, order_number=1)
        G(TestQuestion, test=test, question=question1, order_number=2)

        answers = {
            '0': 'def solution(x,y):\n  return x+y',
            '1': 'def solution(value):\n  return value==value[::-1]',
        }

        response = self.client.post(f'{self.url_base}{test.pk}/score/', data={
            'answers': answers,
            'practice': True,
        }, format='json').data

        self.assertEqual(response['correct'], 2)

    def test_score_statistics_is_added(self):
        [discipline, question1, question2] = self._add_data(
            SINGLE_ANSWER_TYPE_ID)

        test = G(Test, name='Some test', discipline=discipline)

        G(TestQuestion, test=test, question=question2, order_number=1)
        G(TestQuestion, test=test, question=question1, order_number=2)

        self.client.post(
            f'{self.url_base}{test.pk}/score/', data={
                'practice': False,
                'name': 'Person A',
                'email': 'Email@gmail.com',
            }, format='json')

        test_statistics = TestStatistics.objects.first()
        self.assertEqual(test_statistics.name, 'Person A')
        self.assertEqual(test_statistics.email, 'Email@gmail.com')
        self.assertEqual(test_statistics.score, 0)

        response = self.client.post(
            f'{self.url_base}{test.pk}/score/', data={
                'practice': False,
                'name': 'Person A',
                'email': 'bad_email',
            }, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post(
            f'{self.url_base}{test.pk}/score/', data={
                'practice': False,
                'email': 'x@y.com',
            }, format='json')

        self.assertTrue(TestStatistics.objects.count(), 1)

        response = self.client.post(
            f'{self.url_base}{test.pk}/score/', data={
                'practice': False,
                'name': 'Person A',
            }, format='json')

        self.assertTrue(TestStatistics.objects.count(), 1)


class TestApiTests(ScoreTestsMixin, ReadonlyOperationsTestsMixin, APITestCase):
    url_base = '/api/tests/'
    test_model = Test
    serializer_class = ReadonlyTestSerializer

    def test_list(self):
        G(self.test_model, n=10)

        result = self.client.get(self.url_base)
        expected_result = TestQuestionsCountSerializer(
            self.test_model.objects.annotate(questions_count=Count('questions')).order_by('id'), many=True).data
        self.assertEqual(result.data, expected_result)

    def _add_data(self, answer_type_id):
        difficulty = G(QuestionDifficulty, id=1, difficulty='Expert')
        answer_type = G(QuestionAnswerType,
                        id=answer_type_id)

        question1 = G(Question, difficulty=difficulty, answer_type=answer_type, answers=[
                      'A'], possible_answers=['A', 'B', 'C'], content='First question?')

        question2 = G(Question, difficulty=difficulty, answer_type=answer_type, answers=[
                      'D'], possible_answers=['D', 'F', 'G'], content='Second question?')

        discipline = G(Discipline, name='OOP')

        return [discipline, question1, question2]

    def _assert_test_result(self, test_id, answers, expected_result):
        response = self.client.post(
            f'{self.url_base}{test_id}/score/', data={'answers': answers}, format='json')
        self.assertDictEqual(response.data, expected_result)

    def test_score_test_not_found(self):
        response = self.client.post(f'{self.url_base}30/score/')
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)

    def test_retrieve_test_questions(self):
        [discipline1, question1, question2] = self._add_data(
            SINGLE_ANSWER_TYPE_ID)
        [_, question3, question4] = self._add_data(
            SINGLE_ANSWER_TYPE_ID)

        test = G(Test, discipline=discipline1, name='Test 2022')
        G(TestQuestion, test=test, question=question1, order_number=1)
        G(TestQuestion, test=test, question=question2, order_number=2)
        G(TestQuestion, test=test, question=question3, order_number=4)
        G(TestQuestion, test=test, question=question4, order_number=3)

        response = self.client.get(f'{self.url_base}{test.pk}/questions/')
        self.assertEqual(response.data['count'], 4)
        expected_order = [question1.pk,
                          question2.pk, question4.pk, question3.pk]

        for index, pk in enumerate(expected_order):
            self.assertEqual(pk, response.data['questions'][index]['id'])

        response = self.client.get(
            f'{self.url_base}{test.pk}/questions/?include_answers')
        for item in response.data['questions']:
            self.assertTrue('answers' in item)

        response = self.client.get(
            f'{self.url_base}{test.pk}/questions/?include_answers=true')
        for item in response.data['questions']:
            self.assertTrue('answers' in item)

    def test_retrieve_test_questions_not_found(self):
        response = self.client.get(f'{self.url_base}12/questions/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_add_test(self):
        [discipline, q1, q2] = self._add_data(SINGLE_ANSWER_TYPE_ID)

        test_data = {
            'name': 'My first Django test',
            'discipline': discipline.id,
            'questions': [{'question': q1.id}, {'question': q2.id, 'order_number': 15}]
        }
        response = self.client.post(
            self.url_base, data=test_data, format='json')
        self.assertEqual(response.status_code, HTTPStatus.CREATED)
        self.assertEqual(response.data['id'], 1)
        self.assertEqual(TestQuestion.objects.filter(
            order_number=15).count(), 1)

        response = self.client.post(
            self.url_base, data={**test_data, 'discipline': 5})
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

        response = self.client.post(
            self.url_base, data={**test_data, 'questions': []}, format='json')
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

        response = self.client.post(
            self.url_base, data={**test_data, 'name': ''}, format='json')
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

        del test_data['questions']
        response = self.client.post(
            self.url_base, data=test_data, format='json')
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

    def test_discipline_filtering(self):
        [discipline1, discipline2, discipline3] = G(Discipline, n=3)
        G(Test, name='Discipline 1 test', discipline=discipline1)
        G(Test, name='D2 test', discipline=discipline2)

        response = self.client.get(
            f'{self.url_base}?discipline={discipline1.pk}')
        self.assertEqual(response.data[0]['name'], 'Discipline 1 test')

        response = self.client.get(
            f'{self.url_base}?discipline={discipline2.pk}')
        self.assertEqual(response.data[0]['name'], 'D2 test')

        self.assertEqual(
            len(self.client.get(f'{self.url_base}?discipline={discipline3.pk}').data), 0)

        self.assertEqual(self.client.get(
            f'{self.url_base}?discipline=100').status_code, status.HTTP_400_BAD_REQUEST)

    def test_searching(self):
        G(Test, name='Discipline 1 test')
        G(Test, name='D2 test')

        response = self.client.get(f'{self.url_base}?search=D2')
        self.assertEqual(response.data[0]['name'], 'D2 test')

        response = self.client.get(f'{self.url_base}?search=D')
        self.assertEqual(response.data[0]['name'], 'Discipline 1 test')
        self.assertEqual(response.data[1]['name'], 'D2 test')

        response = self.client.get(f'{self.url_base}?search=Discipline')
        self.assertEqual(response.data[0]['name'], 'Discipline 1 test')

        response = self.client.get(f'{self.url_base}?search=XYZ')
        self.assertEqual(len(response.data), 0)

    def test_ordering(self):
        ordering_fields = ['name', 'questions_count', 'created_at']
        [q1, q2, q3, q4] = G(Question, n=4)
        test1 = G(Test, name='A')
        test2 = G(Test, name='B')
        test3 = G(Test, name='C')

        G(TestQuestion, test=test1, question=q1)
        G(TestQuestion, test=test1, question=q2)

        G(TestQuestion, test=test3, question=q1)
        G(TestQuestion, test=test3, question=q2)
        G(TestQuestion, test=test3, question=q3)

        G(TestQuestion, test=test2, question=q1)
        G(TestQuestion, test=test2, question=q2)
        G(TestQuestion, test=test2, question=q3)
        G(TestQuestion, test=test2, question=q4)

        queryset = Test.objects.annotate(questions_count=Count('questions'))

        for ordering_field in ordering_fields:
            response = self.client.get(
                f'{self.url_base}?ordering={ordering_field}')
            self.assertEqual(response.data, TestQuestionsCountSerializer(
                queryset.order_by(ordering_field), many=True).data)

            response = self.client.get(
                f'{self.url_base}?ordering=-{ordering_field}')
            self.assertEqual(response.data, TestQuestionsCountSerializer(
                queryset.order_by(f'-{ordering_field}'), many=True).data)

    def test_get_orderings(self):
        response = self.client.get(f'{self.url_base}orderings/')
        self.assertEqual(
            response.data, ['created_at', 'name', 'questions_count'])
