from rest_framework.test import APITestCase
from ddf import G

from .abstract import AssertUtils, ReadonlyOperationsTestsMixin

from apps.api.models.discipline import Discipline
from apps.api.serializers.discipline import DisciplineSerializer


class DisciplineApiTests(ReadonlyOperationsTestsMixin, AssertUtils, APITestCase):
    url_base = '/api/disciplines/'
    test_model = Discipline
    serializer_class = DisciplineSerializer

    def test_searching(self):
        G(Discipline, name='OOP')
        G(Discipline, name='Python')
        G(Discipline, name='Python Dynamic Fixtures')

        search = 'Py'
        result = self.client.get(f'/api/disciplines/?search={search}')

        expected = DisciplineSerializer(Discipline.objects.filter(
            name__icontains=search), many=True).data
        self.assertEqual(result.data, expected)
    
    def test_ordering(self):
        G(Discipline, name='OOP', description='A')
        G(Discipline, name='Python', description='B')
        G(Discipline, name='Python Dynamic Fixtures', description='C')

        ordering_fields = self.client.get(f'{self.url_base}orderings/').data
        self.assertEqual(len(ordering_fields), 2)

        for ordering_field in ordering_fields:
            self.assertResponseEquals(
                f'{self.url_base}?ordering={ordering_field}', Discipline.objects.order_by(ordering_field))
            self.assertResponseEquals(
                f'{self.url_base}?ordering=-{ordering_field}', Discipline.objects.order_by(f'-{ordering_field}'))