from rest_framework.test import APITestCase
from rest_framework import status
from .abstract import AssertUtils, ReadonlyOperationsTestsMixin
from ddf import G
from django.db.models import Q

from apps.api.models.question import Question, QuestionDifficulty, QuestionAnswerType
from apps.api.serializers.question import QuestionAnswerTypeSerializer, QuestionDifficultySerializer, QuestionSerializer


class QuestionDifficultyApiTests(ReadonlyOperationsTestsMixin, APITestCase):
    url_base = '/api/difficulties/'
    test_model = QuestionDifficulty
    serializer_class = QuestionDifficultySerializer


class QuestionAnswerTypeApiTests(ReadonlyOperationsTestsMixin, APITestCase):
    url_base = '/api/answer_types/'
    test_model = QuestionAnswerType
    serializer_class = QuestionAnswerTypeSerializer


class QuestionApiTests(ReadonlyOperationsTestsMixin, AssertUtils, APITestCase):
    url_base = '/api/questions/'
    test_model = Question
    serializer_class = QuestionSerializer

    def test_add_questions(self):
        difficulty = G(QuestionDifficulty, difficulty='Expert')
        answer_type = G(QuestionAnswerType, answer_type='Single-answer')
        question = {
            'content': 'How are you?',
            'possible_answers': ['A', 'B'],
            'answers': ['A'],
            'difficulty': difficulty.id,
            'answer_type': answer_type.id,
        }

        def assertAdded(data, expected_id):
            response = self.client.post(
                self.url_base, data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            if isinstance(response.data, list):
                self.assertDictEqual(response.data[0], {
                                     'id': expected_id, **data[0]})
            else:
                self.assertDictEqual(
                    response.data, {'id': expected_id, **data})

        assertAdded(question, 1)
        assertAdded([question], 2)

        question2 = {
            **question,
            'content': 'Another question?'
        }

        response = self.client.post(
            self.url_base, [question, question2], format='json')
        self.assertDictEqual(response.data[0], {'id': 3, **question})
        self.assertDictEqual(response.data[1], {'id': 4, **question2})

    def test_filter_questions(self):
        expert_difficulty = G(QuestionDifficulty, difficulty='Expert')
        novice_difficulty = G(QuestionDifficulty, difficulty='Novice')
        single_answer = G(QuestionAnswerType, answer_type='Single-answer')
        multiple_answers = G(QuestionAnswerType,
                             answer_type='Multiple-answers')

        G(Question, n=10, difficulty=expert_difficulty, answer_type=single_answer)
        G(Question, n=5, difficulty=novice_difficulty, answer_type=multiple_answers)

        for difficulty in [novice_difficulty, expert_difficulty]:
            self.assertResponseEquals(f'{self.url_base}?difficulty={difficulty.pk}', Question.objects.filter(
                difficulty=difficulty.pk))

        for answer_type in [single_answer, multiple_answers]:
            self.assertResponseEquals(f'{self.url_base}?answer_type={answer_type.pk}', Question.objects.filter(
                answer_type=answer_type.pk))

        for difficulty in [novice_difficulty, expert_difficulty]:
            for answer_type in [single_answer, multiple_answers]:
                self.assertResponseEquals(f'{self.url_base}?difficulty={difficulty.pk}&answer_type={answer_type.pk}', Question.objects.filter(
                    answer_type=answer_type.pk, difficulty=difficulty.pk))

    def test_questions_ordering(self):
        [difficulty1, difficulty2, difficulty3] = G(QuestionDifficulty, n=3)
        [answer_type1, answer_type2, answer_type3] = G(QuestionAnswerType, n=3)

        G(Question, content="A", difficulty=difficulty1, answer_type=answer_type1)
        G(Question, content="B", difficulty=difficulty2, answer_type=answer_type2)
        G(Question, content="C", difficulty=difficulty3, answer_type=answer_type3)

        ordering_fields = self.client.get(f'{self.url_base}orderings/').data
        self.assertEqual(len(ordering_fields), 3)

        for ordering_field in ordering_fields:
            self.assertResponseEquals(
                f'{self.url_base}?ordering={ordering_field}', Question.objects.order_by(ordering_field))
            self.assertResponseEquals(
                f'{self.url_base}?ordering=-{ordering_field}', Question.objects.order_by(f'-{ordering_field}'))

    def test_searching(self):
        G(Question, content="A", possible_answers=['Hello', 'Hi there'])
        G(Question, content="B")
        G(Question, content="C")

        for search_phrase in ['Hello', 'Hi there', 'A', 'B', 'C']:
            self.assertResponseEquals(f'{self.url_base}?search={search_phrase}', Question.objects.filter(
                Q(content__icontains=search_phrase) | Q(possible_answers__icontains=search_phrase) | Q(answers__icontains=search_phrase)))

    def test_get_orderings(self):
        response = self.client.get(f'{self.url_base}orderings/')
        self.assertEqual(
            response.data, ['content', 'difficulty', 'answer_type'])
