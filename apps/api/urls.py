from django.urls import include, path
from rest_framework import routers

from .views.question import QuestionDifficultyViewSet, QuestionViewSet, QuestionAnswerTypeViewSet
from .views.test import TestViewSet
from .views.views import UserViewSet, GroupViewSet
from .views.discipline import DisciplineViewSet

app_name = 'apps.api'

router = routers.DefaultRouter()
router.register('users', UserViewSet)
router.register('groups', GroupViewSet)
router.register('disciplines', DisciplineViewSet)
router.register('tests', TestViewSet)
router.register('difficulties', QuestionDifficultyViewSet)
router.register('answer_types', QuestionAnswerTypeViewSet)
router.register('questions', QuestionViewSet)

urlpatterns = [
    path('', include(router.urls)),
]

