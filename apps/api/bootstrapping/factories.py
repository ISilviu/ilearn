import factory
from factory.django import DjangoModelFactory

from ..models.discipline import Discipline

class DisciplineFactory(DjangoModelFactory):
    class Meta:
        model = Discipline
    
    name = factory.Faker('company')
