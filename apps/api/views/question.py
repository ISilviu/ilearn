

from apps.api.views.abstract import OrderingsMixin
from apps.api.views.filters import AnswerTypeFilter, DifficultyFilter
from rest_framework import filters
from ..models.question import Question
from ..models.question_type import QuestionDifficulty
from ..models.question import QuestionAnswerType
from ..serializers.question import QuestionSerializer, QuestionDifficultySerializer, QuestionAnswerTypeSerializer
from rest_framework import viewsets


class QuestionDifficultyViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = QuestionDifficulty.objects.all()
    serializer_class = QuestionDifficultySerializer


class QuestionAnswerTypeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = QuestionAnswerType.objects.all()
    serializer_class = QuestionAnswerTypeSerializer


class QuestionViewSet(OrderingsMixin, viewsets.ModelViewSet):
    queryset = Question.objects.select_related(
        'difficulty').select_related('answer_type')
    serializer_class = QuestionSerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter,
                       DifficultyFilter, AnswerTypeFilter]

    search_fields = ['content', 'possible_answers', 'answers']
    ordering_fields = ['content', 'difficulty', 'answer_type']

    def get_serializer(self, *args, **kwargs):
        if 'data' in kwargs and isinstance(kwargs['data'], list):
            kwargs['many'] = True
        return self.serializer_class(*args, **kwargs)
