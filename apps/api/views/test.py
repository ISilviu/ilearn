import multiprocessing
from operator import getitem
from multiprocessing import Process, managers
from django.forms import ValidationError
from RestrictedPython import compile_restricted, safe_globals, Eval, Guards
from apps.api.models.question import CODE_ANSWER_TYPE_ID, MULTIPLE_ANSWERS_TYPE_ID, SINGLE_ANSWER_TYPE_ID
from apps.api.serializers.question import QuestionSerializer, ReadonlyQuestionSerializer, WithAnswersReadonlyQuestionsSerializer
from apps.api.views.abstract import OrderingsMixin
from apps.api.views.filters import DisciplineFilter
from ..models.test import Test, TestStatistics
from ..serializers.test import ReadonlyTestSerializer, TestQuestionsCountSerializer, TestSerializer
from rest_framework import viewsets, status, filters
from rest_framework.response import Response
from rest_framework.decorators import action
from django.db.models import Count
from django.core.validators import validate_email
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string


class TestViewSet(OrderingsMixin, viewsets.ModelViewSet):
    queryset = Test.objects.select_related(
        'discipline').prefetch_related('questions').order_by('id')
    serializer_class = TestSerializer
    filter_backends = [DisciplineFilter,
                       filters.SearchFilter, filters.OrderingFilter]
    ordering_fields = ['created_at', 'name', 'questions_count']
    search_fields = ['name']

    def list(self, request, *args, **kwargs):
        self.queryset = self.queryset.annotate(
            questions_count=Count('questions'))
        self.get_serializer_class = lambda: TestQuestionsCountSerializer
        return super().list(request, *args, **kwargs)

    def get_serializer_class(self):
        is_read_action = self.action == 'list' or self.action == 'retrieve'
        return ReadonlyTestSerializer if is_read_action else self.serializer_class

    def _evaluate_question(self, question, answer):
        def single_answer_score(answer):
            answers = question['answers']
            def is_correct(
                answer): return answer is not None and answer in answers

            return {
                'score': 1 if is_correct(answer) else 0,
                'question': {
                    'content': question['content'],
                    'difficulty': question['difficulty'],
                    'answer_type': question['answer_type'],
                    'answers': [{
                        'content': current_answer,
                        'correct': is_correct(current_answer),
                        'picked': current_answer == answer
                    } for current_answer in question['possible_answers']],
                },
            }

        def multiple_answers_score(answer):
            answers = question['answers']
            if not isinstance(answer, list):
                return {
                    'score': 0,
                    'question': {
                        'content': question['content'],
                        'difficulty': question['difficulty'],
                        'answer_type': question['answer_type'],
                        'answers': [{
                            'content': current_answer,
                            'correct': current_answer in answers,
                            'picked': False,
                        } for current_answer in question['possible_answers']],
                    },
                }

            answers_length = len(answers)
            intersection = set(answer).intersection(answers)
            is_answer_correct = len(
                answer) == answers_length and len(intersection) == answers_length

            return {
                'score': 1 if is_answer_correct else 0,
                'question': {
                    'content': question['content'],
                    'difficulty': question['difficulty'],
                    'answer_type': question['answer_type'],
                    'answers': [{
                        'content': current_answer,
                        'correct': current_answer in answers,
                        'picked': current_answer in answer,
                    } for current_answer in question['possible_answers']],
                },
            }

        def _check_type(value, type, default=None):
            result = default
            try:
                result = type(value)
            except ValueError:
                pass
            return result

        def _get_expected_result(str_value):
            from distutils.util import strtobool
            if str_value in ['true', 'True', 'false', 'False']:
                return bool(strtobool(str_value))
            elif _check_type(str_value, int):
                return int(str_value)
            elif _check_type(str_value, float):
                return float(str_value)
            else:
                return str_value

        def _run_code(code, globals, return_dict):
            locals = {}
            try:
                exec(code, globals, locals)
                return_dict['result'] = locals.get('result')
                return_dict['has_errors'] = False
            except Exception as e:
                return_dict['has_errors'] = True
                error_message = getattr(e, 'msg', e.args[0])
                return_dict['status'] = f'Error: {error_message}'

        def code_answer_score(answer):
            function_name = 'solution'
            possible_answers = ','.join(map(str, question['possible_answers']))
            result_name = 'result'
            result_str = f'\n{result_name}={function_name}({possible_answers})'
            manager = multiprocessing.Manager()
            return_dict = manager.dict()
            safe_globals['__builtins__']['_getitem_'] = getitem
            safe_globals['__builtins__']['_getiter_'] = Eval.default_guarded_getiter
            safe_globals['__builtins__']['_iter_unpack_sequence_'] = Guards.guarded_iter_unpack_sequence

            byte_code = ''
            try:
                byte_code = compile_restricted(
                    f'{answer}{result_str}', '<string>', 'exec')
            except SyntaxError:
                pass
            action_process = Process(target=_run_code, args=(
                byte_code, safe_globals, return_dict))
            action_process.start()
            action_process.join(timeout=1)

            status = return_dict.get('status', '')
            had_errors = return_dict.get('has_errors', False)

            score = 0
            if not had_errors:
                execution_result = return_dict.get(result_name, None)
                status = 'The code timed out or it did not yield any output.'
                if execution_result is not None:
                    expected_result = _get_expected_result(question['answers'])
                    status = 'The code ran successfully, but the test failed.'
                    if execution_result == expected_result:
                        score = 1
                        status = 'Test succedeed'

            return {
                'score': score,
                'question': {
                    'content': question['content'],
                    'difficulty': question['difficulty'],
                    'answer_type': question['answer_type'],
                    'possible_answers': question['possible_answers'],
                    'answers': question['answers'],
                    'status': status
                },
            }

        question_type_score = {
            SINGLE_ANSWER_TYPE_ID: single_answer_score,
            MULTIPLE_ANSWERS_TYPE_ID: multiple_answers_score,
            CODE_ANSWER_TYPE_ID: code_answer_score
        }

        score_fn = question_type_score[question['answer_type']]
        return score_fn(answer)

    @action(detail=True)
    def questions(self, request, pk):
        test = Test.objects.filter(id=pk).first()
        if test is None:
            return Response({'questions': 'Test not found.'}, status=status.HTTP_404_NOT_FOUND)

        test_questions = test.questions.order_by('testquestion__order_number')
        serializer_class = ReadonlyQuestionSerializer
        if 'include_answers' in request.GET:
            serializer_class = WithAnswersReadonlyQuestionsSerializer

        questions = serializer_class(test_questions, many=True).data
        return Response({
            'count': test_questions.count(),
            'questions': questions,
        }, status=status.HTTP_200_OK)

    @action(detail=True, methods=['post'])
    def score(self, request, pk):
        test = Test.objects.filter(id=pk).first()
        if test is None:
            return Response('Test not found', status=status.HTTP_404_NOT_FOUND)

        test_questions_answers = QuestionSerializer(
            test.questions.order_by('testquestion__order_number'), many=True).data

        score = 0
        questions = []
        answers = request.data.get('answers', None)
        for index, question in enumerate(test_questions_answers):

            result = self._evaluate_question(question,
                                             answers.get(str(index), None) if answers else None)
            score += result['score']
            questions.append(result['question'])

        questions_count = len(test_questions_answers)
        final_score = round(score / questions_count * 100)
        wrong_questions = questions_count - score
        response = {
            'correct': score,
            'wrong': wrong_questions,
            'total': questions_count,
            'score': final_score,
            'questions': questions,
        }

        if not request.data.get('practice', True):
            name = request.data.get('name', None)
            email = request.data.get('email', None)
            if name and email:
                try:
                    validate_email(email)
                except ValidationError:
                    return Response('Invalid email', status=status.HTTP_400_BAD_REQUEST)

                if request.data.get('sendMail', False):
                    html_message = render_to_string(
                        'test_results.html', {
                            'test_name': test.name,
                            'final_score': final_score,
                            'correct_answers': score,
                            'incorrect_answers': wrong_questions,
                        })
                    send_mail(f'{test.name} - Results', f'You have managed to achieve a score of {final_score}%.',
                              settings.EMAIL_HOST_USER, [email], fail_silently=True, html_message=html_message)

                TestStatistics(test=test, name=name,
                               email=email, score=score).save()

        return Response(response, status=status.HTTP_200_OK)
