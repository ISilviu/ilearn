from apps.api.views.abstract import OrderingsMixin
from ..models.discipline import Discipline
from ..serializers.discipline import DisciplineSerializer
from rest_framework import viewsets
from rest_framework import filters


class DisciplineViewSet(OrderingsMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Discipline.objects.all()
    serializer_class = DisciplineSerializer

    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['name']
    ordering_fields = ['name', 'description']
