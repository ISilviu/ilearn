from django_filters.rest_framework import DjangoFilterBackend
from django_filters import rest_framework as filters

from ..models.question_type import QuestionDifficulty
from ..models.question import QuestionAnswerType
from ..models.discipline import Discipline

class DifficultyFilter(DjangoFilterBackend):
    def get_filterset_class(self, view, queryset=None):
        class Filter(filters.FilterSet):
            difficulty = filters.ModelMultipleChoiceFilter(
                queryset = QuestionDifficulty.objects.all()
            )

            class Meta:
                fields = ['difficulty']
        
        return Filter

class AnswerTypeFilter(DjangoFilterBackend):
    def get_filterset_class(self, view, queryset=None):
        class Filter(filters.FilterSet):
            answer_type = filters.ModelMultipleChoiceFilter(
                queryset = QuestionAnswerType.objects.all()
            )

            class Meta:
                fields = ['answer_type']
        
        return Filter

class DisciplineFilter(DjangoFilterBackend):
    def get_filterset_class(self, view, queryset=None):
        class Filter(filters.FilterSet):
            discipline = filters.ModelMultipleChoiceFilter(
                queryset = Discipline.objects.all()
            )

            class Meta:
                fields = ['discipline']
        
        return Filter