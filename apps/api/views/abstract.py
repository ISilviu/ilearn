from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

class OrderingsMixin:
    ordering_fields: list = []

    @action(detail=False)
    def orderings(self, request, **kwargs):
        return Response(self.ordering_fields, status=status.HTTP_200_OK)