from django.db import connection, transaction
from django.core.management.base import BaseCommand

from apps.api.models.discipline import Discipline
from apps.api.models.test import Test, TestQuestion
from apps.api.models.question import CODE_ANSWER_TYPE_ID, MULTIPLE_ANSWERS_TYPE_ID, SINGLE_ANSWER_TYPE_ID, Question, QuestionAnswerType
from apps.api.models.question_type import QuestionDifficulty


DISCIPLINES_COUNT = 50


class DefaultValues:

    @staticmethod
    def get_disciplines():
        disciplines = [
            {
                'name': 'Quantum computing',
                'description': 'Quantum computing is a type of computation that harnesses the collective properties of quantum states, such as superposition, interference, and entanglement, to perform calculations.'
            },
            {
                'name': 'OOP',
                'description': 'Object-oriented programming (OOP) is a computer programming model that organizes software design around data, or objects, rather than functions and logic.',
            },
            {
                'name': 'Procedural programming',
                'description': 'Procedural programming is a programming paradigm built around the idea that programs are sequences of instructions to be executed. They focus heavily on splitting up programs into named sets of instructions called procedures, analogous to functions.',
            },
            {
                'name': 'Graph algorithms',
                'description': 'In mathematics, graph theory is the study of graphs, which are mathematical structures used to model pairwise relations between objects. A graph in this context is made up of vertices (also called nodes or points) which are connected by edges (also called links or lines).',
            },
            {
                'name': 'Functional programming',
                'description': 'In computer science, functional programming is a programming paradigm where programs are constructed by applying and composing functions. This is in contrast with impure procedures, common in imperative programming, which can have side effects (such as modifying the program\'s state or taking input from a user).',
            },
            {
                'name': 'Game development',
                'description': 'Game Development is the art of creating games and describes the design, development and release of a game. It may involve concept generation, design, build, test and release phases.',
            },
            {
                'name': 'Web development',
                'description': 'Web development is the work involved in developing a website for the Internet or an intranet. Web development can range from developing a simple single static page of plain text to complex web applications, electronic businesses, and social network services.',
            },
            {
                'name': 'ReactJS',
                'description': 'React is a free and open-source front-end JavaScript library for building user interfaces based on UI components. It is maintained by Meta and a community of individual developers and companies. React can be used as a base in the development of single-page or mobile applications.',
            },
            {
                'name': 'Angular',
                'description': 'Angular is a TypeScript-based free and open-source web application framework led by the Angular Team at Google and by a community of individuals and corporations. Angular is a complete rewrite from the same team that built AngularJS.',
            },
            {
                'name': 'Cybersecurity',
                'description': 'Cybersecurity is the practice of protecting critical systems and sensitive information from digital attacks. Also known as information technology (IT) security, cybersecurity measures are designed to combat threats against networked systems and applications, whether those threats originate from inside or outside of an organization.',
            },
            {
                'name': 'Cryptography',
                'description': 'Cryptography is the study of secure communications techniques that allow only the sender and intended recipient of a message to view its contents. The term is derived from the Greek word kryptos, which means hidden.',
            },
            {
                'name': 'Artificial Intelligence',
                'description': 'Represents the ability of a digital computer or computer-controlled robot to perform tasks commonly associated with intelligent beings. ',
            },
            {
                'name': 'Data Science',
                'description': 'Data science is an interdisciplinary field that uses scientific methods, processes, algorithms and systems to extract knowledge and insights from noisy, structured and unstructured data, and apply knowledge and actionable insights from data across a broad range of application domains.',
            },
            {
                'name': 'Data Structures',
                'description': 'Data Structures are a specialized means of organizing and storing data in computers in such a way that we can perform operations on the stored data more efficiently. Data structures have a wide and diverse scope of usage across the fields of Computer Science and Software Engineering.',
            },
            {
                'name': 'Algorithms',
                'description': 'In computer systems, an algorithm is basically an instance of logic written in software by software developers, to be effective for the intended "target" computer(s) to produce output from given (perhaps null) input. An optimal algorithm, even running in old hardware, would produce faster results than a non-optimal (higher time complexity) algorithm for the same purpose, running in more efficient hardware; that is why algorithms, like computer hardware, are considered technology.',
            },
            {
                'name': 'Relational databases',
                'description': 'A relational database is a collection of data items with pre-defined relationships between them. These items are organized as a set of tables with columns and rows. Each row in a table could be marked with a unique identifier called a primary key, and rows among multiple tables can be made related using foreign keys.',
            },
            {
                'name': 'Design patterns',
                'description': 'A design pattern is a general repeatable solution to a commonly occurring problem in software design. A design pattern isn\'t a finished design that can be transformed directly into code. It is a description or template for how to solve a problem that can be used in many different situations.',
            },
            {
                'name': 'Mobile application development',
                'description': 'Mobile application development is the process of creating software applications that run on a mobile device, and a typical mobile application utilizes a network connection to work with remote computing resources.',
            },
        ]
        return {
            'values': [Discipline(**{'id': index + 1, **discipline}) for index, discipline in enumerate(disciplines)],
            'model': Discipline
        }

    @staticmethod
    def get_test_questions():
        test_questions = [
            {
                'test_id': 1,
                'question_id': 3,
                'order_number': 1,
            },
            {
                'test_id': 1,
                'question_id': 10,
                'order_number': 2,
            },
            {
                'test_id': 2,
                'question_id': 2,
                'order_number': 1,
            },
            {
                'test_id': 2,
                'question_id': 5,
                'order_number': 2,
            },
            {
                'test_id': 3,
                'question_id': 5,
                'order_number': 1,
            },
            {
                'test_id': 4,
                'question_id': 13,
                'order_number': 1,
            },
            {
                'test_id': 5,
                'question_id': 12,
                'order_number': 1,
            },
            {
                'test_id': 10,
                'question_id': 9,
                'order_number': 1,
            },
            {
                'test_id': 10,
                'question_id': 15,
                'order_number': 2,
            }, {
                'test_id': 7,
                'question_id': 8,
                'order_number': 1,
            }, {
                'test_id': 7,
                'question_id': 6,
                'order_number': 2,
            },
            {
                'test_id': 6,
                'question_id': 2,
                'order_number': 1,
            },
            {
                'test_id': 6,
                'question_id': 5,
                'order_number': 2,
            },
            {
                'test_id': 6,
                'question_id': 14,
                'order_number': 3,
            },
            {
                'test_id': 8,
                'question_id': 7,
                'order_number': 1,
            },
            {
                'test_id': 9,
                'question_id': 9,
                'order_number': 2,
            },
            {
                'test_id': 9,
                'question_id': 11,
                'order_number': 3,
            },
            {
                'test_id': 9,
                'question_id': 15,
                'order_number': 4,
            },
            {
                'test_id': 9,
                'question_id': 4,
                'order_number': 5,
            },
        ]
        return {
            'values': [TestQuestion(**test_question) for test_question in test_questions],
            'model': TestQuestion
        }

    @staticmethod
    def get_tests():
        tests = [
            {
                'name': 'Search problems',
                'discipline': '1',
            },
            {
                'name': 'Classes',
                'discipline': '2',
            },
            {
                'name': 'Inheritance',
                'discipline': '2',
            },
            {
                'name': 'Floyd-Warshall Algorithm',
                "discipline": '4'
            },
            {
                'name': 'Game Development Languges',
                "discipline": '6'
            },
            {
                'name': 'Best practices',
                'discipline': '7',
            },
            {
                'name': 'RxJS',
                "discipline": '9'
            },
            {
                'name': 'Fuzzy Logic',
                "discipline": '12'
            },
            {
                'name': 'Mobile Development Languages',
                "discipline": '18'
            },
            {
                'name': 'Kotlin',
                "discipline": '18'
            }
        ]

        my_tests = [Test(name=test_data['name'], discipline_id=test_data['discipline'])
                    for test_data in tests]
        return {
            'values': my_tests,
            'model': Test
        }

    @staticmethod
    def get_question_difficulties():
        question_difficulties = [
            'Novice', 'Intermediate', 'Advanced', 'Expert']
        return {
            'values': [QuestionDifficulty(id=index+1, difficulty=difficulty) for index, difficulty in enumerate(question_difficulties)],
            'model': QuestionDifficulty
        }

    @staticmethod
    def get_questions():
        questions = [
            {
                'content': 'Is dependency injection supported by default in Angular?',
                'possible_answers': ['Yes', 'No'],
                'answers': ['Yes'],
                'difficulty_id': 1,
                'answer_type_id': SINGLE_ANSWER_TYPE_ID
            },
            {
                'content': 'Are classes useful? Why?',
                'possible_answers': [
                    'Code is more easily manageable with classes.',
                    'Classes are deprecated, it\s recomanded to not use them. Functional programming is better.',
                    'Classes have member variables and functions, can be inherited and reused.'
                ],
                'answers': [
                    'Code is more easily manageable with classes.',
                    'Classes have member variables and functions, can be inherited and reused.'
                ],
                'difficulty_id': 2,
                'answer_type_id': MULTIPLE_ANSWERS_TYPE_ID
            },
            {
                'content': 'Which of the following are used for game development?',
                'possible_answers': ['Djano', 'React', 'Uneral Engine 5', 'Cry Engine', 'Xamarin'],
                'answers': ['Uneral Engine 5', 'Cry Engine'],
                'content': 'What complexity does linear search have?',
                'possible_answers': ['O(n)', 'O(log n)', 'O(n log n)'],
                'answers': ['O(n)'],
                'difficulty_id': 2,
                'answer_type_id': SINGLE_ANSWER_TYPE_ID
            },
            {
                'content': 'Which of the following technologies can be used to create mobile applications?',
                'possible_answers': ['Xamarin', 'Vue', 'WPF', 'React Native', 'Kotlin'],
                'answers': ['Xamarin', 'React Native', 'Kotlin'],
                'difficulty_id': 2,
                'answer_type_id': MULTIPLE_ANSWERS_TYPE_ID
            },
            {
                'content': 'In OOP inheritace is user for ...?',
                'possible_answers': [
                    'Inherit everything from a base class.',
                    'Inheriting private members and functions of the base class.',
                    'Inheriting public and protected members and functions of the base class.',
                    'None.',
                ],
                'answers': [
                    'Inheriting private members and functions of the base class.',
                    'Inheriting public and protected members and functions of the base class.',
                ],
                'difficulty_id': 1,
                'answer_type_id': MULTIPLE_ANSWERS_TYPE_ID,
            },
            {
                'content': 'What is RxJS',
                'possible_answers': [
                    'A python library.',
                    'A library for composing asynchronous and event-based programs by using observable sequences.',
                    'Graphics Engine used for game development.',
                    'A library used for synchronous and event-based programs, only availabe in React',
                ],
                'answers': ['A library for composing asynchronous and event-based programs by using observable sequences.'],
                'difficulty_id': 2,
                'answer_type_id': SINGLE_ANSWER_TYPE_ID
            },
            {
                'content': 'What is Fuzzy Logic?',
                'possible_answers': [
                    'Fuzzy logic is an approach to computing based on "degrees of truth" rather than the usual "true or false" (1 or 0) Boolean logic.',
                    'Fuzzy logic is just an another name for Boolean Logic.',
                    'Fuzzy Logic is used in the automotive industry to calculate the loundess of the engine using complex logic.'
                ],
                'answers': ['Fuzzy logic is an approach to computing based on "degrees of truth" rather than the usual "true or false" (1 or 0) Boolean logic.'],
                'difficulty_id': 2,
                'answer_type_id': SINGLE_ANSWER_TYPE_ID
            },
            {
                'content': 'Which of the following applies for ForkJoin from RxJS?',
                'possible_answers': [
                    'This operator is best used when you have a group of observables and care about all the emitted value of each.',
                    'This operator is best used when you have a group of observables and only care about the final emitted value of each.',
                    'This operator can be used to take an action for every inner observable after it has been finished',
                    'If any of the inner observables supplied to forkJoin error any other observables will continue their process.',
                    'This operator is used only when you want to take action when a response has been received for all observables.',
                    'If any of the inner observables supplied to forkJoin error you will lose the value of any other observables.',
                    'None of the above.'
                ],
                'answers':
                [
                    'This operator is best used when you have a group of observables and only care about the final emitted value of each.',
                    'If any of the inner observables supplied to forkJoin error you will lose the value of any other observables.',
                    'This operator is used only when you want to take action when a response has been received for all observables.',
                ],
                'difficulty_id': 4,
                'answer_type_id': MULTIPLE_ANSWERS_TYPE_ID
            },
            {
                'content': 'Does Kotlin have built-in support for coroutines?',
                'possible_answers': ['Yes', 'No'],
                'answers': ['Yes'],
                'difficulty_id': 1,
                'answer_type_id': SINGLE_ANSWER_TYPE_ID
            },
            {
                'content': 'Write a function that, given a number x, and a list y, will return whether the number could be found in the list.',
                'possible_answers': [2, '[1,2,3,4,5]'],
                'answers': 'True',
                'difficulty_id': 1,
                'answer_type_id': CODE_ANSWER_TYPE_ID
            },
            {
                'content': 'Does Xamarin provide native support to hardware APIs?',
                'possible_answers': ['Yes', 'No'],
                'answers': ['No'],
                'difficulty_id': 3,
                'answer_type_id': SINGLE_ANSWER_TYPE_ID
            },
            {
                'content': 'Which of the following is a game engine responsible for?',
                'possible_answers': ['Handle resources loading', 'Create the game story', 'Handle rendering'],
                'answers': ['Handle resources loading', 'Handle rendering'],
                'difficulty_id': 2,
                'answer_type_id': MULTIPLE_ANSWERS_TYPE_ID
            },
            {
                'content': 'What is the difference between the Floyd-Warshall and Dijkstra algorithms?',
                'possible_answers': ['Floyd-Warshall computes the shortest path between all nodes pairs', 'Dijkstra has a lower memory footprint'],
                'answers': ['Floyd-Warshall computes the shortest path between all nodes pairs'],
                'difficulty_id': 4,
                'answer_type_id': MULTIPLE_ANSWERS_TYPE_ID
            },
            {
                'content': 'What does YAGNI stand for?',
                'possible_answers': ['You Aint Gonna Need It', 'You Are Gonna Need It', 'You Are Gonna Negate It'],
                'answers': ['You Aint Gonna Need It'],
                'difficulty_id': 2,
                'answer_type_id': SINGLE_ANSWER_TYPE_ID
            },
            {
                'content': 'Can Kotlin be used on the backend?',
                'possible_answers': ['Yes, with Ktor', 'No', 'Yes, with kt-api'],
                'answers': ['Yes, with Ktor'],
                'difficulty_id': 2,
                'answer_type_id': SINGLE_ANSWER_TYPE_ID
            },
        ]
        return {
            'values': [Question(**question) for question in questions],
            'model': Question,
        }

    @staticmethod
    def get_question_types():
        question_types = [
            {'id': SINGLE_ANSWER_TYPE_ID, 'answer_type': 'Single-answer'},
            {'id': MULTIPLE_ANSWERS_TYPE_ID, 'answer_type': 'Multiple-answers'},
            {'id': CODE_ANSWER_TYPE_ID, 'answer_type': 'Code'},
        ]
        return {
            'values': [QuestionAnswerType(**answer_type) for answer_type in question_types],
            'model': QuestionAnswerType,
        }

    @staticmethod
    def get_insert_order():
        return [
            DefaultValues.get_disciplines(),
            DefaultValues.get_question_types(),
            DefaultValues.get_question_difficulties(),
            DefaultValues.get_tests(),
            DefaultValues.get_questions(),
            DefaultValues.get_test_questions(),
        ]

    @staticmethod
    def get_delete_order():
        return [TestQuestion, Test, Question, QuestionAnswerType, QuestionDifficulty, Discipline]


class Command(BaseCommand):
    help = 'Boostraps the database with fake data.'

    @transaction.atomic
    def handle(self, *args, **kwargs):
        with transaction.atomic():
            for model in DefaultValues.get_delete_order():
                with connection.cursor() as cursor:
                    self.stdout.write(f'Deleting {model}')
                    cursor.execute(
                        f'TRUNCATE TABLE {model._meta.db_table} RESTART IDENTITY CASCADE;')

            for model_values in DefaultValues.get_insert_order():
                model = model_values['model']

                self.stdout.write(f'Bootstrapping {model}')
                values = model_values['values']
                model.objects.bulk_create(
                    values, batch_size=len(values))

        self.stdout.write('\nSaved new data.')
