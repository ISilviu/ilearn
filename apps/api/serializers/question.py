from rest_framework import serializers
from ..models.question import Question, QuestionAnswerType
from ..models.question_type import QuestionDifficulty


class QuestionDifficultySerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionDifficulty
        fields = ['id', 'difficulty']


class QuestionAnswerTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionAnswerType
        fields = ['id', 'answer_type']


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ['id', 'content', 'possible_answers',
                  'answers', 'difficulty', 'answer_type']


class ReadonlyQuestionSerializer(QuestionSerializer):
    difficulty = QuestionDifficultySerializer(read_only=True)
    answer_type = QuestionAnswerTypeSerializer(read_only=True)

    class Meta:
        model = Question
        fields = ['id', 'content', 'possible_answers',
                  'difficulty', 'answer_type']

class WithAnswersReadonlyQuestionsSerializer(ReadonlyQuestionSerializer):
    class Meta:
        model = Question
        fields = ReadonlyQuestionSerializer.Meta.fields + ['answers']