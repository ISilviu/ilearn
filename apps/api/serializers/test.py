from rest_framework import serializers

from apps.api.serializers.discipline import DisciplineSerializer
from apps.api.serializers.question import QuestionSerializer, ReadonlyQuestionSerializer
from ..models.test import Test, TestQuestion


class TestQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestQuestion
        fields = ['question', 'order_number']


# class ReadonlyTestQuestionSerializer(serializers.ModelSerializer):
#     question = ReadonlyQuestionSerializer(read_only=True)

#     class Meta:
#         model = TestQuestion
#         fields = ['question', 'order_number']


class TestSerializer(serializers.ModelSerializer):
    questions = TestQuestionSerializer(source='testquestion_set', many=True)

    class Meta:
        model = Test
        fields = ['id', 'name', 'discipline', 'questions']

    def validate(self, attrs):
        questions = attrs['testquestion_set']
        if isinstance(questions, list) and not len(questions):
            raise serializers.ValidationError(
                {'questions': 'The test must contain at least one question.'})
        return super().validate(attrs)

    def create(self, validated_data):
        # Nested objects insertion doesn't come out of the box, we need to handle this separately.
        questions = validated_data.pop('testquestion_set', [])
        test = super().create(validated_data)

        for question in questions:
            TestQuestion.objects.create(**{'test': test, **question})

        return test


class TestQuestionsSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(read_only=True, many=True)

    class Meta:
        model = Test
        fields = ['questions']


class ReadonlyTestSerializer(serializers.ModelSerializer):
    discipline = DisciplineSerializer(read_only=True)
    questions = TestQuestionSerializer(source='testquestion_set', many=True, read_only=True)

    class Meta:
        model = Test
        fields = ['id', 'name', 'discipline', 'questions']

class TestQuestionsCountSerializer(serializers.ModelSerializer):
    discipline = DisciplineSerializer(read_only=True)
    questions_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = Test
        fields = ['id', 'name', 'discipline', 'questions_count']