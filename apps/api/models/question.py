from django.db import models

from .question_type import QuestionDifficulty

SINGLE_ANSWER_TYPE_ID = 1
MULTIPLE_ANSWERS_TYPE_ID = 2
CODE_ANSWER_TYPE_ID = 3


class QuestionAnswerType(models.Model):
    answer_type = models.CharField(max_length=50)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.answer_type


class Question(models.Model):
    content = models.CharField(max_length=200)
    possible_answers = models.JSONField(null=True)
    answers = models.JSONField()
    difficulty = models.ForeignKey(
        QuestionDifficulty, on_delete=models.SET_NULL, null=True)
    answer_type = models.ForeignKey(
        QuestionAnswerType, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.content
