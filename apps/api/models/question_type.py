from django.db import models


class QuestionDifficulty(models.Model):
    difficulty = models.CharField(max_length=50)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.difficulty
