from django.db import models
from .discipline import Discipline
from .question import Question


class Test(models.Model):
    name = models.CharField(max_length=50)
    discipline = models.ForeignKey(
        Discipline, on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    questions = models.ManyToManyField(Question, through='TestQuestion')


class TestQuestion(models.Model):
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    order_number = models.IntegerField(null=True)


class TestStatistics(models.Model):
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    email = models.EmailField()
    score = models.IntegerField()
    taken_at = models.DateTimeField(auto_now_add=True, null=True)