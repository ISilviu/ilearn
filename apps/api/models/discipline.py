from django.db import models


class Discipline(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=500, null=True)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.name
