import { useQuery } from "react-query";
import { staleTime, cacheTime } from "./constants";

const useTest = testId => useQuery({
    queryKey: [`/tests/${testId}/`],
    staleTime: staleTime,
    cacheTime: cacheTime
});

const useTests = (queryParams) => useQuery({
    queryKey: ['/tests/', queryParams],
    staleTime: staleTime,
    cacheTime: cacheTime,
});

const useTestQuestions = (testId, includeAnswers = false) => useQuery({
    queryKey: [`/tests/${testId}/questions/${includeAnswers ? '?include_answers' : ''}`],
    staleTime: staleTime,
    cacheTime: cacheTime
});

export { useTests, useTest, useTestQuestions };