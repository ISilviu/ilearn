import { QueryClient } from 'react-query';
import request from './api';

const queryFunction = ({ queryKey }) => {
    const url = queryKey[0];
    const params = queryKey[1];
    return request.get(url, { params });
};

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            queryFn: queryFunction,
        },
    },
});

export default queryClient;