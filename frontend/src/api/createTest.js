import React from 'react';
import { useQuery, useMutation } from "react-query";
import { staleTime, cacheTime } from "./constants";
import mutationFn from './mutation';

const useDisciplines = (queryParams, select = null) => useQuery({
    queryKey: [`/disciplines/`, queryParams],
    staleTime: staleTime,
    cacheTime: cacheTime,
    select: select,
});

const useDifficulties = () => useQuery({
    queryKey: ['/difficulties/'],
    staleTime: staleTime,
    cacheTime: cacheTime
});

const useDifficultiesChoices = () => {
    const { data: difficulties } = useDifficulties();
    return React.useMemo(() => difficulties ? difficulties.data.map(item => ({ id: item.id, text: item.difficulty })) : [], [difficulties]);
}

const useAnswerTypes = (select) => useQuery({
    queryKey: ['/answer_types/'],
    staleTime: staleTime,
    cacheTime: cacheTime,
    select: select,
});

const useAddQuestions = (options) => useMutation(
    questions => mutationFn('/questions/', questions),
    options
);

const useAddTest = (options) => useMutation(
    test => mutationFn('/tests/', test),
    options
);

export { useDisciplines, useDifficultiesChoices, useAnswerTypes, useAddQuestions, useAddTest };
