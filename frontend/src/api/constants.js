export const staleTime = 300000;
export const cacheTime = staleTime;

export const apiRoute = process.env.REACT_APP_API_ROOT;