import axios from 'axios';
import Qs from 'qs';
import { apiRoute } from './constants';

export default axios.create({
    baseURL: apiRoute,
    paramsSerializer: (params) => {
        return Qs.stringify(params, { arrayFormat: 'repeat' });
    }
});