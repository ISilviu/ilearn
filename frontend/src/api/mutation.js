import request from './api';
import { apiRoute } from './constants';

const mutationFn = (key, data) => request.post(`${apiRoute}${key}`, data);

export default mutationFn;