import { renderHook } from "@testing-library/react-hooks";
import fileQuestions from './fileQuestions';
import { useAnswerTypes } from '../createTest'
import { useDifficulties } from '../question';

/* eslint-env jest */

jest.mock('../createTest');
jest.mock('../question');

beforeEach(() => {
    useAnswerTypes.mockReturnValue({
        data: [
            {
                id: 1,
                name: 'Single-answer',
            },
            {
                id: 2,
                name: 'Multiple-answers',
            },
        ],
    });

    useDifficulties.mockReturnValue({
        data: [
            {
                id: 1,
                name: 'Novice',
            },
            {
                id: 2,
                name: 'Intermediate',
            },
            {
                id: 3,
                name: 'Advanced',
            },
            {
                id: 4,
                name: 'Expert',
            },
        ],
    });
});

/* eslint-env jest */

describe('useFileQuestions', () => {

    describe('JSON structure is invalid', () => {

        const expectError = input => {
            const { isError, error } = renderHook(() => fileQuestions.useQuestions(input)).result.current;
            expect(isError).toBeTruthy();
            expect(error).toBe('Malformed data');
        };

        test('missing curly brace', () => {
            const input = "[\n    \n        \"content\": \"Question 1\",\n        \"possible_answers\": [\n            \"A\",\n            \"D\"\n        ],\n        \"answers\": [\n            \"A\"\n        ],\n        \"difficulty\": 1,\n        \"answer_type\": 1\n    },\n    {\n        \"content\": \"Question 2\",\n        \"possible_answers\": [\n            \"A\",\n            \"C\",\n            \"D\"\n        ],\n        \"answers\": [\n            \"A\"\n        ],\n        \"difficulty\": 2,\n        \"answer_type\": 2\n    }\n]";
            expectError(input);
        });

        test('missing comma', () => {
            const input = "[\n    \n    {   \"content\": \"Question 1\"\n        \"possible_answers\": [\n            \"A\",\n            \"D\"\n        ],\n        \"answers\": [\n            \"A\"\n        ],\n        \"difficulty\": 1,\n        \"answer_type\": 1\n    },\n    {\n        \"content\": \"Question 2\",\n        \"possible_answers\": [\n            \"A\",\n            \"C\",\n            \"D\"\n        ],\n        \"answers\": [\n            \"A\"\n        ],\n        \"difficulty\": 2,\n        \"answer_type\": 2\n    }\n]";
            expectError(input);
        });

        test('missing array square brace', () => {
            const input = "\n    {\n        \"content\": \"Question 1\",\n        \"possible_answers\": [\n            \"A\",\n            \"D\"\n        ],\n        \"answers\": [\n            \"A\"\n        ],\n        \"difficulty\": 1,\n        \"answer_type\": 1\n    },\n    {\n        \"content\": \"Question 2\",\n        \"possible_answers\": [\n            \"A\",\n            \"C\",\n            \"D\"\n        ],\n        \"answers\": [\n            \"A\"\n        ],\n        \"difficulty\": 2,\n        \"answer_type\": 2\n    }\n]";
            expectError(input);
        });

        test('missing dots', () => {
            const input = "[\n    {\n        \"content\" \"Question 1\",\n        \"possible_answers\": [\n            \"A\",\n            \"D\"\n        ],\n        \"answers\": [\n            \"A\"\n        ],\n        \"difficulty\": 1,\n        \"answer_type\": 1\n    },\n    {\n        \"content\": \"Question 2\",\n        \"possible_answers\": [\n            \"A\",\n            \"C\",\n            \"D\"\n        ],\n        \"answers\": [\n            \"A\"\n        ],\n        \"difficulty\": 2,\n        \"answer_type\": 2\n    }\n]";
            expectError(input);
        });
    });

    test('input data is properly formatted', () => {
        const input = "[\n    {\n        \"content\": \"Question 1\",\n        \"possible_answers\": [\n            \"A\",\n            \"D\"\n        ],\n        \"answers\": [\n            \"A\"\n        ],\n        \"difficulty\": \"noViCe\",\n        \"answer_type\": 1\n    },\n    {\n        \"content\": \"Question 2\",\n        \"possible_answers\": [\n            \"A\",\n            \"C\",\n            \"D\"\n        ],\n        \"answers\": [\n            \"A\"\n        ],\n        \"difficulty\": \"intermediate\",\n        \"answer_type\": \"Multiple-anSwers\"\n    }\n]";
        const { isError, data } = renderHook(() => fileQuestions.useQuestions(input)).result.current;
        expect(isError).toBeFalsy();

        const expectedResult = [
            {
                "content": "Question 1",
                "possible_answers": [
                    "A",
                    "D"
                ],
                "answers": [
                    "A"
                ],
                "id": 1,
                "difficulty": 1,
                "answer_type": 1
            },
            {
                "content": "Question 2",
                "possible_answers": [
                    "A",
                    "C",
                    "D"
                ],
                "answers": [
                    "A"
                ],
                "id": 2,
                "difficulty": 2,
                "answer_type": 2
            }
        ];

        expect(data.data).toEqual(expectedResult);
    });

    describe('Question structure is invalid', () => {

        const expectError = (input, errorMessage) => {
            const { error, isError } = renderHook(() => fileQuestions.useQuestions(input)).result.current;
            expect(isError).toBeTruthy();
            expect(error).toEqual(errorMessage);
        }

        test('missing', () => {
            const inputs = [
                `
                [
                    {
                        "possible_answers": [
                            "A",
                            "C",
                            "D"
                        ],
                        "answers": [
                            "A"
                        ],
                        "difficulty": 2,
                        "answer_type": 2
                    }
                ]
            `,
                `
                [
                    {
                        "content": "Question 2",
                        "answers": [
                            "A"
                        ],
                        "difficulty": 2,
                        "answer_type": 2
                    }
                ]
            `,
                `
                [
                    {
                        "content": "Question 2",
                        "possible_answers": [
                            "A",
                            "C",
                            "D"
                        ],
                        "difficulty": 2,
                        "answer_type": 2
                    }
                ]
            `
            ];

            inputs.forEach(input => expectError(input, "Question #1's structure is invalid."));
        });

        test('empty question content', () => {
            const input = `
            [
                {
                    "content": "",
                    "possible_answers": [
                        "A",
                        "C",
                        "D"
                    ],
                    "answers": [
                        "A"
                    ],
                    "difficulty": 2,
                    "answer_type": 2
                }
            ]
            `;

            expectError(input, "Question #1's content is empty.");
        });

        test('difficulty out of range', () => {
            const inputs = [
                `
                    [
                        {
                            "content": "How are you?",
                            "possible_answers": [
                                "A",
                                "C",
                                "D"
                            ],
                            "answers": [
                                "A"
                            ],
                            "difficulty": 10,
                            "answer_type": 2
                        }
                    ]
            `,
                `
                [
                    {
                        "content": "Question 2",
                        "possible_answers": [
                            "A",
                            "C",
                            "D"
                        ],
                        "answers": [
                            "A"
                        ],
                        "answer_type": 2
                    }
                ]
            `
            ];
            inputs.forEach(
                input => expectError(input, "Question #1's difficulty is out of range.")
            );
        });

        test('answer type out of range', () => {
            const inputs = [
                `
                    [
                        {
                            "content": "How are you?",
                            "possible_answers": [
                                "A",
                                "C",
                                "D"
                            ],
                            "answers": [
                                "A"
                            ],
                            "difficulty": 1,
                            "answer_type": 5
                        }
                    ]
                `,
                `
                [
                    {
                        "content": "Question 2",
                        "possible_answers": [
                            "A",
                            "C",
                            "D"
                        ],
                        "answers": [
                            "A"
                        ],
                        "difficulty": 2
                    }
                ]
            `
            ];
            inputs.forEach(
                input => expectError(input, "Question #1's answer type is out of range.")
            );
        });

        test('answers contains values not present in possible answers', () => {
            const input = `
            [
                {
                    "content": "How are you?",
                    "possible_answers": [
                        "A",
                        "C",
                        "D"
                    ],
                    "answers": [
                        "A",
                        "XYZ"
                    ],
                    "difficulty": 1,
                    "answer_type": 2
                }
            ]
            `;
            expectError(input, "Question #1's answers contains values outside of the possible answers.");
        });

        test('has at least 2 possible answers', () => {
            const input = `
            [
                {
                    "content": "How are you?",
                    "possible_answers": [
                        "A"
                    ],
                    "answers": [
                        "A"
                    ],
                    "difficulty": 1,
                    "answer_type": 2
                }
            ]
            `;
            expectError(input, "Question #1 needs to have at least 2 possible answers.");
        });

        test('is single answer but has many correct answers', () => {
            const input = `
            [
                {
                    "content": "How are you?",
                    "possible_answers": [
                        "A",
                        "B",
                        "C"
                    ],
                    "answers": [
                        "A",
                        "B"
                    ],
                    "difficulty": 1,
                    "answer_type": 1
                }
            ]
            `;
            expectError(input, "Question #1 is of single answer type, yet it has more than one correct answer.");
        });

        test('Error has correct number', () => {
            const input = `
            [
                {
                    "content": "How are you?",
                    "possible_answers": [
                        "A",
                        "B",
                        "C"
                    ],
                    "answers": [
                        "A"
                    ],
                    "difficulty": 1,
                    "answer_type": 1
                },
                {
                    "content": "How are you?",
                    "possible_answers": [
                        "A",
                        "B",
                        "C"
                    ],
                    "answers": [
                        "A"
                    ],
                    "difficulty": 1,
                    "answer_type": 1
                },
                {
                    "content": "How are you?",
                    "possible_answers": [
                        "A",
                        "B",
                        "C"
                    ],
                    "answers": [
                        "A",
                        "B"
                    ],
                    "difficulty": 1,
                    "answer_type": 1
                }
            ]
            `;

            expectError(input, "Question #3 is of single answer type, yet it has more than one correct answer.");
        });
    });

    describe("Query params", () => {
        const input = `
            [
                {
                    "content": "How are you?",
                    "possible_answers": [
                        "A",
                        "B",
                        "Maybe"
                    ],
                    "answers": [
                        "A"
                    ],
                    "difficulty": 3,
                    "answer_type": 2
                },
                {
                    "content": "What is C++?",
                    "possible_answers": [
                        "A",
                        "B",
                        "C"
                    ],
                    "answers": [
                        "A"
                    ],
                    "difficulty": 1,
                    "answer_type": 1
                },
                {
                    "content": "Is C# garbage-collected?",
                    "possible_answers": [
                        "Yes",
                        "No",
                        "Maybe"
                    ],
                    "answers": [
                        "Yes"
                    ],
                    "difficulty": 1,
                    "answer_type": 1
                }
            ]
            `;

        const question1 = {
            "content": "How are you?",
            "possible_answers": [
                "A",
                "B",
                "Maybe"
            ],
            "answers": [
                "A"
            ],
            "id": 1,
            "difficulty": 3,
            "answer_type": 2
        };

        const question2 = {
            "content": "What is C++?",
            "possible_answers": [
                "A",
                "B",
                "C"
            ],
            "id": 2,
            "answers": [
                "A"
            ],
            "difficulty": 1,
            "answer_type": 1
        };

        const question3 = {
            "content": "Is C# garbage-collected?",
            "possible_answers": [
                "Yes",
                "No",
                "Maybe"
            ],
            "answers": [
                "Yes"
            ],
            "difficulty": 1,
            "id": 3,
            "answer_type": 1
        };

        describe('filtering', () => {
            test('searching', () => {
                const result = renderHook(search => fileQuestions.useQuestions(input, { search: search }));
                result.rerender('C#');
                let { data } = result.result.current;
                expect(data).toEqual({
                    data: [question3],
                });

                result.rerender('What is');
                data = result.result.current.data;
                expect(data).toEqual({
                    data: [question2],
                });

                result.rerender('Maybe');
                data = result.result.current.data;
                expect(data).toEqual({
                    data: [],
                });
            });

            test('filtering by difficulty', () => {
                const result = renderHook(difficulties => fileQuestions.useQuestions(input, { difficulty: difficulties }));
                result.rerender([1]);
                let { data } = result.result.current;
                const difficultyOneResults = [question2, question3];

                expect(data).toEqual({
                    data: difficultyOneResults
                });

                result.rerender([1, 3]);
                data = result.result.current.data;
                expect(data).toEqual({
                    data: [question1, ...difficultyOneResults,],
                });
            });

            test('filtering by answer type', () => {
                const result = renderHook(answerType => fileQuestions.useQuestions(input, { answer_type: answerType }));
                result.rerender([1]);
                let { data } = result.result.current;
                const difficultyOneResults = [question2, question3];

                expect(data).toEqual({
                    data: difficultyOneResults
                });

                result.rerender([1, 2]);
                data = result.result.current.data;
                expect(data).toEqual({
                    data: [question1, ...difficultyOneResults],
                });

                result.rerender([2]);
                data = result.result.current.data;
                expect(data).toEqual({
                    data: [question1],
                });
            });

            test('ordering', () => {
                const result = renderHook(ordering => fileQuestions.useQuestions(input, { ordering: ordering }));

                result.rerender('content');
                let { data } = result.result.current;
                expect(data).toEqual({ data: [question1, question3, question2] });

                result.rerender('-content');
                data = result.result.current.data;
                expect(data).toEqual({ data: [question2, question3, question1] });

                result.rerender('answer_type');
                data = result.result.current.data;
                expect(data).toEqual({ data: [question2, question3, question1] });

                result.rerender('-answer_type');
                data = result.result.current.data;
                expect(data).toEqual({ data: [question1, question2, question3] });

                result.rerender('difficulty');
                data = result.result.current.data;
                expect(data).toEqual({ data: [question2, question3, question1] });

                result.rerender('-difficulty');
                data = result.result.current.data;
                expect(data).toEqual({ data: [question1, question2, question3] });
            });

            test('filtering and ordering', () => {
                const result = renderHook(({ ordering, filters }) => fileQuestions.useQuestions(input, { ordering: ordering, ...filters }));

                result.rerender({ ordering: '-content', filters: { difficulty: [1] } });
                let { data } = result.result.current;
                expect(data).toEqual({ data: [question2, question3] });

                result.rerender({ ordering: 'content', filters: { difficulty: [1] } });
                data = result.result.current.data;
                expect(data).toEqual({ data: [question3, question2] });
            });
        });
    });
});