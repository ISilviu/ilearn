import { parseTextFile } from "./textQuestions";

/* eslint-env jest */

describe('parseTextFile', () => {
    test('file is properly parsed', () => {
        const input = `#
        content: Question 1
        possible_answers: How are you?# ABC# D
        answers: D
        difficulty: novice
        answer_type: single-answer
        #
        content: Question 2
        possible_answers: A# ABC# D# C
        answers: C
        difficulty: novice
        answer_type: single-answer`;

        const parsed = parseTextFile(input);
        expect(parsed).toEqual([
            {
                "content": "Question 1",
                "answers": [
                    "D"
                ],
                "possible_answers": [
                    "How are you?",
                    "ABC",
                    "D"
                ],
                "difficulty": "novice",
                "answer_type": "single-answer"
            },
            {
                "content": "Question 2",
                "answers": [
                    "C"
                ],
                "possible_answers": [
                    "A",
                    "ABC",
                    "D",
                    "C"
                ],
                "difficulty": "novice",
                "answer_type": "single-answer"
            }
        ]);
    });

    test('first # is missing', () => {
        const input = `
    content: Question 1
    possible_answers: How are you?# ABC# D
    answers: D
    difficulty: novice
    answer_type: single-answer
    #
    content: Question 2
    possible_answers: A# ABC# D# C
    answers: C
    difficulty: novice
    answer_type: single-answer`;

        expect(parseTextFile(input)).toEqual([{
            "content": "Question 2",
            "answers": [
                "C"
            ],
            "possible_answers": [
                "A",
                "ABC",
                "D",
                "C"
            ],
            "difficulty": "novice",
            "answer_type": "single-answer"
        }]);
    });

    describe('file does not respect format', () => {

        test('missing first #', () => {
            const input = `content: Question 1
                possible_answers: How are you?, ABC, D
                answers: D
                difficulty: novice
                answer_type: single-answer
                #
                content: Question 2
                possible_answers: A, ABC, D, C
                answers: C
                difficulty: novice
                answer_type: single-answer`;

            expect(parseTextFile(input)).toBeNull();
        });
    });
});