const mapQuestion = inputs => {
    let content, answers, possible_answers, difficulty, answer_type = null;
    inputs.forEach(input => {
        const split = input.split(':');
        switch (split[0]) {
            case 'content':
                content = split[1].trim();
                break;
            case 'possible_answers':
                possible_answers = split[1].split('#').map(answer => answer.trim());
                break;
            case 'answers':
                answers = split[1].split('#').map(answer => answer.trim());
                break;
            case 'difficulty':
                difficulty = split[1].trim();
                break;
            case 'answer_type':
                answer_type = split[1].trim();
                break;
            default:
                break;
        }
    });

    return {
        content,
        answers,
        possible_answers,
        difficulty,
        answer_type,
    };
};

const newQuestionMark = '#';
const linesPerQuestion = 6;

const parseTextFile = input => {
    const lines = input.split('\n');
    const results = [];
    for (let index = 0; index < lines.length; index += linesPerQuestion) {
        const [marker, ...questionLines] = lines.slice(index, index + linesPerQuestion);
        if (marker.trim() === newQuestionMark) {
            const question = mapQuestion(questionLines.map(line => line.trim()));
            if (question) {
                results.push(question);
            }
        }
    }
    return results.length > 0 ? results : null;
};

export { parseTextFile };