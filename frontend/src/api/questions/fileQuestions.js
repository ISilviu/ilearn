import React from 'react';
import { get as getValue, has } from 'lodash';
import { useAnswerTypes } from '../createTest';
import { useDifficulties } from '../question';
import { questionTypes } from '../../components/study/test/question';

const minimumAnswersCount = 2;

export const checkQuestions = (questions, answerTypes, difficulties) => {
    const keys = ['content', 'answer_type', 'answers', 'difficulty', 'possible_answers'];
    let isError = false;
    let errorMessage = '';
    const ids = new Set();
    for (let index = 0; index < questions.length; ++index) {
        const question = questions[index];
        if (!keys.every(key => has(question, key))) {
            isError = true;
            errorMessage = `Question #${index + 1}'s structure is invalid.`;
            break;
        }
        if (question.content.length <= 0) {
            isError = true;
            errorMessage = `Question #${index + 1}'s content is empty.`;
            break;
        }
        if (question.answer_type !== questionTypes.code) {
            if (question.possible_answers.length < minimumAnswersCount) {
                isError = true;
                errorMessage = `Question #${index + 1} needs to have at least ${minimumAnswersCount} possible answers.`;
                break;
            }
            if (!question.answers.every(answer => question.possible_answers.includes(answer))) {
                isError = true;
                errorMessage = `Question #${index + 1}'s answers contains values outside of the possible answers.`;
                break;
            }
        }
        if (answerTypes && !answerTypes.includes(question.answer_type)) {
            isError = true;
            errorMessage = `Question #${index + 1}'s answer type is out of range.`;
            break;
        }
        if (difficulties && !difficulties.includes(question.difficulty)) {
            isError = true;
            errorMessage = `Question #${index + 1}'s difficulty is out of range.`;
            break;
        }
        if (question.id) {
            if (ids.has(question.id)) {
                isError = true;
                errorMessage = `Questions need to have different ids.`;
                break;
            } else {
                ids.add(question.id);
            }
        }
        if (question.answer_type === questionTypes.singleSelection && question.answers.length > 1) {
            isError = true;
            errorMessage = `Question #${index + 1} is of single answer type, yet it has more than one correct answer.`;
            break;
        }
    }

    return [isError, errorMessage];
};

const mapQuestion = (difficulties, answerTypes, question, index) => {
    let difficulty = question.difficulty;
    if (typeof difficulty === 'string') {
        const difficultyId = difficulties?.find(d => d.name.toUpperCase() === difficulty.toUpperCase())?.id;
        if (difficultyId) {
            difficulty = difficultyId;
        }
    }

    let answer_type = question.answer_type;
    if (typeof answer_type === 'string') {
        const answerTypeId = answerTypes?.find(a => a.name.toUpperCase() === answer_type.toUpperCase())?.id;
        if (answerTypeId) {
            answer_type = answerTypeId;
        }
    }

    return {
        ...question,
        difficulty: difficulty,
        answer_type: answer_type,
        id: index + 1,
    };
};

const useFileQuestions = (contents, queryParams) => {
    const answerTypes = useAnswerTypes(data => data.data.map(answerType => ({ id: answerType.id, name: answerType.answer_type })));
    const difficulties = useDifficulties(data => data.data.map(difficulty => ({ id: difficulty.id, name: difficulty.difficulty })));

    const [isError, setIsError] = React.useState(false);
    const [error, setError] = React.useState(null);
    const [parsedData, setParsedData] = React.useState(null);

    React.useEffect(() => {
        try {
            const parsedData = JSON.parse(contents).map((question, index) => mapQuestion(difficulties.data, answerTypes.data, question, index));
            const [isError, errorMessage] = checkQuestions(
                parsedData,
                answerTypes.data?.map(a => a.id),
                difficulties.data?.map(d => d.id)
            );

            if (isError) {
                setIsError(true);
                setError(errorMessage);
            } else {
                setParsedData(parsedData);
            }
        } catch (e) {
            setIsError(true);
        }
    }, [contents, answerTypes.data, difficulties.data]);

    const searchFilterPredicate = question => {
        const search = getValue(queryParams, 'search', null);
        return search ? question.content.includes(search) : true;
    };

    const difficultyFilterPredicate = question => {
        const desiredDifficulties = getValue(queryParams, 'difficulty', null);
        return desiredDifficulties ? desiredDifficulties.includes(question.difficulty) : true;
    };

    const answerTypeFilterPredicate = question => {
        const desiredAnswerTypes = getValue(queryParams, 'answer_type', null);
        return desiredAnswerTypes ? desiredAnswerTypes.includes(question.answer_type) : true;
    };

    const orderByPredicate = (firstQuestion, secondQuestion) => {
        const ordering = getValue(queryParams, 'ordering', null);
        if (ordering) {
            switch (ordering) {
                case 'content':
                    return firstQuestion.content.localeCompare(secondQuestion.content);
                case '-content':
                    return secondQuestion.content.localeCompare(firstQuestion.content);
                case 'answer_type':
                    return firstQuestion.answer_type - secondQuestion.answer_type;
                case '-answer_type':
                    return secondQuestion.answer_type - firstQuestion.answer_type;
                case 'difficulty':
                    return firstQuestion.difficulty - secondQuestion.difficulty;
                case '-difficulty':
                    return secondQuestion.difficulty - firstQuestion.difficulty;
                default:
                    return true;
            }
        }
        return true;
    };

    return {
        isError: isError,
        error: error ?? 'Malformed data',
        isLoading: answerTypes.isLoading || difficulties.isLoading,
        data: {
            data: (parsedData ?? [])
                .filter(searchFilterPredicate)
                .filter(difficultyFilterPredicate)
                .filter(answerTypeFilterPredicate)
                .sort(orderByPredicate),
        },
    };
};

const fileQuestions = {
    useQuestions: useFileQuestions,
};

export default fileQuestions;