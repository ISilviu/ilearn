import { useQuestions } from "../question";

const backendQuestions = {
    useQuestions,
};

export default backendQuestions;