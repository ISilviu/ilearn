import { useMutation } from "react-query";
import mutationFn from "./mutation";

const useTestScore = (testId, options) => useMutation(
    answers => mutationFn(`/tests/${testId}/score/`, answers),
    options
);

export { useTestScore };