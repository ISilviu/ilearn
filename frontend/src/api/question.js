import { useQuery } from "react-query";
import { staleTime, cacheTime } from "./constants";

const useQuestions = (queryParams) => useQuery({
    queryKey: [`/questions/`, queryParams],
    staleTime: staleTime,
    cacheTime: cacheTime,
});

const useDifficulties = (select) => useQuery({
    queryKey: [`/difficulties/`],
    staleTime: staleTime,
    cacheTime: cacheTime,
    select: select,
});

export { useQuestions, useDifficulties };
