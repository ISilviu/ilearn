import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { theme } from '../theme';

const withTheme = (children) => (
    <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
            {children}
        </ThemeProvider>
    </StyledEngineProvider>
);

export { withTheme };