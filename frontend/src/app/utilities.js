const MIN = 1;
const normalise = (value, total) => ((value - MIN) * 100) / (total - MIN);

export { normalise };