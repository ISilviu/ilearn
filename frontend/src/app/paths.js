const paths = {
    getFullPath: innerPath => `${window.location.origin}${innerPath}`,
    pages: {
        home: '/',
        questions: '/questions',
        createTest: '/tests/new',
        exploreTests: '/tests',
        practiceTest: {
            path: '/tests/practice/:testId',
            get: (testId) => `/tests/practice/${testId}`,
        },
        takeTest: {
            path: '/tests/take/:testId',
            get: testId => `/tests/take/${testId}`,
        },
        settings: '/settings',
    }
};

export default paths;