import { render } from '@testing-library/react';
import App from './App';
import { BrowserRouter } from 'react-router-dom';

jest.mock('@uiw/react-textarea-code-editor', () => ({}));
jest.mock('array-shuffle', () => ({}));

test('renders app content', () => {
  render(
    <BrowserRouter>
      <App />
    </BrowserRouter>
  );
});
