import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import StatusSnackbar from '../misc/StatusSnackbar';
import Toolbar from '@mui/material/Toolbar';
import Stack from '@mui/material/Stack';
import MenuIcon from '@mui/icons-material/Menu';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Button from '@mui/material/Button';
import {
    Box,
    Dialog,
    IconButton,
    Link as MuiLink,
    List,
    ListItem,
    ListItemText,
    Menu,
    MenuItem,
    Typography,
    useTheme
} from '@mui/material';
import { Link, useHistory, useLocation } from 'react-router-dom';
import SchoolIcon from '@mui/icons-material/School';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import paths from '../../app/paths';
import { useToggle } from 'react-use';
import { useDesktop, useMobile } from './hooks';
import FileInput from '../generic/FileInput';
import { steps } from '../study/test/CreateTest';
import { checkQuestions } from '../../api/questions/fileQuestions';
import { has } from 'lodash';
import { parseTextFile } from "../../api/questions/textQuestions";

function DropdownButton({ extraAction = () => { } }) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const theme = useTheme();
    const isMobile = useMobile();
    const history = useHistory();
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const [openTestStatus, setOpenTestStatus] = React.useState(null);
    const snackbarPosition = {
        vertical: 'bottom',
        horizontal: 'center',
    };

    return (
        <div>
            <Button
                aria-controls={open ? 'new-test-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}
                endIcon={<ExpandMoreIcon sx={{ color: theme.palette.secondary.contrastText }} />}
            >
                <Typography fontWeight={isMobile ? 400 : 500} color={theme.palette.secondary.contrastText}>New test</Typography>
            </Button>
            <Menu
                id="new-test-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'new-test-button',
                }}
            >
                <MenuItem onClick={() => {
                    history.push(paths.pages.createTest);
                    handleClose();
                    extraAction();
                }}>
                    Create
                </MenuItem>
                <Box component="label">
                    <FileInput
                        onContentRead={(data) => {
                            let fileContents = null;
                            let hasError = false;
                            switch (data.type) {
                                case 'text/plain': {
                                    const testDetails = data.result.slice(0, data.result.indexOf('#')).trim().split('\n');
                                    if (testDetails.length !== 2) {
                                        hasError = true;
                                        setOpenTestStatus({ success: false, message: 'Malformed text file.' });
                                    }

                                    const disciplineId = parseInt(testDetails[0].slice(testDetails[0].indexOf(':') + 1).trim());
                                    const testName = testDetails[1].slice(testDetails[1].indexOf(':') + 1).trim();
                                    const parsedQuestions = parseTextFile(data.result.slice(data.result.indexOf('#')));
                                    fileContents = {
                                        discipline: {
                                            id: disciplineId,
                                        },
                                        testName: testName,
                                        questions: parsedQuestions.map(q => ({
                                            ...q,
                                            answer_type: parseInt(q.answer_type),
                                            answerType: parseInt(q.answer_type),
                                            difficulty: parseInt(q.difficulty)
                                        })),
                                    };

                                    if (fileContents) {
                                        const [isError, errorMessage] = checkQuestions(
                                            fileContents.questions,
                                            [1, 2, 3],
                                            [1, 2, 3, 4]
                                        );

                                        if (isError) {
                                            hasError = true;
                                            setOpenTestStatus({ success: false, message: errorMessage });
                                        } else if (!has(fileContents, 'testName') || fileContents.testName === '') {
                                            hasError = true;
                                            setOpenTestStatus({ success: false, message: 'Missing test name' });
                                        }
                                    }
                                    break;
                                }
                                case 'application/json': {
                                    try {
                                        fileContents = JSON.parse(data.result);
                                    } catch (e) {
                                        setOpenTestStatus({ success: false, message: 'Malformed JSON data.' });
                                    }
                                    if (fileContents) {
                                        const [isError, errorMessage] = checkQuestions(
                                            fileContents.questions.map(q => ({ ...q, answer_type: q.answer_type.id })),
                                            [1, 2, 3],
                                            [1, 2, 3, 4]
                                        );

                                        if (isError) {
                                            hasError = true;
                                            setOpenTestStatus({ success: false, message: errorMessage });
                                        } else if (!has(fileContents, 'testName') || fileContents.testName === '') {
                                            hasError = true;
                                            setOpenTestStatus({ success: false, message: 'Missing test name' });
                                        }
                                    }
                                    break;
                                }
                                default: {
                                    hasError = true;
                                    setOpenTestStatus({ success: false, message: 'Unsupported input type.' });
                                }
                            }
                            if (!hasError) {
                                history.push('/tests/new', {
                                    step: steps.finish.step,
                                    test: fileContents,
                                });
                                extraAction();
                            }
                            handleClose();
                        }}
                    />
                    <MenuItem>
                        Open
                    </MenuItem>
                </Box>
            </Menu>
            <StatusSnackbar
                visible={Boolean(openTestStatus)}
                setInvisible={() => setOpenTestStatus(null)}
                message={openTestStatus?.message ?? ''}
                severity={openTestStatus?.success ? 'success' : 'error'}
                position={snackbarPosition}
            />
        </div>
    );
}

DropdownButton.propTypes = {
    extraAction: PropTypes.func,
};

export default function ILearnAppBar() {
    const theme = useTheme();
    const location = useLocation();
    const isMobile = useMobile();
    const isDesktop = useDesktop();

    const options = React.useMemo(() => [
        {
            name: 'Explore tests',
            to: paths.pages.exploreTests,
        },
    ], []);

    const [mobileDrawerOpen, toggleDrawer] = useToggle(false);

    const history = useHistory();

    const mobileDrawer = (isMobile && mobileDrawerOpen) ? (
        <Dialog fullScreen open={mobileDrawerOpen} onClose={toggleDrawer}>
            <Stack p={1}>
                <Button fullWidth startIcon={<ArrowBackIosIcon fontSize="medium" sx={{ color: theme.palette.secondary.contrastText }} />} onClick={toggleDrawer}>
                    Back
                </Button>
                <List>
                    <Box pl={1.02}>
                        <DropdownButton extraAction={toggleDrawer} />
                    </Box>
                    {options.map((option, index) => (
                        <ListItem key={index}>
                            <MuiLink component={Link} key={index} to={option.to} underline="none">
                                <ListItemText onClick={() => {
                                    history.push(option.to);
                                    toggleDrawer();
                                }}>
                                    <Typography color={theme.palette.secondary.contrastText}>{option.name}</Typography>
                                </ListItemText>
                            </MuiLink>
                        </ListItem>
                    ))}
                </List>
            </Stack>
        </Dialog>
    ) : null;

    const desktopDrawer = isDesktop ? (
        <Stack direction="row" pl={10} flex={1} spacing={5.5}>
            <DropdownButton />
            {options.map((option, index) =>
                <MuiLink component={Link} key={index} to={option.to} underline="none">
                    <Button key={index} sx={{ color: theme.palette.secondary.contrastText }}>
                        <Typography fontWeight={500}>{option.name}</Typography>
                    </Button>
                </MuiLink>
            )}
        </Stack>
    ) : null;

    return (
        <AppBar elevation={location.pathname === paths.pages.home ? 1 : 0} position="fixed" enableColorOnDark sx={{ display: 'flex' }}>
            <Toolbar sx={{ bgcolor: theme.palette.secondary.main }}>
                <MuiLink component={Link} to={paths.pages.home}>
                    <SchoolIcon fontSize="large" sx={{ color: theme.palette.secondary.contrastText, ":hover": { color: theme.palette.primary.main } }} />
                </MuiLink>
                {isMobile ? <Box display="flex" flexGrow={1} /> : null}
                {desktopDrawer}
                {isMobile ? (
                    <>
                        <IconButton disableRipple onClick={toggleDrawer}>
                            <MenuIcon fontSize="medium" sx={{ color: theme.palette.secondary.contrastText }} />
                        </IconButton>
                        {mobileDrawer}
                    </>
                ) : null}
            </Toolbar>
        </AppBar>
    )
}
