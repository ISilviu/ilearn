import { useMediaQuery } from '@mui/material';
import React from 'react';

const useMobile = () => useMediaQuery(theme => theme.breakpoints.down('md'));
const useDesktop = () => useMediaQuery(theme => theme.breakpoints.up('md'));

const useDirection = () => {
    const isMobile = useMobile();
    return React.useMemo(() => isMobile ? 'column' : 'row', [isMobile]);
}

export { useMobile, useDirection, useDesktop };