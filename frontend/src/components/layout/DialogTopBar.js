import React from 'react';
import PropTypes from "prop-types";
import { Box, IconButton, Typography, useTheme } from "@mui/material";

import CloseIcon from '@mui/icons-material/Close';

export default function DialogTopBar({ title, onClose }) {
    const theme = useTheme();

    return (
        <Box
            bgcolor={theme.palette.primary.main}
            height={64}
            display="flex"
            alignItems="center"
            position="fixed"
            width="100%"
            zIndex={theme.zIndex.appBar + 1}
            gap={2}
            flexShrink={1}
        >
            <IconButton
                sx={{ color: theme.palette.primary.contrastText }}
                onClick={onClose}
                size="large"
            >
                <CloseIcon />
            </IconButton>
            <Typography flexGrow={1} variant="h6" color={theme.palette.primary.contrastText}>{title}</Typography>
        </Box>
    );
}

DialogTopBar.propTypes = {
    title: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
};