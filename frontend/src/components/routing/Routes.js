import React from 'react';
import { Route } from 'react-router-dom';
import CreateTest from '../study/test/CreateTest';
import paths from '../../app/paths';
import LandingPage from '../pages/LandingPage';
import PracticeTest from '../study/test/take/PracticeTest';
import Questions from '../study/question/Questions';
import backendQuestions from '../../api/questions/backendQuestions';
import ExploreTests from '../study/test/explore/ExploreTests';
import TakeTest from '../study/test/take/TakeTest';

export default function Routes() {
    return (
        <>
            <Route exact path={paths.pages.home}>
                <LandingPage />
            </Route>
            <Route path={paths.pages.createTest}>
                <CreateTest />
            </Route>
            <Route path={paths.pages.takeTest.path}>
                <TakeTest />
            </Route>
            <Route path={paths.pages.practiceTest.path}>
                <PracticeTest />
            </Route>
            <Route exact path={paths.pages.exploreTests}>
                <ExploreTests />
            </Route>
            <Route path={paths.pages.questions}>
                <Questions questionsApi={backendQuestions} />
            </Route>
        </>
    );
}
