import React from 'react';

import { fireEvent, render } from '@testing-library/react';
import CheckboxGroup from './CheckboxGroup';
import '@testing-library/jest-dom';

/* eslint-env jest */

describe('CheckboxGroup', () => {
    test('renders', () => {
        const checkboxGroup = render(
            <CheckboxGroup checkedValues={[]} choices={['a', 'b', 'c']} onChange={jest.fn()} />
        );

        const choices = checkboxGroup.queryAllByRole('checkbox');
        expect(choices).toHaveLength(3);
    });

    test('uses checked values prop', () => {
        const checkedValues = ['a', 'b'];

        const checkboxGroup = render(
            <CheckboxGroup checkedValues={checkedValues} choices={['a', 'b', 'c']} onChange={jest.fn()} />
        );

        const choices = checkboxGroup.queryAllByRole('checkbox');
        checkedValues.forEach((_, index) => expect(choices[index].checked).toEqual(true));
        expect(choices[2].checked).toEqual(false);
    });

    test('reacts on click events', () => {
        const onChange = jest.fn();
        const choices = ['a', 'b', 'c'];

        const checkboxGroup = render(
            <CheckboxGroup checkedValues={[]} choices={choices} onChange={onChange} />
        );

        choices.forEach(choice => {
            fireEvent.click(checkboxGroup.queryByLabelText(choice));
            expect(onChange).toHaveBeenLastCalledWith([choice]);
        });
    });

});