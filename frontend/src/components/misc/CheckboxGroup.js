import React from 'react';
import { Checkbox, FormControlLabel, FormGroup } from "@mui/material";
import PropTypes from 'prop-types';
import { isEqual } from "lodash";

export default function CheckboxGroup({ checkedValues, choices, onChange, disabled }) {

    const onCheckboxChange = (e, value) => {
        if (e.target.checked) {
            onChange(checkedValues ? checkedValues.concat(value) : [value]);
        } else {
            onChange(checkedValues?.filter(checked => !isEqual(checked, value)));
        }
    };

    const isChecked = React.useCallback(
        choice => checkedValues?.includes(choice) ?? false,
        [checkedValues]
    );

    return (
        <FormGroup>
            {
                choices.map((choice, index) =>
                    <FormControlLabel
                        key={index}
                        control={<Checkbox checked={isChecked(choice)} onChange={e => onCheckboxChange(e, choice)} />}
                        label={choice}
                        disabled={disabled}
                    />
                )
            }
        </FormGroup>
    );
}

CheckboxGroup.propTypes = {
    checkedValues: PropTypes.array,
    choices: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
}