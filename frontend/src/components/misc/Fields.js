import React from 'react';
import PropTypes from 'prop-types';
import { Box, Button, CircularProgress, IconButton, useTheme } from '@mui/material';
import DoneIcon from '@mui/icons-material/Done';

function NavigationButton({ text, onClick, enabled }) {
    const theme = useTheme();
    return (
        <Button
            variant="contained"
            disabled={!enabled}
            sx={{ fontSize: theme.spacing(2.3) }}
            onClick={onClick}
        >
            {text}
        </Button>
    );
}

NavigationButton.propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    enabled: PropTypes.bool.isRequired,
}

function LoadingIndicator({ additionalProps }) {
    return (
        <Box display="flex" justifyContent="center" alignItems="center" height="100%" {...additionalProps}>
            <CircularProgress />
        </Box>
    );
}

LoadingIndicator.propTypes = {
    additionalProps: PropTypes.object,
}

function IconFab({ disabled, onClick, ariaLabel }) {
    const theme = useTheme();
    return (
        <IconButton
            sx={{
                height: theme.spacing(5.5),
                width: theme.spacing(5.5),
                bgcolor: theme.palette.primary.main,
                color: theme.palette.primary.contrastText,
                ":hover": { bgcolor: theme.palette.primary.main, color: theme.palette.primary.contrastText },
                ":disabled": { bgcolor: theme.colors.disabled.bgcolor, color: theme.palette.primary.contrastText }
            }}
            aria-label={ariaLabel}
            disabled={disabled}
            onClick={onClick}
        >
            <DoneIcon />
        </IconButton>
    );
}

IconFab.propTypes = {
    disabled: PropTypes.bool.isRequired,
    ariaLabel: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
};

export { NavigationButton, LoadingIndicator, IconFab };