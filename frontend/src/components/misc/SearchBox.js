import React from 'react';
import PropTypes from 'prop-types';

import { InputAdornment, TextField } from '@mui/material';

import SearchIcon from '@mui/icons-material/Search';

export default function SearchBox({ search, onChange, variant }) {
    return (
        <TextField
            label="Search..."
            value={search}
            variant={variant}
            onChange={(e) => onChange(e.target.value)}
            InputProps={{
                endAdornment: (
                    <InputAdornment position="end">
                        <SearchIcon />
                    </InputAdornment>
                ),
            }}
            inputProps={{
                "aria-label": 'search-input',
            }}
        />
    );
}

SearchBox.propTypes = {
    search: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    variant: PropTypes.string,
};