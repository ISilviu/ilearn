import React from 'react';
import PropTypes from 'prop-types';

import { Alert, Snackbar } from "@mui/material";

function StatusSnackbar({ visible, autoHideDuration = 4000, setInvisible = () => { }, position, severity, message }) {
    return (
        <Snackbar
            open={visible}
            autoHideDuration={autoHideDuration}
            onClose={setInvisible}
            anchorOrigin={position}
            key="status-snackbar"
        >
            <Alert variant="filled" onClose={setInvisible} severity={severity}>
                {message}
            </Alert>
        </Snackbar>
    );
}

StatusSnackbar.propTypes = {
    visible: PropTypes.bool,
    autoHideDuration: PropTypes.bool,
    position: PropTypes.shape({
        vertical: PropTypes.oneOf(['top', 'bottom']),
        horizontal: PropTypes.oneOf(['center', 'left', 'right']),
    }),
    setInvisible: PropTypes.func,
    severity: PropTypes.string,
    message: PropTypes.string,
};

export default StatusSnackbar;