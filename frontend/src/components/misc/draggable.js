const onDragEnd = (result, data, setData) => {
    if (!result.destination) {
        return;
    }

    const questionsCopy = Array.from(data);
    const [reordered] = questionsCopy.splice(result.source.index, 1);
    questionsCopy.splice(result.destination.index, 0, reordered);

    setData(questionsCopy);
};

export { onDragEnd };