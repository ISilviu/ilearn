import { render } from '@testing-library/react';
import "@testing-library/jest-dom/extend-expect";
import { fireEvent } from '@testing-library/react';
import Filters from './Filters';
import { range } from 'lodash';
import { QueryParamProvider } from 'use-query-params';

/* eslint-env jest */

describe('Filters', () => {
    const useFiltersOptions = () => ({
        filters: [
            {
                id: 'filter1',
                isLoading: false,
                data: [
                    {
                        id: '1',
                        name: 'choice1',
                    },
                    {
                        id: '2',
                        name: 'choice2',
                    },
                    {
                        id: '3',
                        name: 'choice3',
                    },
                ],
                title: 'Filter1',
                expanded: true,
            },
            {
                id: 'filter2',
                isLoading: false,
                data: [
                    {
                        id: '1',
                        name: 'choice21',
                    },
                    {
                        id: '2',
                        name: 'choice22',
                    },
                    {
                        id: '3',
                        name: 'choice23',
                    }
                ],
                title: 'Filter2',
                expanded: false,
            },
        ],
        getQueryParams: filters => {
            const queryParams = {};
            const firstFilterIds = filters.filter(f => f.id === 'filter1').map(f => f.choice.id);
            if (firstFilterIds.length) {
                queryParams['filter1'] = firstFilterIds;
            }

            const secondFilterIds = filters.filter(f => f.id === 'filter2').map(f => f.choice.id);
            if (secondFilterIds.length) {
                queryParams['filter2'] = secondFilterIds;
            }

            return queryParams;
        },
        getFilters: () => [],
    });

    test('renders', () => {
        const component = render(
            <QueryParamProvider>
                <Filters useFiltersOptions={useFiltersOptions} onFiltersChange={jest.fn()} />
            </QueryParamProvider>
        );
        expect(component).not.toBeNull();
        expect(component.queryByTestId('SearchIcon')).not.toBeInTheDocument();
    });

    test('renders options, calls onFiltersChange', () => {
        const onFiltersChange = jest.fn();
        const component = render(
            <QueryParamProvider>
                <Filters useFiltersOptions={useFiltersOptions} onFiltersChange={onFiltersChange} />
            </QueryParamProvider>
        );

        const filter1Text = component.queryByText('Filter1');
        expect(filter1Text).toBeInTheDocument();

        ['choice1', 'choice2', 'choice3'].forEach(choice => {
            expect(component.queryByText(choice)).toBeInTheDocument();
        });

        const filter2Text = component.queryByText('Filter2');
        expect(filter2Text).toBeInTheDocument();

        const filter2Button = component.queryByRole('button', { expanded: false, name: 'Filter2' });
        expect(filter2Button).toBeInTheDocument();

        fireEvent.click(filter2Button);
        expect(component.queryByRole('button', { expanded: true, name: 'Filter2' })).toBeInTheDocument();

        ['choice21', 'choice22', 'choice23'].forEach(choice => {
            expect(component.queryByText(choice)).toBeInTheDocument();
        });

        const choice21 = component.queryByRole('checkbox', { name: 'choice21' });
        expect(choice21).not.toBeChecked();
        fireEvent.click(choice21);

        expect(choice21).toBeChecked();
        expect(onFiltersChange).toHaveBeenCalledTimes(2);
        expect(onFiltersChange).toHaveBeenLastCalledWith(expect.objectContaining({
            filter2: ['1'],
        }));
    });

    test('clear filters', () => {
        const onFiltersChange = jest.fn();
        const component = render(
            <QueryParamProvider>
                <Filters useFiltersOptions={useFiltersOptions} onFiltersChange={onFiltersChange} />
            </QueryParamProvider>
        );

        expect(component.queryByRole('button', { name: 'Clear filters' })).not.toBeInTheDocument();

        const choices = ['choice1', 'choice2'];
        choices.forEach(currentChoice => {
            const choice = component.queryByRole('checkbox', { name: currentChoice });
            expect(choice).not.toBeChecked();
            fireEvent.click(choice);
            expect(choice).toBeChecked();
        });

        const clearFiltersButton = component.queryByRole('button', { name: 'Clear filters' });
        expect(clearFiltersButton).toBeInTheDocument();
        fireEvent.click(clearFiltersButton);

        choices.forEach(currentChoice =>
            expect(component.queryByRole('checkbox', { name: currentChoice }))
                .not.toBeChecked()
        );
    });

    describe('search', () => {
        const filtersOptions = useFiltersOptions();
        const newValue = range(9).map(index => ({
            id: `${index}`,
            name: `choice${index}`,
        }));
        filtersOptions.filters[0].data = newValue;

        const useOptions = () => filtersOptions;

        test('renders search', () => {
            const component = render(
                <QueryParamProvider>
                    <Filters useFiltersOptions={useOptions} onFiltersChange={jest.fn()} />
                </QueryParamProvider>
            );
            expect(component.queryByTestId('SearchIcon')).toBeInTheDocument();
        });

        test('search filters items', () => {
            const component = render(
                <QueryParamProvider>
                    <Filters useFiltersOptions={useOptions} onFiltersChange={jest.fn()} />
                </QueryParamProvider>
            );
            const search = component.queryByLabelText('search-input');
            fireEvent.change(search, { target: { value: '8' } });
            expect(search.value).toBe('8');
            expect(component.queryByText('choice8')).toBeInTheDocument();
            range(8).forEach(index =>
                expect(component.queryByText(`choice${index}`)).not.toBeInTheDocument()
            );
        });
    });
});