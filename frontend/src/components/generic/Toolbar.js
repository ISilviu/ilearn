import React from "react";
import { Badge, Box, Dialog, Fab, IconButton, Stack, Tooltip } from "@mui/material";
import { useDirection, useMobile } from "../layout/hooks";
import SearchBox from "../misc/SearchBox";
import OrderingFilter from "./OrderingFilter";
import DialogTopBar from "../layout/DialogTopBar";
import FilterListIcon from '@mui/icons-material/FilterList';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';
import Filters from "./Filters";
import { useTheme } from "@mui/styles";

export default function Toolbar({ filtersProps, searchProps, orderingProps, selectedItemsBadgeProps }) {
    const isMobile = useMobile();
    const direction = useDirection();
    const theme = useTheme();
    const [isFiltersDialogOpen, setFiltersDialogOpen] = React.useState(false);

    const searchBox = searchProps ? <SearchBox {...searchProps} /> : null;
    const orderingFilter = orderingProps ? <OrderingFilter {...orderingProps} /> : null;

    const selectedQuestionsBadge = selectedItemsBadgeProps ? (
        <Fab onClick={selectedItemsBadgeProps.onClick} color="primary">
            <Badge badgeContent={selectedItemsBadgeProps.badgeContent} color="secondary" aria-label="selected questions count">
                <QuestionAnswerIcon color={theme.palette.primary.contrastText} fontSize="large" />
            </Badge>
        </Fab>
    ) : null;

    const filtersDialog = isFiltersDialogOpen ? (
        <Dialog open={isFiltersDialogOpen} fullScreen>
            <Stack>
                <DialogTopBar
                    title="Filters"
                    onClose={() => setFiltersDialogOpen(false)}
                    onSave={() => setFiltersDialogOpen(false)}
                />
                <Box mt={7.5}>
                    <Filters useFiltersOptions={filtersProps.useFiltersOptions} onFiltersChange={filtersProps.onFiltersChange} />
                </Box>
            </Stack>
        </Dialog>
    ) : null;

    const filtersOpenButton = filtersProps ? (
        <Tooltip title="Filters">
            <IconButton color="primary" onClick={() => setFiltersDialogOpen(true)}>
                <FilterListIcon />
            </IconButton>
        </Tooltip>
    ) : null;

    return (
        <Box display="flex" flexDirection={direction} alignItems="center" justifyContent="center" gap={isMobile ? 3 : 2}>
            {isMobile ? (
                <>
                    {filtersDialog}
                    <Box display="flex" alignItems="center" gap={2}>
                        {searchBox}
                        <Box display="flex" flex={1}>
                            {orderingFilter}
                        </Box>
                    </Box>
                    <Box display="flex" alignItems="center" gap={2}>
                        {filtersOpenButton}
                        {selectedQuestionsBadge}
                    </Box>
                </>
            ) : (
                <>
                    {searchBox}
                    {orderingFilter}
                    {selectedQuestionsBadge}
                </>
            )}
        </Box>
    );
}