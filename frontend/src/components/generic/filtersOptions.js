import React from 'react';
import answerTypeFilter from "../../filters/answerTypeFilter";
import difficultyFilter from "../../filters/difficultyFilter";

const useQuestionsFilterOptions = () => {
    const difficultyFilterData = difficultyFilter.useFilter();
    const answerTypeFilterData = answerTypeFilter.useFilter();

    const getQueryParams = React.useCallback(filters => ({
        ...difficultyFilter.getQueryParams(filters),
        ...answerTypeFilter.getQueryParams(filters),
    }), []);

    const getFilters = React.useCallback(
        queryParams =>
            [
                ...difficultyFilter.getFiltersFromQueryParams(queryParams),
                ...answerTypeFilter.getFiltersFromQueryParams(queryParams),
            ],
        []
    );

    return React.useMemo(
        () => ({
            filters: [difficultyFilterData, answerTypeFilterData],
            getQueryParams: getQueryParams,
            getFilters: getFilters,
        }),
        [difficultyFilterData, answerTypeFilterData, getQueryParams, getFilters]
    );
};

export { useQuestionsFilterOptions };