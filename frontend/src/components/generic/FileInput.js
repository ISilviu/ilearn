import React from 'react';
import PropTypes from 'prop-types';

const oneMegabyte = 1048576;

export default function FileInput({ onContentRead, onSizeLimitExceeded, sizeLimit = oneMegabyte, hidden = true }) {
    const readContents = e => {
        const reader = new FileReader();
        let fileType = null;
        reader.onload = e => onContentRead({ result: e.target.result, type: fileType });
        if (e.target.files.length > 0) {
            const size = e.target.files[0].size;
            if (size < sizeLimit) {
                reader.readAsText(e.target.files[0]);
                fileType = e.target.files[0].type;
            } else {
                const sizeInMegabytes = (size / oneMegabyte).toFixed(2);
                const limitInMegabytes = (sizeLimit / oneMegabyte).toFixed(0);

                onSizeLimitExceeded(`The file size is ${sizeInMegabytes}MB, while the maximum supported size is ${limitInMegabytes}MB.`);
            }
        }
        // reset the target value
        e.target.value = null;
    };

    return (
        <input type="file" hidden={hidden} onChange={readContents} />
    );
}

FileInput.propTypes = {
    onContentRead: PropTypes.func.isRequired,
    onSizeLimitExceeded: PropTypes.func,
    sizeLimit: PropTypes.number,
    hidden: PropTypes.bool,
};