import React from 'react';

import { useDebouncedCallback } from 'use-debounce';
import { useTimer } from 'react-timer-hook';
import { useHistory } from 'react-router';
import paths from '../../app/paths';
import { useMap } from 'react-use';
import { steps } from '../study/test/CreateTest';

const useDebouncedState = (initialState, delay = 500) => {
    const [state, setState] = React.useState(initialState);
    const [debouncedState, setDebouncedState] = React.useState(initialState);

    const setDebounced = useDebouncedCallback((value) => setDebouncedState(value), delay);

    const change = React.useCallback(newState => {
        setState(newState);
        setDebounced(newState);
    }, [setDebounced]);

    return [debouncedState, state, change];
};

const useActiveStep = initialStep => {
    const [activeStep, setActiveStep] = React.useState(initialStep);

    const canMovePrevious = React.useCallback(
        () => activeStep > steps.chooseDiscipline.step,
        [activeStep]
    );
    const movePrevious = React.useCallback(() => setActiveStep(activeStep - 1), [activeStep]);

    const canMoveNext = React.useCallback(state => {
        switch (activeStep) {
            case 0: return state.selectedDisciplineId !== null;
            case 1: {
                const hasQuestions = state.questions.length > 0;
                const undeletedQuestions = state.questions.filter((q, index) => !state.isDeleted(index));
                return hasQuestions && undeletedQuestions.length > 0 && !undeletedQuestions.includes(undefined);
            };
            case 2: return state.testName !== '';
            default: return true;
        }
    }, [activeStep]);
    const moveNext = React.useCallback(() => setActiveStep(activeStep + 1), [activeStep]);

    return {
        activeStep,
        canMovePrevious,
        movePrevious,
        canMoveNext,
        moveNext,
    };
};

const useDebouncedRedirect = (addTestStatus, timeout = 15) => {
    const history = useHistory();
    const timer = useTimer({
        expiryTimestamp: timeout,
        onExpire: () => history.push(addTestStatus?.success ? paths.pages.practiceTest.get(addTestStatus.testId) : paths.pages.home),
        autoStart: false
    });

    React.useEffect(() => {
        if (addTestStatus?.success) {
            timer.start();
        }
    }, [timer, addTestStatus]);

    return timer.isRunning;
};

const useServerSideProcessing = () => {
    const [filters, setFilters] = React.useState({});
    const [debouncedSearch, search, changeSearch] = useDebouncedState('');
    const [ordering, setOrdering] = React.useState({});

    const queryParams = React.useMemo(() => ({
        ...filters,
        search: debouncedSearch,
        ...ordering,
    }),
        [debouncedSearch, filters, ordering]
    );

    return [queryParams, { setFilters, setOrdering, changeSearch, search }];
};

const useSelectedItems = () => {
    const [selectedQuestions, { set: addQuestion, remove: removeQuestion }] = useMap({});

    const isSelected = React.useCallback(
        id => id in selectedQuestions,
        [selectedQuestions]
    );

    return [selectedQuestions, { addQuestion, removeQuestion, isSelected }];
};

export { useSelectedItems, useDebouncedState, useActiveStep, useDebouncedRedirect, useServerSideProcessing };