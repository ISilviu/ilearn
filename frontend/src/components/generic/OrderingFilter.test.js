import { render } from '@testing-library/react';
import "@testing-library/jest-dom/extend-expect";
import { fireEvent } from '@testing-library/react';
import OrderingFilter from './OrderingFilter';

/* eslint-env jest */

jest.mock('../../api/question');

describe('Ordering Filter', () => {

    const orderingValues = ['Ordering Option 1', 'Option 2', 'Option 3'];
    const defaultOrdering = orderingValues[0];

    test('renders', () => {
        const component = render(<OrderingFilter values={orderingValues} defaultValue={defaultOrdering}  onOrderingChanged={jest.fn()} />);
        expect(component).not.toBeNull();
    });

    test('renders options, calls onOrderingChanged', () => {
        const onOrderingChanged = jest.fn();
        const component = render(<OrderingFilter values={orderingValues} defaultValue={defaultOrdering} onOrderingChanged={onOrderingChanged} />);

        expect(onOrderingChanged).toHaveBeenCalledTimes(1);
        expect(onOrderingChanged).toHaveBeenCalledWith(expect.objectContaining({
            ordering: 'Ordering Option 1',
        }));

        expect(component).not.toBeNull();

        const select = component.queryByRole('button', { expanded: false });
        expect(select).toBeInTheDocument();

        fireEvent.mouseDown(select);
        expect(component.getByRole('presentation')).toBeInTheDocument();

        const options = component.queryAllByRole('option');
        ['Ordering Option 1', 'Option 2', 'Option 3'].forEach((option, index) => {
            expect(options[index]).toHaveAttribute('data-value', option);
        });

        // Only one option should be checked
        const checkedOption = component.queryByRole('option', { selected: true });
        expect(checkedOption).toBeInTheDocument();
        expect(checkedOption).toHaveAttribute('data-value', 'Ordering Option 1');

        fireEvent.click(options[1]);
        expect(options[1]).toHaveAttribute('aria-selected', 'true');
        expect(onOrderingChanged).toHaveBeenLastCalledWith(expect.objectContaining({
            ordering: 'Option 2',
        }));
    });

    test('changes the direction', () => {
        const onOrderingChanged = jest.fn();
        const component = render(<OrderingFilter values={orderingValues} defaultValue={defaultOrdering} onOrderingChanged={onOrderingChanged} />);

        const directionButton = component.queryByTestId('ArrowUpwardIcon');
        expect(directionButton).toBeInTheDocument();

        expect(onOrderingChanged).toHaveBeenLastCalledWith(expect.objectContaining({
            ordering: 'Ordering Option 1',
        }));

        fireEvent.click(directionButton);

        expect(onOrderingChanged).toHaveBeenLastCalledWith(expect.objectContaining({
            ordering: '-Ordering Option 1',
        }));
    });
});