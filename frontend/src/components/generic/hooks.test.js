import { renderHook } from "@testing-library/react-hooks";
import { act } from "react-dom/test-utils";
import { useActiveStep } from "./hooks";

/* eslint-env jest */

describe('useActiveStep', () => {

    test('uses default value', () => {
        const { activeStep } = renderHook(() => useActiveStep(15)).result.current;
        expect(activeStep).toBe(15);
    });

    test('move previous or next', () => {
        const hooksResult = renderHook(() => useActiveStep(15)).result;
        expect(hooksResult.current.activeStep).toBe(15);

        act(() => hooksResult.current.moveNext());
        expect(hooksResult.current.activeStep).toBe(16);

        act(() => hooksResult.current.movePrevious());
        expect(hooksResult.current.activeStep).toBe(15);
    });

    test('can move previous', () => {
        const hooksResult = renderHook(() => useActiveStep(15)).result;
        expect(hooksResult.current.canMovePrevious()).toBe(true);
        act(() => hooksResult.current.moveNext());
        expect(hooksResult.current.canMovePrevious()).toBe(true);
    });

    test('can move next', () => {
        const hooksResult = renderHook(() => useActiveStep(0)).result;
        expect(hooksResult.current.canMoveNext({ selectedDisciplineId: null })).toBe(false);
        expect(hooksResult.current.canMoveNext({ selectedDisciplineId: 10 })).toBe(true);

        act(() => hooksResult.current.moveNext());

        expect(hooksResult.current.canMoveNext({
            questions: [],
        })).toBe(false);

        expect(hooksResult.current.canMoveNext({
            questions: [1, 2],
            isDeleted: () => false,
        })).toBe(true);

        act(() => hooksResult.current.moveNext());
        expect(hooksResult.current.canMoveNext({
            testName: '',
        })).toBe(false);

        expect(hooksResult.current.canMoveNext({
            testName: 'Some test',
        })).toBe(true);
        act(() => hooksResult.current.moveNext());
        expect(hooksResult.current.activeStep).toBe(3);
    });
});