import React from 'react';
import PropTypes from 'prop-types';

import { Box, IconButton, MenuItem, Select } from '@mui/material';

import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import useOrdering from '../../filters/ordering';

function OrderingFilter({ values, defaultValue, defaultDirection, onOrderingChanged, getDisplayText }) {
    const [ordering, setOrdering, direction,
        changeDirection, getOrderingQueryParams] = useOrdering(defaultValue, defaultDirection);

    // set the default value
    React.useEffect(
        () => setOrdering(defaultValue),
        [defaultValue, setOrdering]
    );

    React.useEffect(
        () => {
            onOrderingChanged(getOrderingQueryParams());
        },
        [direction, ordering, getOrderingQueryParams, onOrderingChanged]
    );

    return (
        <Box display="flex" justifyContent="center" alignItems="center">
            <Select
                value={ordering}
                onChange={e => setOrdering(e.target.value)}
            >
                {values.map(ordering => <MenuItem key={ordering} value={ordering}>{getDisplayText ? getDisplayText(ordering) : ordering}</MenuItem>)}
            </Select>
            <IconButton color="primary" onClick={changeDirection}>
                {direction ? <ArrowUpwardIcon /> : <ArrowDownwardIcon />}
            </IconButton>
        </Box>
    );
}

OrderingFilter.propTypes = {
    values: PropTypes.arrayOf(PropTypes.string).isRequired,
    defaultValue: PropTypes.string,
    onOrderingChanged: PropTypes.func,
    getDisplayText: PropTypes.func,
};

export default OrderingFilter;