import React from 'react';
import {
    Accordion, AccordionDetails, AccordionSummary, Button, Checkbox,
    Chip,
    FormControlLabel,
    Stack, Typography
} from '@mui/material';
import { useQueryParam, ArrayParam, withDefault } from 'use-query-params';
import PropTypes from 'prop-types';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { LoadingIndicator } from '../misc/Fields';
import Scrollbars from 'react-custom-scrollbars-2';
import { Box } from '@mui/system';
import SearchBox from '../misc/SearchBox';
import { get, isEmpty } from 'lodash';

function Filters({ useFiltersOptions, onFiltersChange }) {
    const [queryFilters, setQueryFilters] = useQueryParam('filters', withDefault(ArrayParam, []));
    const { filters, getQueryParams, getFilters } = useFiltersOptions();

    const [selectedFilters, setSelectedFilters] = React.useState(getFilters(queryFilters));
    const [search, setSearch] = React.useState({});

    React.useEffect(() => {
        const queryParams = getQueryParams(selectedFilters);
        if (!isEmpty(queryParams)) {
            let selectedParams = [];
            Object.keys(queryParams).forEach(key => {
                selectedParams.push(...queryParams[key].map(item => `${key}_${item}`));
            });
            setQueryFilters(selectedParams);
        }
        onFiltersChange(queryParams);
    },
        [selectedFilters, onFiltersChange, getQueryParams, setQueryFilters]
    );

    const onChange = React.useCallback((filterId, wasUnchecked, choice) => {
        const newFilters = wasUnchecked ?
            [...selectedFilters, { id: filterId, checked: wasUnchecked, choice }] :
            selectedFilters.filter(f => !(f.id === filterId && f.choice.id === choice.id));

        setSelectedFilters(newFilters);
    },
        [selectedFilters, setSelectedFilters]
    );

    const isChecked = (filterId, choiceId) =>
        selectedFilters.some(f => f.id === filterId && f.choice.id === choiceId);

    const searchBoxThreshold = 7;
    const renderFilter = (filterData, index) => filterData.isLoading ? <LoadingIndicator key={index} /> : (
        <Accordion key={index} defaultExpanded={filterData.expanded}>
            <AccordionSummary expandIcon={<ExpandMoreIcon color="primary" />}>
                <Stack spacing={1}>
                    <Typography>{filterData.title ?? 'Filter'}</Typography>
                    <Box display="flex" flexWrap="wrap" rowGap={0.2}>
                        {filterData.data?.filter(choice => selectedFilters.some(selected => selected.id === filterData.id && selected.choice.id === choice.id)).map((filter, index) => (
                            <Chip key={index} label={filter.name} color="primary" />
                        ))}
                    </Box>
                </Stack>
            </AccordionSummary>
            <AccordionDetails>
                <Stack>
                    <Box p={1.2}>
                        {filterData.data?.length > searchBoxThreshold ? (
                            <SearchBox
                                onChange={searchTerm => {
                                    const newSearchTerm = { ...search };
                                    newSearchTerm[filterData.id] = searchTerm;
                                    setSearch(newSearchTerm);
                                }}
                                variant="standard"
                            />
                        ) : null}
                    </Box>
                    <Scrollbars autoHeight autoHeightMax={400}>
                        <Stack>
                            {filterData.data?.filter(choice => {
                                const searchTerm = get(search, filterData.id, null);
                                return searchTerm ? choice.name.includes(searchTerm) : true;
                            }).map((choice, choiceIndex) => (
                                <FormControlLabel
                                    key={choiceIndex}
                                    control={<Checkbox />}
                                    label={<Typography variant="body2">{choice.name}</Typography>}
                                    checked={isChecked(filterData.id, choice.id)}
                                    onChange={e => onChange(filterData.id, e.target.checked, choice)}
                                />
                            ))}
                        </Stack>
                    </Scrollbars>
                </Stack>
            </AccordionDetails>
        </Accordion>
    );

    const clearFiltersButton = selectedFilters.length ? (
        <Button
            sx={{ mb: 3 }}
            onClick={() => {
                setSelectedFilters([]);
                setQueryFilters([]);
            }}
            variant="contained">
            Clear filters
        </Button>
    ) : null;

    return (
        <Stack p={1.5}>
            {clearFiltersButton}
            {filters.map(renderFilter)}
        </Stack>
    );
}

Filters.propTypes = {
    useFiltersOptions: PropTypes.func.isRequired,
    onFiltersChange: PropTypes.func.isRequired,
};

export default Filters;