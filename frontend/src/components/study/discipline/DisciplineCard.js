import React from 'react';
import PropTypes from 'prop-types';

import { Box, Card, Typography, useTheme } from '@mui/material';

import ScienceIcon from '@mui/icons-material/Science';
import Scrollbars from 'react-custom-scrollbars-2';

export default function DisciplineCard({ discipline, isSelected, setSelectedDiscipline }) {
    const theme = useTheme();
    return (
        <Card
            aria-checked={isSelected}
            role="option"
            key={discipline.id}
            onClick={() => setSelectedDiscipline(isSelected ? null : {
                id: discipline.id,
                name: discipline.name,
            })}
            raised={isSelected}
            sx={{
                p: 1,
                display: 'flex',
                flexDirection: 'column',
                overflow: 'auto',
                bgcolor: isSelected ? theme.colors.selectedCard : theme.palette.primary.main,
                borderRadius: theme.radius.button,
                width: 300,
                height: 200,
            }}
        >
            <Scrollbars autoHide>
                <Box display="flex">
                    <ScienceIcon sx={{ color: theme.palette.primary.contrastText, fontSize: 80 }} />
                    <Typography alignSelf="center" justifyContent="center" px={1} color={theme.palette.primary.contrastText}>{discipline.name}</Typography>
                </Box>
                <Typography color={theme.palette.primary.contrastText}>
                    {discipline.description}
                </Typography>
            </Scrollbars>
        </Card>
    );
}

DisciplineCard.propTypes = {
    discipline: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string,
    }),
    isSelected: PropTypes.bool.isRequired,
    setSelectedDiscipline: PropTypes.func.isRequired,
}