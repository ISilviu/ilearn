import React from 'react';
import PropTypes from 'prop-types';
import { Document, Page, PDFViewer, StyleSheet, Text, View, Font, Image } from '@react-pdf/renderer';
import { appName } from '../../../constants';
import { useTheme } from '@mui/styles';
import { questionTypes } from './question';

Font.register({
    family: 'Custom',
    fonts: [
        {
            src: 'https://fonts.gstatic.com/s/questrial/v13/QdVUSTchPBm7nuUeVf7EuStkm20oJA.ttf',
        },
        {
            src: 'https://fonts.gstatic.com/s/questrial/v13/QdVUSTchPBm7nuUeVf7EuStkm20oJA.ttf',
            fontWeight: 'bold',
        },
        {
            src: 'https://fonts.gstatic.com/s/questrial/v13/QdVUSTchPBm7nuUeVf7EuStkm20oJA.ttf',
            fontWeight: 'normal',
            fontStyle: 'italic',
        },
    ],
});

const getStyles = theme => StyleSheet.create({
    page: {
        flexDirection: 'column',
    },
    header: {
        backgroundColor: theme.palette.primary.main,
        height: 185,
    },
    testName: {
        color: theme.palette.primary.contrastText,
        fontSize: 35,
        paddingTop: 40,
        fontFamily: 'Custom',
    },
    leftAlign: {
        paddingLeft: 15,
        paddingRight: 25,
    },
    subtitle: {
        color: theme.palette.primary.contrastText,
        fontSize: 15,
        paddingTop: 7,
        fontFamily: 'Custom',
    },
    question: {
        fontSize: 15,
        paddingTop: 15,
        fontFamily: 'Custom',
    },
    horizontalCenteredFlexbox: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    answer: {
        fontSize: 13.5,
        paddingLeft: 2,
        fontFamily: 'Custom',
    },
    section: {
        margin: 10,
        padding: 10,
        flexGrow: 1
    },
    image: {
        width: 35,
        height: 30,
    },
    answerIcon: {
        width: 15,
        height: 15,
    },
});

const checkboxUrl = 'https://cdn-icons-png.flaticon.com/512/545/545666.png';
const checkedCheckboxUrl = 'https://cdn-icons-png.flaticon.com/512/7155/7155152.png';
const radioUrl = 'https://cdn-icons-png.flaticon.com/512/6567/6567685.png';
const checkedRadioUrl = 'https://cdn-icons-png.flaticon.com/512/6567/6567686.png';

const codeLogo = 'https://i.pinimg.com/originals/1c/54/f7/1c54f7b06d7723c21afc5035bf88a5ef.png';

export default function PreviewTestPDF({ testDetails, questions, includeAnswers }) {
    const { testName, questionsCount, discipline } = testDetails;
    const theme = useTheme();
    const styles = getStyles(theme);

    const getChoiceSource = React.useCallback((answerType, includeAnswers, isAnswer) => {
        switch (answerType.id) {
            case questionTypes.singleSelection: return (includeAnswers && isAnswer) ? checkedRadioUrl : radioUrl;
            case questionTypes.multipleSelection: return (includeAnswers && isAnswer) ? checkedCheckboxUrl : checkboxUrl;
            case questionTypes.code: return codeLogo;
            default: return null;
        }
    }, []);

    return (
        <PDFViewer height="100%" width="100%">
            <Document>
                <Page size="A4" style={styles.page}>
                    <View style={styles.header}>
                        <View style={styles.leftAlign}>
                            <Text style={styles.testName}>{testName}</Text>
                            <View style={{ paddingTop: 20 }} />
                            <Text style={styles.subtitle}>{`${questionsCount} questions`}</Text>
                            <Text style={styles.subtitle}>{discipline.name}</Text>
                            <View style={{ paddingTop: 15 }}>
                                <Text style={{ color: theme.palette.primary.contrastText, fontSize: 8 }}>{`Created with ${appName}`}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ ...styles.leftAlign, paddingTop: 10 }}>
                        {questions.map((question, index) => (
                            <React.Fragment key={index}>
                                <Text style={styles.question}>{`${index + 1}) ${question.content}`}</Text>
                                {question.answer_type.id !== questionTypes.code ? (
                                    question.possible_answers.map((choice, answerIndex) =>
                                        <View key={answerIndex} style={{ ...styles.horizontalCenteredFlexbox, paddingTop: 7 }}>
                                            <Image style={styles.answerIcon} src={getChoiceSource(question.answer_type, includeAnswers, question.answers.includes(choice))} />
                                            <Text style={styles.answer}>{choice}</Text>
                                        </View>)
                                ) : <View style={{ backgroundColor: 'white', height: 100 }} />}
                            </React.Fragment>
                        ))}
                    </View>
                </Page>
            </Document>
        </PDFViewer>
    );
}

PreviewTestPDF.propTypes = {
    testDetails: PropTypes.object,
    questions: PropTypes.array,
    includeAnswers: PropTypes.bool,
};