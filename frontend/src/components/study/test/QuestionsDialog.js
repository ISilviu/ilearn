import React from 'react';
import PropTypes from 'prop-types';

import { Box, Stack, Dialog } from '@mui/material';

import Questions from '../question/Questions';
import DialogTopBar from '../../layout/DialogTopBar';

function QuestionsDialog({ questionsApi, isOpen, setOpen, onQuestionsSubmit }) {

    const onSubmit = React.useCallback(selectedQuestions => {
        onQuestionsSubmit(selectedQuestions);
        setOpen(false);
    }, [onQuestionsSubmit, setOpen]);

    const questionsDialog = isOpen ? (
        <Dialog open={isOpen} fullScreen>
            <Stack height="100%" width="100%">
                <DialogTopBar
                    title="Questions"
                    onClose={() => setOpen(false)}
                />
                <Box mt={7.5} height="100%">
                    <Questions onSubmit={onSubmit} questionsApi={questionsApi} />
                </Box>
            </Stack>
        </Dialog>
    ) : null;

    return questionsDialog;
}

QuestionsDialog.propTypes = {
    questionsApi: PropTypes.object.isRequired,
    isOpen: PropTypes.bool,
    setOpen: PropTypes.func,
    onQuestionsSubmit: PropTypes.func,
    setIds: PropTypes.func,
};

export default QuestionsDialog;