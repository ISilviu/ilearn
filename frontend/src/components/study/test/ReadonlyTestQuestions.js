import React from 'react';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/styles';
import { DragDropContext } from 'react-beautiful-dnd';
import { Droppable } from 'react-beautiful-dnd';
import { Box, Stack, SvgIcon } from '@mui/material';
import { Draggable } from 'react-beautiful-dnd';
import ReadonlyQuestion from './ReadonlyQuestion';
import { WithCard } from './questionCard';

import DragIndicatorIcon from '@mui/icons-material/DragIndicator';

export default function ReadonlyTestQuestions({ questions, onDragEnd, getColorPalette, isChecked }) {
    const theme = useTheme();
    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="questions">
                {(provided) => (
                    <Stack
                        height="90%"
                        mt={5}
                        flexGrow={1}
                        overflow="auto"
                        alignItems="center"
                        spacing={1}
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                    >
                        {questions.map((question, index) => question ? (
                            <Draggable key={index} draggableId={`${index}`} index={index}>
                                {(provided, snapshot) => (
                                    <Box
                                        display="flex"
                                        justifyContent="center"
                                        width="100%"
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        ref={provided.innerRef}
                                    >
                                        <WithCard bgcolor={snapshot.isDragging ? theme.colors.draggingCard : 'initial'}>
                                            <ReadonlyQuestion
                                                question={question.content}
                                                difficulty={question.difficulty}
                                                answers={question.answers}
                                                possibleAnswers={question.possible_answers}
                                                questionType={question.answerType}
                                                getColorPalette={getColorPalette}
                                                isChecked={isChecked}
                                                showTest
                                            />
                                        </WithCard>
                                        <Box display="flex" alignItems="center">
                                            <SvgIcon component={DragIndicatorIcon} fontSize="small" />
                                        </Box>
                                    </Box>
                                )}
                            </Draggable>
                        ) : null)
                        }
                        {provided.placeholder}
                    </Stack>
                )}
            </Droppable>
        </DragDropContext>
    );
}

ReadonlyTestQuestions.propTypes = {
    questions: PropTypes.array,
    onDragEnd: PropTypes.func,
    getColorPalette: PropTypes.func,
    isChecked: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
};