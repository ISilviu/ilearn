import React from 'react';

import {
    Box, TextField, Stack, Stepper, StepLabel, Step, useTheme, Fab, Tooltip, InputAdornment, SvgIcon, Typography
} from '@mui/material';

import SchoolIcon from '@mui/icons-material/School';
import CreateIcon from '@mui/icons-material/Create';
import AddIcon from '@mui/icons-material/Add';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';

import UploadFileIcon from '@mui/icons-material/UploadFile';

import { useList, useSet, useToggle } from 'react-use';
import { useAddQuestions, useAddTest, useDisciplines } from '../../../api/createTest';

import SearchBox from '../../misc/SearchBox';
import { useMobile } from '../../layout/hooks';
import { IconFab, NavigationButton } from '../../misc/Fields';
import { useActiveStep, useDebouncedRedirect, useServerSideProcessing } from '../../generic/hooks';
import StatusSnackbar from '../../misc/StatusSnackbar';
import TestQuestions from './TestQuestions';
import QuestionsDialog from './QuestionsDialog';
import Disciplines from './Disciplines';

import OrderingFilter from '../../generic/OrderingFilter';
import disciplineOrdering from '../../../filters/disciplineOrdering';
import FileInput from '../../generic/FileInput';
import fileQuestions from '../../../api/questions/fileQuestions';
import backendQuestions from '../../../api/questions/backendQuestions';
import ReadonlyTestQuestions from './ReadonlyTestQuestions';
import { onDragEnd } from '../../misc/draggable';
import { isEmpty } from 'lodash';
import { parseTextFile } from '../../../api/questions/textQuestions';
import { useLocation } from 'react-router-dom';

const useCreateTest = (testName, selectedDisciplineId, questions) => {
    const [status, setStatus] = React.useState(null);
    const testQuestions = React.useMemo(() => questions.filter(question => question), [questions]);

    const addTest = useAddTest({
        onSuccess: (data, variables, context) => {
            setStatus({
                testId: data.data.id,
                success: true,
                message: 'Test added successfully.'
            });
        },
        onError: (err, variables, context) => {
            setStatus({
                success: false,
                message: err.message
            });
        }
    });

    const addQuestions = useAddQuestions({
        onSuccess: (data, variables, context) => {
            let lastIndex = 0;
            const questions = testQuestions.map((question, index) => {
                let mappedQuestion = {};
                if (!question.alreadyExists) {
                    const id = data.data[lastIndex].id;
                    ++lastIndex;
                    mappedQuestion.question = id;

                } else {
                    mappedQuestion.question = question.id;
                }

                return {
                    ...mappedQuestion,
                    order_number: index,
                };
            });

            addTest.mutate({
                name: testName,
                discipline: selectedDisciplineId,
                questions: questions,
            });
        },
        onError: (err, variables, context) => {
            setStatus({
                success: false,
                message: err.message
            });
        }
    });

    const saveTest = React.useCallback(() => {
        const requestBody = testQuestions
            .filter(question => !question.alreadyexists)
            .map(question => ({ ...question, answer_type: question.answerType }));
        addQuestions.mutate(requestBody);
    }, [addQuestions, testQuestions]);

    const resetStatus = React.useCallback(() => setStatus(null), []);

    return [status, saveTest, resetStatus];
};

export const steps = {
    chooseDiscipline: {
        step: 0,
        title: 'Choose discipline',
    },
    pickQuestions: {
        step: 1,
        title: 'Pick questions',
    },
    finish: {
        step: 2,
        title: 'Finish',
    },
};

const mapSelectedQuestions = (selectedQuestions, alreadyExists) =>
    Object.values(selectedQuestions).map(selectedQuestion => ({
        ...selectedQuestion,
        answerType: selectedQuestion.answer_type,
        alreadyExists,
    }));

export default function CreateTest() {

    const location = useLocation();
    let initialStep = steps.chooseDiscipline.step;
    let initialDiscipline = null;
    let initialTestName = '';
    let initialQuestionsList = [];

    if (location.state) {
        initialStep = location.state.step;
        initialDiscipline = location.state.test.discipline;
        initialTestName = location.state.test.testName;
        initialQuestionsList = location.state.test.questions;
    }

    const [
        queryParams,
        { setOrdering, changeSearch, search }
    ] = useServerSideProcessing();

    const { isLoading: areDisciplinesLoading, data: disciplinesResponse } = useDisciplines(queryParams);
    const disciplines = React.useMemo(
        () => disciplinesResponse ? disciplinesResponse.data : [],
        [disciplinesResponse]
    );

    const snackbarPosition = {
        vertical: 'bottom',
        horizontal: 'center',
    };

    const { activeStep,
        canMovePrevious, canMoveNext,
        movePrevious, moveNext } = useActiveStep(initialStep);

    const [selectedDiscipline, setSelectedDisciplineId] = React.useState(initialDiscipline);
    const { id: selectedDisciplineId } = selectedDiscipline ?? { id: null };
    const [testName, setTestName] = React.useState(initialTestName);
    const [questions, questionsActions] = useList(initialQuestionsList);

    const [, { has: isDeleted, add: addDeletedQuestionIndex }] = useSet();

    const [addTestStatus, saveTest, resetAddTestStatus] = useCreateTest(testName, selectedDisciplineId, questions);
    useDebouncedRedirect(addTestStatus);

    const theme = useTheme();
    const isMobile = useMobile();

    const getColorPalette = React.useCallback(
        () => theme.palette.text.main,
        [theme.palette.text.main]
    );

    const [questionsDialogOpen, setQuestionsDialogOpen] = React.useState(false);

    const onExistingQuestionsSubmit = React.useCallback(
        selectedQuestions => {
            questionsActions.push(...mapSelectedQuestions(selectedQuestions, true));
        },
        [questionsActions]
    );

    const renderCurrentStep = (currentStep) => {
        const marginTop = isMobile ? 1.5 : 5;
        switch (currentStep) {
            case steps.chooseDiscipline.step:
                return (
                    <Box display="flex" flexGrow={1} justifyContent="center" overflow="auto">
                        <Disciplines
                            loading={areDisciplinesLoading}
                            disciplines={disciplines}
                            selectedDiscipline={selectedDisciplineId}
                            setSelectedDiscipline={setSelectedDisciplineId}
                            marginTop={marginTop}
                        />
                    </Box>
                );
            case steps.pickQuestions.step:
                return (
                    <>
                        <TestQuestions
                            questions={questions}
                            updateQuestion={questionsActions.updateAt}
                            addDeletedQuestion={addDeletedQuestionIndex}
                            isDeleted={isDeleted}
                            marginTop={marginTop}
                        />
                        <QuestionsDialog
                            questionsApi={backendQuestions}
                            isOpen={questionsDialogOpen}
                            setOpen={setQuestionsDialogOpen}
                            onQuestionsSubmit={onExistingQuestionsSubmit}
                        />
                        {inputQuestionsDialog}
                    </>
                );
            case steps.finish.step:
                return (
                    <>
                        <StatusSnackbar
                            visible={Boolean(addTestStatus)}
                            setInvisible={resetAddTestStatus}
                            message={addTestStatus?.message ?? ''}
                            severity={addTestStatus?.success ? 'success' : 'error'}
                            position={snackbarPosition}
                        />
                        <ReadonlyTestQuestions
                            questions={questions.filter((_, index) => !isDeleted(index))}
                            onDragEnd={result => onDragEnd(result, questions, questionsActions.set)}
                            getColorPalette={getColorPalette}
                            isChecked
                        />
                    </>
                );
            default:
                return null;
        }
    };

    const hasTestNameError = testName === '';
    const [disciplinesOrderings, defaultOrdering] = disciplineOrdering.useOrderings();

    const [inputFileContents, setFileContents] = React.useState(null);
    const [inputFileError, setInputFileError] = React.useState({ error: false, message: '' });
    const [inputDialogOpen, toggleInputDialog] = useToggle(false);

    const fileApi = React.useMemo(() => {
        let fileContents = null;
        if (inputFileContents) {
            const { result, type } = inputFileContents;
            fileContents = result;
            if (type === 'text/plain' && !isEmpty(result)) {
                const parsedQuestions = parseTextFile(result);
                fileContents = JSON.stringify(parsedQuestions);
            }
        }
        return {
            useQuestions: queryParams => fileQuestions.useQuestions(fileContents, queryParams),
        };
    },
        [inputFileContents]
    );

    const onFileQuestionsSubmit = React.useCallback(
        selectedQuestions => questionsActions.push(...mapSelectedQuestions(selectedQuestions, false)),
        [questionsActions]
    );

    const inputQuestionsDialog = inputDialogOpen ? (
        <QuestionsDialog
            questionsApi={fileApi}
            isOpen={inputDialogOpen}
            setOpen={toggleInputDialog}
            onQuestionsSubmit={onFileQuestionsSubmit}
        />
    ) : null;

    const renderToolbar = (activeStep) => {
        switch (activeStep) {
            case steps.chooseDiscipline.step: return (
                <>
                    <SearchBox search={search} onChange={changeSearch} />
                    <OrderingFilter
                        values={disciplinesOrderings}
                        defaultValue={defaultOrdering}
                        onOrderingChanged={setOrdering}
                        getDisplayText={disciplineOrdering.getDisplayText}
                    />
                </>
            );
            case steps.pickQuestions.step: return (
                <>
                    <Tooltip title="Add new question">
                        <Fab color="primary" onClick={() => questionsActions.push(undefined)}>
                            <AddIcon />
                        </Fab>
                    </Tooltip>
                    <Tooltip title="Search existing questions">
                        <Fab color="secondary" onClick={() => setQuestionsDialogOpen(true)}>
                            <ManageSearchIcon />
                        </Fab>
                    </Tooltip>
                    <Tooltip title="Import questions file">
                        <Fab component="label" >
                            <UploadFileIcon />
                            <FileInput
                                onContentRead={data => {
                                    setFileContents(data);
                                    toggleInputDialog();
                                }}
                                onSizeLimitExceeded={reason => {
                                    setInputFileError({
                                        error: true,
                                        message: reason
                                    });
                                }}
                            />
                        </Fab>
                    </Tooltip>
                    <StatusSnackbar
                        visible={Boolean(inputFileError.error)}
                        setInvisible={() => setInputFileError({ error: false, message: '' })}
                        message={inputFileError.message}
                        position={snackbarPosition}
                        severity="error"
                    />
                </>
            )
            case steps.finish.step: return (
                <Box display="flex" gap={2.5}>
                    <TextField
                        value={testName}
                        onChange={(e) => setTestName(e.target.value)}
                        error={hasTestNameError}
                        label="Test name"
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <CreateIcon sx={{ color: hasTestNameError ? theme.palette.error.main : theme.palette.primary.main }} />
                                </InputAdornment>
                            )
                        }}
                    />
                    <Stack alignItems="center">
                        <SvgIcon component={SchoolIcon} color="primary" sx={{ fontSize: 25 }} />
                        <Typography>{selectedDiscipline.name}</Typography>
                    </Stack>
                </Box>
            );
            default: return null;
        }
    }

    const renderNavigation = (activeStep) => {
        let actions = null;
        const isPreviousEnabled = canMovePrevious();
        const isNextEnabled = canMoveNext({
            selectedDisciplineId,
            questions,
            isDeleted,
            testName,
        });

        const previousButton = <NavigationButton text="Previous" onClick={movePrevious} enabled={isPreviousEnabled} />;
        switch (activeStep) {
            case steps.finish.step: {
                actions = (
                    <>
                        {previousButton}
                        <IconFab disabled={!isNextEnabled} onClick={() => saveTest(questions)} ariaLabel="add-test" />
                    </>
                );
                break;
            }
            default: {
                actions = (
                    <>
                        {previousButton}
                        <NavigationButton text="Next" onClick={moveNext} enabled={isNextEnabled} />
                    </>
                );
            }

        }

        return (
            <Box display="flex" alignItems="flex-end" justifyContent="flex-end" gap={1.5}>
                {actions}
            </Box>
        );
    }

    return (
        <Stack pt={3} px={4} height="95%">
            <Stepper activeStep={activeStep}>
                {Object.values(steps).map(({ title }, index) => (
                    <Step key={index}>
                        <StepLabel>{title}</StepLabel>
                    </Step>
                ))}
            </Stepper>
            <Stack mt={5} direction="row" spacing={2} alignItems="center" justifyContent="center">
                {renderToolbar(activeStep)}
            </Stack>
            {renderCurrentStep(activeStep)}
            {renderNavigation(activeStep)}
        </Stack >
    );
}