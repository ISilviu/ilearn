import React from 'react';

const createExpiryTimestamp = (minutes, seconds) => {
    const timestamp = new Date();
    timestamp.setSeconds(timestamp.getSeconds() + seconds);
    timestamp.setMinutes(timestamp.getMinutes() + minutes);
    return timestamp;
};

const useExpiryTimestamp = (minutes, seconds) => {    
    const [expiryTimestamp, setExpiryTimeStamp] = React.useState(createExpiryTimestamp(minutes, seconds));
    const restart = (minutes, seconds) => setExpiryTimeStamp(createExpiryTimestamp(minutes, seconds));

    return [expiryTimestamp, restart];
};

export { createExpiryTimestamp, useExpiryTimestamp };