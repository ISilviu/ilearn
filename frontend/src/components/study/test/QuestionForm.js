import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty, isEqual } from 'lodash';
import { useList } from 'react-use';
import {
    Box, TextField, MenuItem, Stack, IconButton, RadioGroup, FormControlLabel, Radio, Alert, Checkbox
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { Controller, useForm } from 'react-hook-form';

import { useAnswerTypes, useDifficultiesChoices } from '../../../api/createTest';
import Done from '@mui/icons-material/Done';
import { useDirection, useMobile } from '../../layout/hooks';
import { questionTypes } from './question';

export default function QuestionForm({ defaultValues, onSubmit, onDelete }) {

    const { handleSubmit, control, watch, formState, trigger } = useForm();
    const { handleSubmit: handleAnswersSubmit, control: controlAnswers, formState: answersFormState } = useForm();

    const defaultFormValues = React.useMemo(() => ({
        question: '',
        difficulty: 1,
        answerType: questionTypes.singleSelection,
        possible_answers: [],
        answers: [],
    }), []);

    const { content: defaultQuestion, difficulty: defaultDifficulty, answerType: defaultAnswerType, possible_answers: defaultPossibleAnswers, answers: defaultAnswers }
        = defaultValues ?? defaultFormValues;

    const [multipleAnswersState, setMultipleAnswersState] =
        React.useState(defaultPossibleAnswers?.filter(a => defaultAnswers?.includes(a)));
    const [answers, answersActions] = useList(defaultPossibleAnswers);

    const answerType = watch('answerType', defaultAnswerType);

    const getFormValues = React.useCallback((data) => {
        const getChoices = () => {
            switch (data.answerType) {
                case questionTypes.singleSelection:
                    return {
                        possible_answers: answers,
                        answers: [data.answers],
                    };
                case questionTypes.multipleSelection:
                    return {
                        possible_answers: answers,
                        answers: multipleAnswersState,
                    };
                case questionTypes.code:
                    return {
                        possible_answers: data.test_case.split(','),
                        answers: data.expected_result,
                    };
                default:
                    return null;
            }
        };
        const isFormValid = isEmpty(formState.errors) && isEmpty(answersFormState.errors);

        return isFormValid ? {
            content: data.question,
            difficulty: data.difficulty,
            answerType: data.answerType,
            ...getChoices(),
        } : undefined;
    },
        [answers, answersFormState, formState.errors, multipleAnswersState]
    );

    React.useEffect(
        () => trigger(),
        [trigger]
    );

    const difficultiesChoices = useDifficultiesChoices();

    const { data: answerTypes } = useAnswerTypes();
    const answerTypesChoices = React.useMemo(() => answerTypes ?
        answerTypes.data.map(item => ({ id: item.id, text: item.answer_type })) : [],
        [answerTypes]
    );

    const onBlur = (data) => {
        const formValues = getFormValues(data);
        onSubmit(formValues);
    };

    const onAddAnswerClicked = (data) => {
        answersActions.push(data['new-answer']);
    };

    const isMobile = useMobile();
    const direction = useDirection();

    const renderAnswerSection = (answerType) => {
        switch (answerType) {
            case questionTypes.singleSelection:
                return (
                    <Controller
                        name="answers"
                        control={control}
                        defaultValue={defaultAnswerType === questionTypes.singleSelection ? (defaultAnswers ? defaultAnswers[0] : null) : null}
                        rules={{
                            validate: {
                                atLeastTwoChoices: () => answers.length > 1 || 'At least two choices are required',
                                atLeastOneAnswer: (data) => {
                                    if (answerType !== questionTypes.singleSelection) {
                                        return true;
                                    }
                                    return answers.includes(data) || 'At least one answer is required';
                                },
                            }
                        }}
                        render={({ field }) => (
                            <RadioGroup aria-label="answers" {...field}>
                                {
                                    answers.map((answer, index) => (
                                        <Box display="flex" key={index}>
                                            <FormControlLabel {...field} value={answer} control={<Radio />} label={answer} />
                                            <IconButton color="error" onClick={() => answersActions.removeAt(index)}>
                                                <DeleteOutlineIcon />
                                            </IconButton>
                                        </Box>
                                    ))
                                }
                            </RadioGroup>
                        )}
                    />
                );
            case questionTypes.multipleSelection:
                return (
                    <Controller
                        name="answers"
                        control={control}
                        defaultValue={defaultPossibleAnswers}
                        rules={{
                            validate: {
                                atLeastTwoChoices: () => answers.length > 1 || 'At least two choices are required',
                                atLeastOneAnswer: () => multipleAnswersState.length || 'At least one answer is needed'
                            }
                        }}
                        render={({ field }) => (
                            <>
                                {answers.map((answer, index) => (
                                    <Controller
                                        control={control}
                                        key={index}
                                        name={`answers${index}`}
                                        render={({ field }) => (
                                            <Box display="flex" key={index}>
                                                <FormControlLabel {...field}
                                                    control={<Checkbox
                                                        checked={multipleAnswersState.includes(answer)}
                                                        onChange={(e, checked) => {
                                                            if (checked) {
                                                                setMultipleAnswersState(
                                                                    multipleAnswersState.concat(answer)
                                                                );
                                                            } else {
                                                                setMultipleAnswersState(
                                                                    multipleAnswersState.filter(value => !isEqual(value, answer))
                                                                );
                                                            }
                                                        }} />}
                                                    label={answer}
                                                    value={answer}
                                                />
                                                <IconButton color="error" onClick={() => answersActions.removeAt(index)}>
                                                    <DeleteOutlineIcon />
                                                </IconButton>
                                            </Box>
                                        )}
                                    />
                                ))}
                            </>
                        )}
                    />
                );
            case questionTypes.code:
                return (
                    <Box display="flex" flexDirection={direction}>
                        <Controller
                            name="test_case"
                            control={control}
                            defaultValue={defaultPossibleAnswers?.join(',')}
                            rules={{
                                required: { value: true, message: 'The test case values are required' },
                                validate: {
                                    commaSeparated: testCaseValues => {
                                        const values = testCaseValues.split(',');
                                        return values.length > 0 || 'The test case values have to be split by comma.';
                                    },
                                }
                            }}
                            render={({ field }) =>
                                <TextField
                                    multiline
                                    InputProps={{ fullWidth: true }}
                                    helperText={formState.errors.test_case?.message}
                                    required
                                    error={Boolean(formState.errors.test_case)}
                                    {...field}
                                    label="Test case values"
                                    variant="outlined"
                                    fullWidth
                                />
                            }
                        />
                        <Controller
                            name="expected_result"
                            control={control}
                            defaultValue={defaultAnswers}
                            rules={{ required: { value: true, message: 'The expected result is required' } }}
                            render={({ field }) =>
                                <TextField
                                    multiline
                                    InputProps={{ fullWidth: true }}
                                    helperText={formState.errors.expected_result?.message}
                                    required
                                    error={Boolean(formState.errors.expected_result)}
                                    {...field}
                                    label="Expected result"
                                    variant="outlined"
                                    fullWidth
                                />
                            }
                        />
                    </Box>
                );
            default:
                return null;
        }
    }

    return (
        <Stack direction="row" width="100%" spacing={1.5} py={2} pl={2} pr={1} gap={0.5}>
            <Stack width="100%" spacing={1.5}>
                <Stack direction={direction} spacing={2}>
                    <Controller
                        name="question"
                        control={control}
                        defaultValue={defaultQuestion}
                        rules={{ required: { value: true, message: 'The question is required' } }}
                        render={({ field }) => <TextField multiline InputProps={{ fullWidth: true }} helperText={formState.errors.question?.message} required error={Boolean(formState.errors.question)} {...field} label="Question" variant="outlined" fullWidth />}
                    />

                    <Controller
                        name="difficulty"
                        control={control}
                        defaultValue={defaultDifficulty}
                        render={({ field }) => (
                            <TextField select label="Difficulty" variant="outlined" fullWidth {...field}>
                                {difficultiesChoices.map(difficulty => <MenuItem key={difficulty.id} value={difficulty.id}>{difficulty.text}</MenuItem>)}
                            </TextField>
                        )}
                    />
                    <Controller
                        name="answerType"
                        control={control}
                        defaultValue={defaultAnswerType}
                        render={({ field }) => (
                            <TextField select label="Question Type" variant="outlined" fullWidth {...field}>
                                {answerTypesChoices.map(answerType => <MenuItem key={answerType.id} value={answerType.id}>{answerType.text}</MenuItem>)}
                            </TextField>
                        )} />
                </Stack>
                {answerType !== questionTypes.code ? (
                    <Box display="flex" width={isMobile ? '100%' : '35%'}>
                        <Controller
                            name="new-answer"
                            control={controlAnswers}
                            rules={{
                                required: true,
                                validate: {
                                    noDuplicates: answer => !answers.includes(answer) || 'No duplicate answers are allowed',
                                }
                            }}
                            render={({ field }) =>
                                <TextField
                                    multiline
                                    required
                                    helperText={answersFormState.errors ? answersFormState.errors['new-answer']?.message : null}
                                    error={Boolean(answersFormState.errors['new-answer'])}
                                    {...field}
                                    label="New answer"
                                    variant="outlined"
                                    fullWidth
                                />
                            }
                        />
                        <IconButton color="primary" onClick={handleAnswersSubmit(onAddAnswerClicked)}>
                            <AddIcon />
                        </IconButton>
                    </Box>
                ) : null}
                {renderAnswerSection(answerType)}
                {formState.errors.answers ? <Alert severity="error">{formState.errors.answers.message}</Alert> : null}
            </Stack>
            <Stack alignItems="center" justifyContent="center">
                <IconButton color="error" onClick={onDelete}>
                    <DeleteOutlineIcon />
                </IconButton>
                <IconButton color="primary" onClick={handleSubmit(onBlur)}>
                    <Done />
                </IconButton>
            </Stack>
        </Stack>

    );
}

QuestionForm.propTypes = {
    defaultValues: PropTypes.object,
    onSubmit: PropTypes.func,
    onDelete: PropTypes.func,
};