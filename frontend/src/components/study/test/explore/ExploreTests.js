import { Box, Grid, Paper, Stack, Typography } from "@mui/material";
import React from "react";
import Scrollbars from "react-custom-scrollbars-2";
import { useWindowSize } from "react-use";
import { useTests } from "../../../../api/test";
import disciplineFilter from "../../../../filters/disciplineFilter";
import testOrdering from "../../../../filters/testOrdering";
import Filters from "../../../generic/Filters";
import { useServerSideProcessing } from "../../../generic/hooks";
import Toolbar from "../../../generic/Toolbar";
import { useDesktop } from "../../../layout/hooks";
import { WithCard } from "../questionCard";
import ReadonlyTest from "../ReadonlyTest";

const useFilters = () => {
    const disciplineFilterData = disciplineFilter.useFilter();

    const getQueryParams = React.useCallback(
        filters => disciplineFilter.getQueryParams(filters),
        []
    );

    const getFilters = React.useCallback(
        queryParams => disciplineFilter.getFiltersFromQueryParams(queryParams),
        []
    );

    return React.useMemo(() => ({
        filters: [disciplineFilterData],
        getQueryParams: getQueryParams,
        getFilters: getFilters,
    }), [disciplineFilterData, getQueryParams, getFilters]);
};

export default function ExploreTests() {
    const [
        queryParams,
        { setFilters, search, changeSearch, setOrdering }
    ] = useServerSideProcessing();

    const [orderings, defaultOrdering] = testOrdering.useOrderings();

    const tests = useTests(queryParams);
    const testsLength = tests.data?.data ? tests.data.data.length : 0;
    const isDesktop = useDesktop();
    const { height } = useWindowSize();

    return (
        <Box display="flex" flexGrow={1}>
            {isDesktop ? (
                <Box width={350} overflow="auto" height={height}>
                    <Scrollbars autoHide>
                        <Filters useFiltersOptions={useFilters} onFiltersChange={setFilters} />
                    </Scrollbars>
                </Box>
            ) : null}
            <Stack width="100%" py={1.5}>
                <Paper sx={{ py: 2.5 }}>
                    <Toolbar
                        searchProps={{
                            search: search,
                            onChange: changeSearch
                        }}
                        orderingProps={{
                            values: orderings,
                            defaultValue: defaultOrdering,
                            defaultDirection: false,
                            onOrderingChanged: setOrdering,
                            getDisplayText: testOrdering.getDisplayText
                        }}
                        filtersProps={{
                            useFiltersOptions: useFilters,
                            onFiltersChange: setFilters,
                        }}
                    />
                </Paper>
                <Grid container overflow="auto" p={2} spacing={2.5} height="100%">
                    {(tests.isLoading === false && testsLength === 0) ? (
                        <Box display="flex" alignItems="center" justifyContent="center" width="100%" height="100%">
                            <Typography>There are no tests that match your requirements.</Typography>
                        </Box>
                    ) : null}
                    {tests.data?.data?.map(test => test ? (
                        <Grid item xs={12} md={6} lg={3} key={test.id}>
                            <WithCard width="85%">
                                <ReadonlyTest
                                    testId={test.id}
                                    testName={test.name}
                                    discipline={test.discipline}
                                    questionsCount={test.questions_count}
                                />
                            </WithCard>
                        </Grid>
                    ) : null)}
                </Grid>
            </Stack>
        </Box>
    );
}