import React from 'react';
import PropTypes from 'prop-types';

import { Stack } from "@mui/material";
import { LoadingIndicator } from "../../misc/Fields";
import DisciplineCard from '../discipline/DisciplineCard';

function Disciplines({ loading, disciplines, marginTop, selectedDiscipline, setSelectedDiscipline }) {
    if (loading) {
        return <LoadingIndicator additionalProps={{ flexGrow: 1 }} />;
    }

    return (
        <Stack
            direction="row"
            justifyContent="center"
            alignItems="center"
            mt={marginTop}
            overflow="auto"
            flexWrap="wrap"
            gap={1}
        >
            {disciplines.map(discipline => (
                <DisciplineCard
                    key={discipline.id}
                    discipline={discipline}
                    isSelected={discipline.id === selectedDiscipline}
                    setSelectedDiscipline={setSelectedDiscipline}
                />
            ))}
        </Stack>
    );
}

Disciplines.propTypes = {
    loading: PropTypes.bool,
    disciplines: PropTypes.array,
    marginTop: PropTypes.number,
    selectedDiscipline: PropTypes.number,
    setSelectedDiscipline: PropTypes.func,
};

export default Disciplines;