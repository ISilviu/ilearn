import React from 'react';
import PropTypes from 'prop-types';

import { Box, Paper, Typography, Stack } from '@mui/material';

import QuestionForm from './QuestionForm';

function TestQuestions({ questions, updateQuestion, addDeletedQuestion, marginTop, isDeleted }) {
    if (!questions.length) {
        return (
            <Box
                display="flex"
                alignItems="center"
                justifyContent="center"
                height="100%"
            >
                <Typography paragraph>It's pretty empty out here, try adding some questions.</Typography>
            </Box>
        );
    }

    const questionsList = (
        questions.map((question, index) => (
            !isDeleted(index) ? (
                <Paper component="div" elevation={2} key={index}>
                    <Box display="flex">
                        <QuestionForm
                            defaultValues={question}
                            onSubmit={(data) => updateQuestion(index, data)}
                            onDelete={() => addDeletedQuestion(index)}
                        />
                    </Box>
                </Paper>
            ) : null
        ))
    );

    return (
        <Stack mt={marginTop} spacing={2.5} flex={1} overflow="auto">
            {questionsList}
        </Stack>
    );
}

TestQuestions.propTypes = {
    questions: PropTypes.array,
    isDeleted: PropTypes.func,
    marginTop: PropTypes.number,
    updateQuestion: PropTypes.func,
    addDeletedQuestion: PropTypes.func,
};

export default TestQuestions;