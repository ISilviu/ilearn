import React from 'react';
import PropTypes from 'prop-types';
import SchoolIcon from '@mui/icons-material/School';
import {
    Box, Stack, SvgIcon, Typography, Dialog,
    Checkbox, FormControlLabel, Button, Select, MenuItem
} from "@mui/material";
import { useTheme } from '@mui/styles';
import { WithCard } from './questionCard';
import { useTestQuestions } from '../../../api/test';
import ReadonlyTestQuestions from './ReadonlyTestQuestions';
import { onDragEnd } from '../../misc/draggable';

import arrayShuffle from 'array-shuffle';
import PreviewTestPDF from './PreviewTestPDF';
import { useToggle } from 'react-use';
import DialogTopBar from '../../layout/DialogTopBar';
import { questionTypes } from './question';

const useQuestions = (testId, filter) => {
    const testQuestions = useTestQuestions(testId, true);
    const [questions, setQuestions] = React.useState([]);

    React.useEffect(() => {
        if (testQuestions.data) {
            setQuestions(testQuestions.data.data.questions.map(question => ({
                ...question,
                answerType: question.answer_type.id,
                difficulty: question.difficulty.id,
            })));
        }
    }, [testQuestions.data, filter]);

    return [questions, setQuestions];
};

const exportFormats = {
    pdf: 1,
    json: 2,
    txt: 3,
};

export default function ExportTest({ testDetails }) {
    const { testId, testName, questionsCount, discipline } = testDetails;
    const theme = useTheme();
    const [questions, setQuestions] = useQuestions(testId);

    const getColorPalette = React.useCallback(
        () => theme.palette.text.main,
        [theme.palette.text.main]
    );

    const [includeAnswers, toggleIncludeAnswers] = useToggle(false);
    const [exportFormat, setExportFormat] = React.useState(exportFormats.pdf);
    const [randomizeQuestions, toggleRandomizeQuestions] = useToggle(false);

    const [previewDialogOn, togglePreviewDialog] = useToggle(false);

    const previewDialog = previewDialogOn ? (
        <Dialog open={previewDialogOn} fullScreen onClose={togglePreviewDialog}>
            <Stack height="100%">
                <DialogTopBar title="Preview test" onClose={togglePreviewDialog} />
                <Box mt={7} height="100%">
                    <PreviewTestPDF
                        testDetails={testDetails}
                        questions={randomizeQuestions ? arrayShuffle(questions) : questions}
                        includeAnswers={includeAnswers}
                    />
                </Box>
            </Stack>
        </Dialog>
    ) : null;

    return (
        <Box width="100%" height="100%" display="flex">
            <Stack ml={2} width="20%" spacing={1.2}>
                <Stack>
                    <WithCard width="100%" bgcolor={theme.palette.primary.main}>
                        <Stack alignItems="center" bgcolor={theme.palette.primary.main} borderRadius={theme.radius.general}>
                            <SvgIcon component={SchoolIcon} color="secondary" sx={{ fontSize: 35 }} />
                            <Typography variant="body1" color="secondary">{discipline.name}</Typography>
                        </Stack>
                    </WithCard>
                    <WithCard width="100%">
                        <Stack display="flex" alignItems="center">
                            <Typography variant="h5">{testName}</Typography>
                            <Typography variant="body1">{`${questionsCount} questions`}</Typography>
                        </Stack>
                    </WithCard>
                </Stack>
                {exportFormat === exportFormats.pdf ? (
                    <FormControlLabel
                        control={<Checkbox />}
                        label="Include answers"
                        checked={includeAnswers}
                        onChange={toggleIncludeAnswers}
                    />
                ) : null}
                <FormControlLabel
                    control={<Checkbox />}
                    label="Randomize questions"
                    checked={randomizeQuestions}
                    onChange={toggleRandomizeQuestions}
                />
                <Select
                    value={exportFormat}
                    onChange={e => setExportFormat(e.target.value)}
                >
                    <MenuItem value={exportFormats.pdf}>PDF</MenuItem>
                    <MenuItem value={exportFormats.json}>json</MenuItem>
                    {questions.some(q => q.answer_type.id !== questionTypes.code) ? (
                        <MenuItem value={exportFormats.txt}>txt</MenuItem>
                    ) : null}
                </Select>
                <Button onClick={() => {
                    switch (exportFormat) {
                        case exportFormats.pdf:
                            togglePreviewDialog();
                            break;
                        case exportFormats.json:
                            const element = document.createElement('a');
                            const testQuestions = randomizeQuestions ? arrayShuffle(questions) : questions;
                            const file = new Blob([JSON.stringify({
                                ...testDetails,
                                questions: testQuestions.map(question => ({
                                    ...question,
                                    choices: undefined,
                                })),
                            }, null, "\t")], { type: 'application/json' });
                            element.href = URL.createObjectURL(file);
                            element.download = `${testDetails.testName}.json`;
                            document.body.appendChild(element);
                            element.click();
                            break;
                        case exportFormats.txt: {
                            const element = document.createElement('a');
                            const testQuestions = randomizeQuestions ? arrayShuffle(questions) : questions;
                            let txtFile = '';
                            txtFile += `discipline: ${testDetails.discipline.id}\n`;
                            txtFile += `name: ${testDetails.testName}\n`;
                            txtFile += `#\n`;
                            testQuestions.filter(q => q.answer_type.id !== questionTypes.code).forEach((question, index) => {
                                txtFile += `content: ${question.content}\n`;
                                txtFile += `possible_answers: ${question.possible_answers.join('#')}\n`;
                                txtFile += `answers: ${question.answers.join('#')}\n`;
                                txtFile += `difficulty: ${question.difficulty}\n`;
                                txtFile += `answer_type: ${question.answer_type.id}\n`;
                                txtFile += index === testQuestions.length - 1 ? `\n` : `#\n`;
                            })
                            const file = new Blob([txtFile], { type: "text/plain" });
                            element.href = URL.createObjectURL(file);
                            element.download = `${testDetails.testName}.txt`;
                            document.body.appendChild(element);
                            element.click();
                            break;
                        }
                        default:
                            break;

                    }
                }} variant="contained">
                    <Typography p={0.3} variant='body2'>
                        Export
                    </Typography>
                </Button>
            </Stack>
            <Stack justifyContent="center" alignItems="center" width="100%" flexGrow={1}>
                <Box width="100%" height="100%">
                    <ReadonlyTestQuestions
                        questions={questions}
                        onDragEnd={result => onDragEnd(result, questions, setQuestions)}
                        getColorPalette={getColorPalette}
                        isChecked={includeAnswers}
                    />
                </Box>
            </Stack>
            {previewDialog}
        </Box>
    );
}

ExportTest.propTypes = {
    testDetails: PropTypes.object,
};