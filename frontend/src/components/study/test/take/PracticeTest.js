import React from 'react';
import PropTypes from 'prop-types';
import CodeEditor from '@uiw/react-textarea-code-editor';
import { Box } from '@mui/system';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import { FormControl, FormControlLabel, IconButton, LinearProgress, Paper, Radio, RadioGroup, Stack, Typography, useTheme } from '@mui/material';
import { IconFab, LoadingIndicator, NavigationButton } from '../../../misc/Fields';
import { useTimer } from 'react-timer-hook';
import { useMap, useToggle, useWindowSize } from 'react-use';
import { useTest, useTestQuestions } from '../../../../api/test';
import { useDirection, useMobile } from '../../../layout/hooks';
import { normalise } from '../../../../app/utilities';
import { useExpiryTimestamp } from '../hooks';
import { useTestScore } from '../../../../api/takeTest';
import TestHeader from './TestHeader';
import TestResults from './TestResults';
import { useParams } from 'react-router-dom';
import CheckboxGroup from '../../../misc/CheckboxGroup';
import { questionTypes } from '../question';
import ReadonlyQuestion from '../ReadonlyQuestion';
import Scrollbars from 'react-custom-scrollbars-2';
import { WithCard } from '../questionCard';

function AnswersRadioGroup({ choice, setChoice, choices, disabled }) {
    return (
        <FormControl component="fieldset" disabled={disabled}>
            <RadioGroup
                aria-label="choices"
                value={choice}
                onChange={(e) => setChoice(e.target.value)}
                name="choices-radio-buttons-group"
            >
                {choices.map((choice, index) => <FormControlLabel key={index} value={choice} control={<Radio />} label={choice} />)}
            </RadioGroup>
        </FormControl>
    );
}

AnswersRadioGroup.propTypes = {
    choice: PropTypes.string,
    setChoice: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
}

function Timer({ expiryTimestamp, onTimeout }) {
    const { seconds, minutes, restart } = useTimer({
        expiryTimestamp: expiryTimestamp,
        onExpire: onTimeout,
    });

    React.useEffect(
        () => restart(expiryTimestamp),
        [expiryTimestamp, restart]
    );

    const displaySeconds = seconds < 10 ? `0${seconds}` : seconds;
    const color = minutes === 0 && seconds <= 20 ? 'red' : 'black';

    return (
        <Typography
            color={color}
            variant="h4"
        >
            {`${minutes}:${displaySeconds}`}
        </Typography>
    );
}

Timer.propTypes = {
    expiryTimestamp: PropTypes.object.isRequired,
    onTimeout: PropTypes.func.isRequired,
}

function TimerBox(props) {
    const theme = useTheme();
    return (
        <Box
            bgcolor={theme.colors.testCardBackground}
            borderRadius={theme.radius.general}
            height={85}
            width={85}
            display="flex"
            alignItems="center"
            justifyContent="center"
        >
            <Timer {...props} />
        </Box>
    )
}

const useAnswerFormState = () => {
    const [currentAnswer, setCurrentAnswer] = React.useState(null);
    const [disabled, setDisabled] = React.useState(false);

    const disable = React.useCallback(() => setDisabled(true), []);
    const resetFormState = React.useCallback(() => {
        setCurrentAnswer(null);
        setDisabled(false);
    }, []);

    return [currentAnswer, setCurrentAnswer, disabled, disable, resetFormState];
};

export default function PracticeTest({ additionalSubmitProps, practice = true }) {
    let { testId } = useParams();
    testId = testId ?? 54;

    const theme = useTheme();

    const testStates = React.useMemo(() => ({
        inProgress: 1,
        finished: 2,
        halted: 3,
        validating: 4,
    }), []);

    const [currentQuestionIndex, setCurrentQuestionIndex] = React.useState(0);
    const [testState, setTestState] = React.useState(testStates.inProgress);
    const [testResult, setTestResult] = React.useState(null);
    const [currentAnswer, setCurrentAnswer,
        isFormDisabled, disableForm,
        resetFormState
    ] = useAnswerFormState();

    const [answers, { set: addAnswer }] = useMap({});

    const [isNextEnabled, setNextEnabled] = React.useState(false);
    React.useEffect(
        () => setNextEnabled(currentAnswer != null),
        [currentAnswer]
    );

    const test = useTest(testId);
    const testQuestions = useTestQuestions(testId);

    const testData = React.useMemo(() => {
        let testData = {};
        if (test.data && test.data.data && testQuestions.data && testQuestions.data.data) {
            const data = test.data.data;
            const testQuestionsData = testQuestions.data.data;
            testData = {
                name: data.name,
                discipline: data.discipline,
                questions: testQuestionsData.questions,
                questionsCount: testQuestionsData.count,
            };
        }
        return testData;
    }, [test, testQuestions]);

    const allowedTime = [1, 5];
    const [expiryTimestamp, resetExpiryTimestamp] = useExpiryTimestamp(...allowedTime);
    const onTimeout = React.useCallback(() => {
        setNextEnabled(true);
        disableForm();
    }, [disableForm]);

    const timer = <TimerBox expiryTimestamp={expiryTimestamp} onTimeout={onTimeout} />;

    const advanceQuestion = () => {
        addAnswer(currentQuestionIndex, currentAnswer);

        setCurrentQuestionIndex(currentQuestionIndex + 1);
        resetExpiryTimestamp(...allowedTime);
        resetFormState();
        setNextEnabled(false);
    };

    const testScore = useTestScore(testId, {
        onSuccess: (data, variables, context) => {
            setTestResult(data.data);
            setTestState(testStates.finished);
        },
    });

    const finishTest = () => {
        addAnswer(currentQuestionIndex, currentAnswer);
        setTestState(testStates.validating);

        answers[currentQuestionIndex] = currentAnswer;
        testScore.mutate({
            answers,
            practice,
            ...additionalSubmitProps,
        });
    };

    const isFinalQuestion = currentQuestionIndex === (testData.questionsCount - 1);

    const isMobile = useMobile();
    const { width: windowWidth } = useWindowSize();
    const direction = useDirection();

    const answerType = testData?.questions?.at(currentQuestionIndex)?.answer_type?.id;

    const renderTestState = (testState) => {
        const genericTestHeader = (
            <TestHeader
                leftText={testData.name}
                rightText={`${questionsRemaining === 1 ? `One question` : `${questionsRemaining} questions`}  remaining`}
                bgcolor={testHeaderBackgroundColor}
            />
        );

        const renderAnswerSection = questionType => {
            switch (questionType) {
                case questionTypes.singleSelection:
                    return (
                        <AnswersRadioGroup
                            choice={currentAnswer}
                            setChoice={setCurrentAnswer}
                            choices={testData.questions[currentQuestionIndex].possible_answers}
                            disabled={isFormDisabled}
                        />
                    );
                case questionTypes.multipleSelection:
                    return (
                        <CheckboxGroup
                            checkedValues={currentAnswer}
                            choices={testData.questions[currentQuestionIndex].possible_answers}
                            onChange={data => setCurrentAnswer(data)}
                            disabled={isFormDisabled}
                        />
                    );
                case questionTypes.code:
                    return (
                        <CodeEditor
                            value={currentAnswer}
                            language="py"
                            placeholder="Your solution goes here. The function name has to be 'solution'."
                            onChange={e => setCurrentAnswer(e.target.value)}
                            padding={15}
                            style={{
                                fontSize: 14,
                                backgroundColor: "#f5f5f5",
                                fontFamily: 'ui-monospace,SFMono-Regular,SF Mono,Consolas,Liberation Mono,Menlo,monospace',
                            }}
                            disabled={isFormDisabled}
                        />
                    );
                default:
                    return null;
            }
        }

        switch (testState) {
            case testStates.inProgress:
                return testData.questions[currentQuestionIndex] ? (
                    <>
                        {genericTestHeader}
                        <LinearProgress sx={{ height: theme.spacing(2), borderRadius: theme.radius.general }} variant="determinate" value={normalise(currentQuestionIndex + 1, testData.questionsCount)} color="primary" />
                        <Box p={1.5}>
                            <Stack spacing={3}>
                                <Typography variant="h5" fontWeight={550}>{testData.questions[currentQuestionIndex].content}</Typography>
                                {renderAnswerSection(answerType)}
                            </Stack>
                        </Box>
                        <Box display="flex" alignItems="flex-end" justifyContent="flex-end" pr={2} pb={2}>
                            {isFinalQuestion ?
                                <IconFab disabled={!isNextEnabled} onClick={finishTest} ariaLabel="submit-test" /> :
                                <NavigationButton text="Next" onClick={advanceQuestion} enabled={isNextEnabled} />}
                        </Box>
                    </>
                ) : null;
            case testStates.finished:
                return testResult ? (
                    <TestResults
                        correctAnswers={testResult.correct}
                        totalQuestions={testResult.total}
                        testName={testData.name}
                        wrongAnswers={testResult.wrong}
                        testScore={testResult.score}
                    />
                ) : null;
            case testStates.validating:
                return (
                    <>
                        {genericTestHeader}
                        <Stack py={2.5} display="flex" alignItems="center" spacing={3.5}>
                            <Typography variant="body1">We are validating your test ...</Typography>
                            <LoadingIndicator />
                        </Stack>
                    </>
                )

            default:
                return null;
        }
    };

    const testHeaderBackgroundColor = theme.colors.testHeader.inProgress;
    const questionsRemaining = testData.questionsCount - currentQuestionIndex - 1;

    const mobileTimer = isMobile ? (
        <Box mb={2}>
            {timer}
        </Box>
    ) : null;

    const desktopTimer = !isMobile ? (
        <Box ml={2}>
            {timer}
        </Box>
    ) : null;

    const [answersExpanded, toggle] = useToggle();
    const answersComponentRef = React.useRef(null);
    React.useEffect(() => {
        if (answersExpanded) {
            answersComponentRef.current.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
        }
    })

    const getColorPalette = React.useCallback((answer, index) => {
        const questionAnswer = testResult?.questions[index].answers.find(a => a.content === answer);
        const isCorrect = questionAnswer?.correct;
        const isPicked = questionAnswer?.picked;

        if (isPicked) {
            return isCorrect ? theme.colors.test.score.best : theme.colors.test.score.low;
        }
        if (isCorrect) {
            return theme.colors.test.score.best;
        }
        return theme.palette.text.main;
    },
        [theme.colors.test.score.best, theme.colors.test.score.low, theme.palette.text.main, testResult]
    );

    const isChecked = React.useCallback(answer => answer.picked, []);

    if (test.isLoading || testQuestions.isLoading) {
        return <LoadingIndicator />;
    }

    if (!testData.questionsCount) {
        return <Box fontWeight="bold" display="flex" alignItems="center" justifyContent="center" flexDirection="column" height='100%' flexGrow={1}
        >
            Ops. No questions found for this test.
        </Box>
    }

    return (
        <Scrollbars>
            <Stack height="100%">
                <Box display="flex" alignItems="center" justifyContent="center" flexDirection="column" height={300} flexGrow={1}>
                    <Box display="flex" px={1.5} width={isMobile ? windowWidth : 900} flexDirection={direction} alignItems={isMobile ? 'center' : null}>
                        {testState !== testStates.finished ? mobileTimer : null}
                        <Paper sx={{ width: '100%', display: 'flex', flexDirection: 'column', gap: 1.5, borderRadius: theme.radius.general, bgcolor: theme.colors.testCardBackground }} elevation={0}>
                            {renderTestState(testState)}
                        </Paper>
                        {testState !== testStates.finished ? desktopTimer : null}
                    </Box>
                </Box>
                {testState === testStates.finished ? (
                    <Stack alignItems="center" >
                        <Typography variant="h6">Your answers</Typography>
                        <IconButton color="primary" aria-label="see answers" onClick={() => toggle()}>
                            {
                                answersExpanded ?
                                    <ExpandLessIcon sx={{ fontSize: 47 }} /> :
                                    <ExpandMoreIcon sx={{ fontSize: 47 }} />
                            }
                        </IconButton>
                    </Stack>
                ) : null}
                {
                    (testResult && answersExpanded) ? (
                        <Stack height="1%" alignItems="center" spacing={1.5} py={2} ref={answersComponentRef}>
                            {
                                testResult.questions?.map((question, index) => (
                                    <WithCard key={index}>
                                        {
                                            question.answer_type === questionTypes.code ? (
                                                <ReadonlyQuestion
                                                    index={index}
                                                    question={question.content}
                                                    difficulty={question.difficulty}
                                                    answers={question.answers}
                                                    possibleAnswers={question.possible_answers}
                                                    questionType={question.answer_type}
                                                    getColorPalette={getColorPalette}
                                                    status={question.status}
                                                    showTest={false}
                                                />
                                            ) : (
                                                <ReadonlyQuestion
                                                    index={index}
                                                    question={question.content}
                                                    difficulty={question.difficulty}
                                                    answers={question.answers.filter(q => q.picked).map(q => q.content)}
                                                    possibleAnswers={question.answers.map(q => q.content)}
                                                    questionType={question.answer_type}
                                                    getColorPalette={getColorPalette}
                                                    isChecked={isChecked}
                                                />
                                            )
                                        }
                                    </WithCard>
                                ))
                            }
                        </Stack>
                    ) : null
                }
            </Stack>
        </Scrollbars>
    );
}

PracticeTest.propTypes = {
    practice: PropTypes.bool,
    additionalSubmitProps: PropTypes.object,
};