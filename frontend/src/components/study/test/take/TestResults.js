import React from 'react';
import { Stack, Typography, useTheme } from "@mui/material";
import { Box } from "@mui/system";

import CheckIcon from '@mui/icons-material/Check';
import ClearIcon from '@mui/icons-material/Clear';
import { useDirection, useMobile } from '../../../layout/hooks';
import TestHeader from './TestHeader';

export default function TestResults({ testName, totalQuestions, testScore, correctAnswers, wrongAnswers }) {

    const theme = useTheme();

    const getScoreColor = React.useCallback(testScore => {
        let color = null;
        if (testScore > 70) {
            color = theme.colors.test.score.best;
        } else if (testScore > 50) {
            color = theme.colors.test.score.medium;
        } else {
            color = theme.colors.test.score.low;
        }
        return color;
    }, [theme.colors]);

    const scoreColor = getScoreColor(testScore);

    const renderMultiTypograhpy = (children) => (
        <Box display="flex" gap={0.9} alignItems="center">
            {children}
        </Box>
    );

    const statsProps = React.useMemo(() => ({ fontWeight: 550, variant: 'subtitle1' }), []);

    const isMobile = useMobile();
    const direction = useDirection();

    const scoreTypograhyVariant = React.useMemo(() => isMobile ? 'h5' : 'h4', [isMobile]);

    return (
        <>
            <TestHeader
                leftText={testName}
                rightText={`${totalQuestions} questions`}
                bgcolor={scoreColor}
                textColor="white"
            />
            <Stack display="flex" alignItems="center" justifyContent="center" pt={10}>
                {renderMultiTypograhpy((
                    <>
                        <Typography fontWeight={600} variant={scoreTypograhyVariant}>Your test score is</Typography>
                        <Typography fontWeight={600} variant={scoreTypograhyVariant} color={scoreColor}>{`${testScore}%`}</Typography>
                    </>
                ))}
                <Box display="flex" py={5} gap={5.5} flexDirection={direction}>
                    <Box display="flex" alignItems="center" gap={1}>
                        <CheckIcon fontSize="large" sx={{ color: 'white', bgcolor: theme.colors.test.correctAnswers, borderRadius: theme.radius.general }} />
                        <Stack>
                            <Typography {...statsProps} >Correct</Typography>
                            {renderMultiTypograhpy((
                                <>
                                    <Typography {...statsProps} color={theme.colors.test.correctAnswers}>{correctAnswers}</Typography>
                                    <Typography {...statsProps} >questions</Typography>
                                </>
                            ))}
                        </Stack>
                    </Box>
                    <Box display="flex" alignItems="center" gap={1}>
                        <ClearIcon fontSize="large" sx={{ color: 'white', bgcolor: theme.colors.test.wrongAnswers, borderRadius: theme.radius.general }} />
                        <Stack>
                            <Typography {...statsProps} >Wrong</Typography>
                            {renderMultiTypograhpy((
                                <>
                                    <Typography {...statsProps} color={theme.colors.test.wrongAnswers}>{wrongAnswers}</Typography>
                                    <Typography {...statsProps} >questions</Typography>
                                </>
                            ))}
                        </Stack>
                    </Box>
                </Box>
            </Stack>
        </>
    );
}