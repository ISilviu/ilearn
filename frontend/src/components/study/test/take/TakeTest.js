import React from 'react';
import { Box, Button, Checkbox, FormControlLabel, Paper, Stack, TextField } from '@mui/material';
import { useTheme } from '@mui/styles';
import { useParams } from "react-router-dom";
import { useToggle, useWindowSize } from 'react-use';
import { useTest } from '../../../../api/test';
import { useDirection, useMobile } from '../../../layout/hooks';
import PracticeTest from './PracticeTest';
import TestHeader from './TestHeader';
import EmailIcon from '@mui/icons-material/Email';
import PersonIcon from '@mui/icons-material/Person';
import * as yup from "yup";
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { has, size } from 'lodash';

const schema = yup.object({
    name: yup.string().required('Your name is required.'),
    email: yup.string().required('Your email is required.').email('Please enter a correct email.'),
});

export default function TakeTest() {
    const { handleSubmit, control, formState: { errors }, trigger } = useForm({
        resolver: yupResolver(schema),
        mode: 'onChange',
        defaultValues: {
            name: '',
            email: '',
            sendMail: false,
        },
    });

    React.useEffect(() => trigger(), [trigger]);

    const { testId } = useParams();
    const test = useTest(testId);
    const testData = React.useMemo(() => {
        let testName = '';
        let questionsCount = 0;
        let disciplineName = '';

        if (test.data) {
            disciplineName = test.data.data?.discipline.name;
            testName = test.data.data?.name;
            questionsCount = test.data.data?.questions.length;
        }

        return {
            disciplineName,
            testName,
            questionsCount,
        };
    }, [test.data]);

    const theme = useTheme();
    const isMobile = useMobile();
    const direction = useDirection();
    const { width: windowWidth } = useWindowSize();

    const [isInitialPage, beginTest] = useToggle(true);

    const [userData, setUserData] = React.useState({});

    return isInitialPage ? (
        <Stack height="100%">
            <Box display="flex" alignItems="center" justifyContent="center" flexDirection="column" height={300} flexGrow={1}>
                <Box display="flex" px={1.5} width={isMobile ? windowWidth : 900} flexDirection={direction} alignItems={isMobile ? 'center' : null}>
                    <Paper sx={{ width: '100%', display: 'flex', flexDirection: 'column', gap: 1.5, borderRadius: theme.radius.general, bgcolor: theme.colors.testCardBackground }} elevation={0}>
                        <TestHeader
                            leftText={`${testData.disciplineName} - ${testData.testName}`}
                            rightText={`${testData.questionsCount} questions`}
                            bgcolor={theme.palette.primary.main}
                            textColor={theme.palette.primary.contrastText}
                        />
                    </Paper>
                </Box>
                <Stack borderRadius={theme.radius.general} bgcolor={theme.colors.testCardBackground} width={isMobile ? windowWidth : 900} alignItems="center" pt={2}>
                    <Box display="flex" alignItems="center" gap={1}>
                        <Controller
                            name="name"
                            control={control}
                            render={({ field }) => (
                                <TextField
                                    helperText={errors.name?.message}
                                    label="Your name"
                                    InputProps={{ endAdornment: <PersonIcon /> }}
                                    error={has(errors, 'name')}
                                    {...field}
                                />
                            )}
                        />
                        <Controller
                            name="email"
                            control={control}
                            render={({ field }) => (
                                <TextField
                                    helperText={errors.email?.message}
                                    label="Your email"
                                    InputProps={{ endAdornment: <EmailIcon /> }}
                                    error={has(errors, 'email')}
                                    {...field}
                                />
                            )}
                        />
                    </Box>
                    <Box pt={4}>
                        <Controller
                            name="sendMail"
                            control={control}
                            render={({ field }) => (
                                <FormControlLabel label="Email test results" control={<Checkbox />} {...field} />
                            )}
                        />
                    </Box>
                    <Box pt={4} pb={2}>
                        <Button variant="contained" disabled={size(errors) > 0} onClick={handleSubmit(data => {
                            setUserData(data);
                            beginTest();
                        })}>
                            Begin test
                        </Button>
                    </Box>
                </Stack>
            </Box>
        </Stack>
    ) : <PracticeTest additionalSubmitProps={userData} practice={false} />;
}