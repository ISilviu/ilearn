import { ThemeProvider } from '@mui/styles';
import { render } from '@testing-library/react';
import { theme } from '../../../../theme';
import { useTest, useTestQuestions } from '../../../../api/test';
import { useTestScore } from '../../../../api/takeTest';
import "@testing-library/jest-dom/extend-expect";
import { fireEvent } from '@testing-library/react';
import PracticeTest from './PracticeTest';
import { questionTypes } from '../question';

/* eslint-env jest */

jest.mock('../../../../api/test');
jest.mock('../../../../api/takeTest');
jest.mock('@uiw/react-textarea-code-editor', () => ({}));
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useParams: () => ({ testId: 15 })
}));

beforeEach(() => {
    useTestScore.mockImplementation(jest.fn());
});

describe('Take Test', () => {
    test('does not render without questions', () => {
        useTest.mockReturnValue({
            data: {
                data: {
                    name: 'Some test',
                    discipline: 'Discipline Name',
                    questions: [],
                },
            },
        });

        useTestQuestions.mockReturnValue({
            data: {
                data: {
                    count: 0,
                    questions: [],
                },
            },
        });

        const component = render(
            <ThemeProvider theme={theme}>
                <PracticeTest />
            </ThemeProvider>
        );

        const testName = component.queryByText('Some test');
        expect(testName).toBeNull();
    });

    test('complete flow', () => {
        useTest.mockReturnValue({
            data: {
                data: {
                    name: 'Some test',
                    discipline: 'Discipline Name',
                },
            },
        });

        useTestQuestions.mockReturnValue({
            data: {
                data: {
                    count: 2,
                    questions: [
                        {
                            answer_type: { id: questionTypes.singleSelection, answer_type: 'Single-answer' },
                            content: 'How are you?',
                            difficulty: { id: 1, difficulty: 'Easy' },
                            id: 15,
                            possible_answers: ['Ok', 'Fine'],
                        },
                        {
                            answer_type: { id: questionTypes.multipleSelection, answer_type: 'Multiple-answers' },
                            content: 'How well are you?',
                            difficulty: { id: 1, difficulty: 'Easy' },
                            id: 15,
                            possible_answers: ['Ok', 'Great'],
                        },
                    ],
                },
            },
        });

        const testScoreFn = jest.fn();
        useTestScore.mockReturnValue({
            mutate: testScoreFn,
        });

        const component = render(
            <ThemeProvider theme={theme}>
                <PracticeTest />
            </ThemeProvider>
        );

        const testName = component.queryByText('Some test');
        expect(testName).toBeInTheDocument();

        expect(component.queryByText('How are you?')).toBeInTheDocument();

        expect(component.queryByText('One question remaining')).toBeInTheDocument();

        const nextButton = component.queryByRole('button', { name: 'Next' });
        expect(nextButton).toBeInTheDocument();
        expect(nextButton).toBeDisabled();

        ['Ok', 'Fine'].forEach(choice => {
            expect(component.queryByRole('radio', { name: choice })).toBeInTheDocument();
        });

        const okChoice = component.queryByRole('radio', { name: 'Ok' });
        fireEvent.click(okChoice);
        expect(nextButton).not.toBeDisabled();

        expect(component.queryByRole('radio', { name: 'Ok', checked: true })).toBeInTheDocument();
        fireEvent.click(nextButton);
        expect(component.queryByRole('button', { name: 'Next' })).toBeNull();

        const finishButton = component.queryByLabelText('submit-test', { selector: 'button' });
        expect(finishButton).toBeInTheDocument();
        expect(finishButton).toBeDisabled();

        expect(component.queryByText('How well are you?')).toBeInTheDocument();

        ['Ok', 'Great'].forEach(choice => {
            expect(component.queryByRole('checkbox', { name: choice })).toBeInTheDocument();
        });

        const greatChoice = component.queryByRole('checkbox', { name: 'Great' });
        fireEvent.click(greatChoice);

        fireEvent.click(finishButton);
        expect(testScoreFn).toHaveBeenCalled();
    });
});