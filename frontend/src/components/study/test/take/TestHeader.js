import React from 'react';
import PropTypes from 'prop-types';
import { useDirection } from '../../../layout/hooks';
import { Typography, useTheme } from '@mui/material';
import { Box } from '@mui/system';

export default function TestHeader({ leftText, rightText, bgcolor, textColor = null }) {
    const theme = useTheme();
    const direction = useDirection();

    return (
        <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            flexDirection={direction}
            borderRadius={theme.radius.general}
            bgcolor={bgcolor}
            width="100%"
            height={60}
        >
            <Typography fontWeight={600} pl={1.5} variant="body1" color={textColor} flex={1}>{leftText}</Typography>
            <Typography fontWeight={600} pr={1.5} variant="body1" color={textColor} >{rightText}</Typography>
        </Box >
    );
}

TestHeader.propTypes = {
    leftText: PropTypes.string.isRequired,
    rightText: PropTypes.string,
    bgcolor: PropTypes.string.isRequired,
    textColor: PropTypes.string,
}