import { Paper } from "@mui/material";
import { useTheme } from "@mui/styles";
import PropTypes from 'prop-types';
import { Box } from "@mui/system";
import { useMobile } from "../../layout/hooks";

function WithCard({ width, children, elevation, bgcolor }) {
    const isMobile = useMobile();
    const theme = useTheme();

    return (
        <Paper
            component="div"
            elevation={elevation ?? 4}
            sx={{ height: 'fit-content', width: isMobile ? '100%' : width ?? '25%', borderRadius: theme.radius.button, bgcolor: bgcolor ?? 'white' }}>
            <Box p={2}>
                {children}
            </Box>
        </Paper>
    );
}

WithCard.propTypes = {
    width: PropTypes.string,
    children: PropTypes.object,
    elevation: PropTypes.number,
    bgcolor: PropTypes.string,
};

export { WithCard };