import { render } from '@testing-library/react';
import "@testing-library/jest-dom/extend-expect";
import { fireEvent } from '@testing-library/react';

import Disciplines from './Disciplines';

import { withTheme } from '../../../app/testUtilities';

/* eslint-env jest */

describe('Disciplines component', () => {
    test('renders nothing', () => {
        const component = render(<Disciplines disciplines={[]} />);
        expect(component).not.toBeNull();
    });

    test('renders loading component', () => {
        const component = render(<Disciplines loading />);
        expect(component.queryByRole('progressbar')).toBeInTheDocument();
    });

    const disciplines = [
        {
            id: 1,
            name: 'Web development',
            description: 'Web development is great',
        },
        {
            id: 2,
            name: 'Desktop development',
            description: 'Desktop development is great',
        },
    ];

    test('renders disciplines', () => {
        const component = render(withTheme(<Disciplines disciplines={disciplines} selectedDiscipline={0} setSelectedDiscipline={jest.fn()} />));
        ['Web development', 'Web development is great', 'Desktop development', 'Desktop development is great'].forEach(text => {
            expect(component.queryByText(text)).toBeInTheDocument();
        });
    });

    test('highlights selected discipline', () => {
        const component = render(withTheme(<Disciplines disciplines={disciplines} selectedDiscipline={1} setSelectedDiscipline={jest.fn()} />));
        expect(component.queryByRole('option', { checked: true })).toBeInTheDocument();
    });

    test('calls setSelectedDiscipline', () => {
        const setSelectedDiscipline = jest.fn();
        const component = render(
            withTheme(
                <Disciplines
                    disciplines={disciplines}
                    selectedDiscipline={1}
                    setSelectedDiscipline={setSelectedDiscipline} />
            )
        );

        const uncheckedDiscipline = component.queryByRole('option', { checked: false });
        fireEvent.click(uncheckedDiscipline);

        expect(setSelectedDiscipline).toHaveBeenCalledTimes(1);
        expect(setSelectedDiscipline).toHaveBeenCalledWith({
            id: 2,
            name: 'Desktop development'
        });

        fireEvent.click(component.queryByRole('option', { checked: true }));
        expect(setSelectedDiscipline).toHaveBeenCalledWith(null);
    });
});