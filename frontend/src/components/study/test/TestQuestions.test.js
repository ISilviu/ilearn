import { render } from '@testing-library/react';
import "@testing-library/jest-dom/extend-expect";
import { fireEvent, act } from '@testing-library/react';
import TestQuestions from './TestQuestions';

import { useAnswerTypes, useDifficultiesChoices } from '../../../api/createTest';
import { withTheme } from '../../../app/testUtilities';

/* eslint-env jest */

jest.mock('../../../api/createTest');

beforeEach(() => {
    useDifficultiesChoices.mockReturnValue([
        {
            id: 1,
            text: 'Easy',
        },
        {
            id: 2,
            text: 'Medium',
        },
        {
            id: 3,
            text: 'Hard',
        },
        {
            id: 4,
            text: 'Expert',
        },
    ]);

    useAnswerTypes.mockReturnValue({
        data: {
            data: [
                {
                    id: 1,
                    answer_type: 'Single selection',
                },
                {
                    id: 2,
                    answer_type: 'Multiple selection',
                },
            ],
        },
    });
});

describe('Test Questions', () => {
    test('renders', () => {
        const component = render(<TestQuestions questions={[]} />);
        expect(component).not.toBeNull();
    });

    test('renders empty text', () => {
        const component = render(<TestQuestions questions={[]} />);
        expect(component.queryByText("It's pretty empty out here, try adding some questions.")).toBeInTheDocument();
    });

    test('renders questions forms', () => {
        const component = render(withTheme(<TestQuestions questions={[undefined, undefined]} isDeleted={() => false} />));
        const queryResult = component.queryAllByRole('radiogroup');
        queryResult.forEach(radioGroup => expect(radioGroup).toBeInTheDocument());
        expect(queryResult.length).toBe(2);

        expect(component.queryAllByTestId('DeleteOutlineIcon').length).toBe(2);
        expect(component.queryAllByTestId('DoneIcon').length).toBe(2);
    });

    test('does not render deleted questions', () => {
        const component = render(withTheme(<TestQuestions questions={[undefined, undefined]} isDeleted={() => true} />));
        expect(component.queryByRole('radiogroup')).not.toBeInTheDocument();
    });

    test('deleted questions are skipped', () => {
        const component = render(withTheme(<TestQuestions questions={[undefined, undefined]} isDeleted={(index) => index === 0 ? true : false} />));
        expect(component.queryByRole('radiogroup')).toBeInTheDocument();
    });

    const questions = [
        {
            content: 'How are you?',
            difficulty: 1,
            answers: ['A'],
            possible_answers: ['A', 'B'],
            answerType: 1,
        },
    ];

    test('renders default values of questions', () => {
        const component = render(withTheme(<TestQuestions questions={questions} isDeleted={() => false} />));
        expect(component.queryByText('How are you?')).toBeInTheDocument();

        const radioGroup = component.queryByRole('radiogroup');
        expect(radioGroup).toBeInTheDocument();

        expect(component.queryByDisplayValue('A')).toBeChecked();
        const secondOption = component.queryByDisplayValue('B');
        expect(secondOption).toBeInTheDocument();
        expect(secondOption).not.toBeChecked();

        expect(component.queryByRole('button', { name: 'Easy' })).toBeInTheDocument();
        expect(component.queryByRole('button', { name: 'Single selection' })).toBeInTheDocument();
    });

    // failing
    // test('onSubmit', () => {
    //     const updateQuestion = jest.fn();
    //     const component = render(withTheme(<TestQuestions questions={questions} isDeleted={() => false} updateQuestion={updateQuestion} />));

    //     const doneIconButton = component.queryByTestId('DoneIcon');
    //     act(() => {
    //         fireEvent.click(doneIconButton);
    //     });
    //     expect(updateQuestion).toHaveBeenCalled();
    // });

    test('onDelete is called', () => {
        const deleteQuestion = jest.fn();
        const component = render(withTheme(<TestQuestions questions={[undefined]} isDeleted={() => false} addDeletedQuestion={deleteQuestion} />));

        const deleteIconButton = component.queryByTestId('DeleteOutlineIcon');
        act(() => {
            fireEvent.click(deleteIconButton);
        });
        expect(deleteQuestion).toHaveBeenCalledTimes(1);
    });

});