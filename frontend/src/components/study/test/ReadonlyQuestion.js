import React from 'react';
import PropTypes from 'prop-types';
import { Stack, Box, Typography, FormControlLabel, Radio, FormGroup, Checkbox, Tooltip, SvgIcon } from '@mui/material';
import { useDifficultiesChoices } from '../../../api/createTest';
import { questionTypes, questionDifficulties } from './question';

import CheckBoxIcon from '@mui/icons-material/CheckBox';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';

import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';

import Battery20Icon from '@mui/icons-material/Battery20';
import Battery50Icon from '@mui/icons-material/Battery50';
import Battery80Icon from '@mui/icons-material/Battery80';
import BatteryChargingFullIcon from '@mui/icons-material/BatteryChargingFull';

export default function ReadonlyQuestion({
    index, question, difficulty, answers,
    possibleAnswers, questionType, getColorPalette, isChecked, showTest, status
}) {

    const difficultiesChoices = useDifficultiesChoices();
    const renderControl = React.useCallback((questionType, color) => {
        const props = { sx: { color } };
        switch (questionType) {
            case questionTypes.singleSelection:
                return (
                    <Radio
                        icon={<RadioButtonUncheckedIcon {...props} />}
                        checkedIcon={<RadioButtonCheckedIcon {...props} />}
                    />
                );
            case questionTypes.multipleSelection:
                return (
                    <Checkbox
                        icon={<CheckBoxOutlineBlankIcon {...props} />}
                        checkedIcon={<CheckBoxIcon {...props} />}
                    />
                );
            default:
                return null;
        }
    },
        []
    );

    const renderChoices = React.useCallback(() => {
        switch (questionType) {
            case questionTypes.singleSelection:
            case questionTypes.multipleSelection:
                return possibleAnswers.map((answer, currentIndex) => {
                    const color = getColorPalette(answer, index);
                    const checked = isChecked ? answers.includes(answer) : false;
                    return (
                        <FormControlLabel
                            key={currentIndex}
                            checked={checked}
                            value={answer}
                            control={renderControl(questionType, color)}
                            label={<Typography color={color}>{answer}</Typography>}
                            disabled
                        />
                    );
                })
            case questionTypes.code:
                return (
                    <Stack>
                        {
                            showTest ? (
                                <Typography>{`solution(${possibleAnswers.join(',')}) = ${answers}`}</Typography>
                            ) : (
                                <Typography>{status}</Typography>
                            )
                        }
                    </Stack>
                );
            default:
                return null;
        }
    },
        [getColorPalette, questionType, renderControl, isChecked, answers, index, possibleAnswers, showTest, status]
    );

    const getDifficultyIcon = React.useCallback(difficulty => {
        switch (difficulty) {
            case questionDifficulties.novice: return Battery20Icon;
            case questionDifficulties.intermediate: return Battery50Icon;
            case questionDifficulties.advanced: return Battery80Icon;
            case questionDifficulties.expert: return BatteryChargingFullIcon;
            default: return null;
        };
    }, []);

    const difficultyIcon = getDifficultyIcon(difficulty);

    return (
        <Stack width="100%" spacing={1}>
            <Box display="flex" alignItems="center">
                <Typography flexGrow={1} sx={{ wordBreak: 'break-all' }} variant="h6">{question}</Typography>
                <Tooltip title={`${difficultiesChoices.find(d => d.id === difficulty)?.text} question`}>
                    <SvgIcon component={difficultyIcon} color="primary" sx={{ fontSize: 28 }} />
                </Tooltip>
            </Box>
            <FormGroup aria-label="answers">
                {renderChoices()}
            </FormGroup>
        </Stack>
    );
}

ReadonlyQuestion.propTypes = {
    index: PropTypes.number,
    question: PropTypes.string,
    difficulty: PropTypes.number,
    answers: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    possibleAnswers: PropTypes.array,
    questionType: PropTypes.number,
    getColorPalette: PropTypes.func,
    isChecked: PropTypes.bool,
    showTest: PropTypes.bool,
    status: PropTypes.string,
};
