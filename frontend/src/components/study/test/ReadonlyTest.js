import React from 'react';
import PropTypes from 'prop-types';
import {
    Link as MuiLink, Box, Divider, IconButton, Stack, SvgIcon, Tooltip, Typography, Dialog
} from "@mui/material";
import QuizIcon from '@mui/icons-material/Quiz';
import SchoolIcon from '@mui/icons-material/School';
import ImportExportIcon from '@mui/icons-material/ImportExport';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import { useToggle } from 'react-use';
import { Link } from 'react-router-dom';
import paths from '../../../app/paths';
import DialogTopBar from '../../layout/DialogTopBar';
import ExportTest from './ExportTest';

export default function ReadonlyTest(props) {
    const { testId, testName, discipline, questionsCount } = props;
    const [optionsExpanded, toggleOptions] = useToggle(false);
    const [exportDialogOpen, toggleExportDialog] = useToggle(false);
    const options = React.useMemo(() => [
        {
            title: 'Export',
            icon: ImportExportIcon,
            color: 'text',
            onClick: toggleExportDialog
        },
        {
            title: 'Practice',
            icon: CheckCircleOutlineIcon,
            color: 'primary',
            to: questionsCount ? paths.pages.practiceTest.get(testId) : undefined,
        },
    ], [testId, toggleExportDialog, questionsCount]);

    const exportDialog = exportDialogOpen ? (
        <Dialog open={exportDialogOpen} fullScreen onClose={toggleExportDialog}>
            <Stack height="100%">
                <DialogTopBar title="Export test" onClose={toggleExportDialog} />
                <Box mt={10.5} height="100%">
                    <ExportTest testDetails={props} />
                </Box>
            </Stack>
        </Dialog>
    ) : null;

    return (
        <Stack width="100%" spacing={1.2} justifyContent="center" alignItems="center">
            <Box width="100%" display="flex" alignItems="center">
                <Stack width="calc(100% - 24px)" spacing={1} alignItems="center" flexGrow={1}>
                    <Tooltip title={testName}>
                        <Typography width="100%" align='center' noWrap variant="h6">{testName}</Typography>
                    </Tooltip>
                    <Box display="flex" alignItems="center" gap={4}>
                        <Box>
                            <Tooltip title={discipline.name}>
                                <SvgIcon component={SchoolIcon} color="primary" sx={{ fontSize: 25 }} />
                            </Tooltip>
                        </Box>
                        <Box display="flex" alignItems="center" gap={2}>
                            <SvgIcon component={QuizIcon} color="primary" sx={{ fontSize: 25 }} />
                            <Stack>
                                <Typography display="flex" justifyContent="center">{`${questionsCount}`}</Typography>
                                <Typography>questions</Typography>
                            </Stack>
                        </Box>
                    </Box>
                </Stack>
                <IconButton sx={{ maxHeight: 35, maxWidth: 35 }} onClick={toggleOptions}>
                    <SvgIcon component={optionsExpanded ? ExpandLessIcon : ExpandMoreIcon} color="text" />
                </IconButton>
            </Box>
            {optionsExpanded ? (
                <>
                    <Divider sx={{ width: '100%' }} />
                    <Box display="flex" columnGap={3}>
                        {
                            options.map((option, index) => (
                                <Stack key={index} justifyContent="center" alignItems="center">
                                    {option.to ? (
                                        <MuiLink component={Link} to={option.to} underline='none'>
                                            <IconButton disabled={!questionsCount} onClick={option.onClick}>
                                                <SvgIcon color={questionsCount ? option.color : 'disabled'} component={option.icon} />
                                            </IconButton>
                                        </MuiLink>
                                    ) : (
                                        <IconButton disabled={!questionsCount} onClick={option.onClick}>
                                            <SvgIcon component={option.icon} color={questionsCount ? option.color : 'disabled'} />
                                        </IconButton>
                                    )}
                                    <Typography color={questionsCount ? 'text' : 'disabled'} variant="body2">{option.title}</Typography>
                                </Stack>
                            ))
                        }
                        <Stack justifyContent="center" alignItems="center">
                            <MuiLink component={Link} to={paths.pages.takeTest.get(testId)} underline="none" target="_blank">
                                <IconButton disabled={!questionsCount}>
                                    <SvgIcon component={ShareIcon} color={questionsCount === false ? 'disabled' : 'bleu'} />
                                </IconButton>
                            </MuiLink>
                            <Typography variant="body2">Share</Typography>
                        </Stack>
                    </Box>
                </>
            ) : null}
            {exportDialog}
        </Stack>
    );
}

ReadonlyTest.propTypes = {
    testId: PropTypes.number,
    testName: PropTypes.string,
    discipline: PropTypes.object,
    questionsCount: PropTypes.number,
};