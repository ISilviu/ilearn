const questionTypes = {
    singleSelection: 1,
    multipleSelection: 2,
    code: 3,
};

const questionDifficulties = {
    novice: 1,
    intermediate: 2,
    advanced: 3,
    expert: 4,
};

export { questionTypes, questionDifficulties };