import React from 'react';
import PropTypes from 'prop-types';

import { Box, Grid, Paper, Stack, Typography } from '@mui/material';

import { useTheme } from '@mui/styles';
import Scrollbars from 'react-custom-scrollbars-2';
import { useDesktop, useMobile } from '../../layout/hooks';
import { LoadingIndicator } from '../../misc/Fields';
import { WithCard } from '../test/questionCard';
import ReadonlyQuestion from '../test/ReadonlyQuestion';
import { useWindowSize } from 'react-use';
import Filters from '../../generic/Filters';
import { useSelectedItems, useServerSideProcessing } from '../../generic/hooks';
import { size } from 'lodash';
import questionOrdering from '../../../filters/questionOrdering';
import Toolbar from '../../generic/Toolbar';
import { useQuestionsFilterOptions } from '../../generic/filtersOptions';

export default function Questions({ questionsApi, onSubmit = () => { } }) {
    const theme = useTheme();
    const isMobile = useMobile();

    const [
        queryParams,
        { setFilters, setOrdering, changeSearch, search }
    ] = useServerSideProcessing();

    const [orderings, defaultOrdering] = questionOrdering.useOrderings();

    const questions = questionsApi.useQuestions(queryParams);
    const [selectedQuestions, { addQuestion, removeQuestion, isSelected }] = useSelectedItems();

    const isDesktop = useDesktop();
    const { height } = useWindowSize();

    const questionsData = React.useMemo(() => {
        let questionsData = [];
        if (questions.data && questions.data.data) {
            questionsData = questions.data.data;
        }
        return questionsData;
    }, [questions.data]);

    const getColorPalette = React.useCallback(
        () => theme.palette.text.main,
        [theme.palette.text.main]
    );

    const onQuestionClick = React.useCallback((questionId, question) => {
        if (isSelected(questionId)) {
            removeQuestion(questionId);
        } else {
            addQuestion(questionId, question);
        }
    }, [addQuestion, isSelected, removeQuestion]);

    const renderReadonlyQuestion = question => (
        <Box
            key={question.id}
            onClick={() => onQuestionClick(question.id, question)}
            border={isSelected(question.id) ? `2.5px solid ${theme.palette.primary.main}` : null}
            borderRadius={theme.radius.button}
        >
            <WithCard width="100%" elevation={2}>
                <ReadonlyQuestion
                    question={question.content}
                    difficulty={question.difficulty}
                    answers={question.answers}
                    possibleAnswers={question.possible_answers}
                    questionType={question.answer_type}
                    getColorPalette={getColorPalette}
                    isChecked
                    showTest
                />
            </WithCard>
        </Box>
    );

    if (questions.isError) {
        return (
            <Box
                display="flex"
                justifyContent="center"
                alignItems="center"
                height="100%"
            >
                <Typography paragraph>
                    Error, the data could not be loaded. Reason: {questions.error} :(
                </Typography>
            </Box>
        );
    }

    return (
        <Box display="flex" flexGrow={1}>
            {isDesktop ? (
                <Box width={350} overflow="auto" height={height}>
                    <Scrollbars autoHide>
                        <Filters useFiltersOptions={useQuestionsFilterOptions} onFiltersChange={setFilters} />
                    </Scrollbars>
                </Box>
            ) : null}
            <Stack width="100%" py={1.5}>
                <Paper sx={{ py: 2.5 }}>
                    <Toolbar
                        searchProps={{ search: search, onChange: changeSearch }}
                        orderingProps={{
                            values: orderings,
                            defaultValue: defaultOrdering,
                            onOrderingChanged: setOrdering,
                            getDisplayText: questionOrdering.getDisplayText
                        }}
                        selectedItemsBadgeProps={{
                            onClick: () => onSubmit(selectedQuestions),
                            badgeContent: size(selectedQuestions),
                        }}
                        filtersProps={{
                            useFiltersOptions: useQuestionsFilterOptions,
                            onFiltersChange: setFilters,
                        }}
                    />
                </Paper>
                {questions.isLoading ? <LoadingIndicator /> : (
                    <Paper sx={{ mt: 1 }}>
                        {
                            isMobile ? (
                                <Stack overflow="auto" spacing={2}>
                                    {questionsData.map(question => question ? renderReadonlyQuestion(question) : null)}
                                </Stack>
                            ) : (
                                <Grid container spacing={4} overflow="auto" p={2} height={height}>
                                    {questionsData.map(question => question ? (
                                        <Grid item xs={12} md={6} lg={4} key={question.id}>
                                            {renderReadonlyQuestion(question)}
                                        </Grid>
                                    ) : null)}
                                </Grid>
                            )
                        }
                    </Paper>
                )}
            </Stack>
        </Box >
    );
}

Questions.propTypes = {
    questionsApi: PropTypes.shape({
        useQuestions: PropTypes.func.isRequired,
    }),
    onSubmit: PropTypes.func,
}