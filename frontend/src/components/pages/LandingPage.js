import React from 'react';
import PropTypes from 'prop-types';

import ExtensionIcon from '@mui/icons-material/Extension';
import ImportExportIcon from '@mui/icons-material/ImportExport';
import PersonIcon from '@mui/icons-material/Person';

import {
    Box, Button, Stack, Typography, useTheme, Link as MuiLink, Card, SvgIcon
} from '@mui/material';
import { Link } from 'react-router-dom';
import paths from '../../app/paths';
import { useDirection, useMobile } from '../layout/hooks';
import Scrollbars from 'react-custom-scrollbars-2';

function PresentationCard({ title, description, icon, paletteText = 'primary' }) {
    const theme = useTheme();
    return (
        <Card
            raised
            sx={{
                p: 1.5,
                display: 'flex',
                flexDirection: 'column',
                bgcolor: theme.palette[paletteText].main,
                borderRadius: theme.radius.general,
                width: 250,
            }}
        >
            <Box display="flex">
                <SvgIcon component={icon} sx={{ color: theme.palette[paletteText].contrastText, fontSize: 40 }} />
                <Typography alignSelf="center" fontFamily="Fredoka" justifyContent="center" px={1} color={theme.palette[paletteText].contrastText}>{title}</Typography>
            </Box>
            <Typography color={theme.palette[paletteText].contrastText} fontFamily="Fredoka">
                {description}
            </Typography>
        </Card>
    );
}

PresentationCard.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    paletteText: PropTypes.string,
    icon: PropTypes.any,
};

export default function LandingPage() {
    const theme = useTheme();

    const direction = useDirection();
    const isMobile = useMobile();

    const features = React.useMemo(() => [
        {
            title: 'Reuse questions',
            description: 'The platform has powerful tooling to enable question reusability.',
            icon: ExtensionIcon,
        },
        {
            title: 'Export tools',
            description: 'Tests can be exported as PDF or JSON.',
            paletteText: 'secondary',
            icon: ImportExportIcon,
        },
        {
            title: 'Test yourself',
            description: 'The application has a bult-in test taking solution.',
            icon: PersonIcon,
        },
    ], []);

    return (
        <Scrollbars>
            <Stack px={isMobile ? 1 : 0} alignItems="center" justifyContent={isMobile ? undefined : "center"} height="100%" spacing={2}>
                <Box display="flex" flexDirection={direction} gap={1}>
                    <Typography variant="h1" fontFamily="Fredoka" fontWeight={500} >Quizes, </Typography>
                    <Typography variant="h1" fontFamily="Fredoka" fontWeight={500} color="primary" > but better.</Typography>
                </Box>
                <Typography variant="h6" fontFamily="Fredoka" fontWeight={400}>Creating them has never been easier.</Typography>
                <Box pt={3}>
                    <MuiLink component={Link} to={paths.pages.createTest} underline='none'>
                        <Button variant="contained" sx={{ fontSize: theme.spacing(3) }}>
                            <Typography variant="h6" fontFamily="Fredoka" px={2} fontWeight={400} >Your first quiz</Typography>
                        </Button>
                    </MuiLink>
                </Box>
                <Box display="flex" pt={10} gap={3} flexDirection={direction}>
                    {features.map((feature, index) => <PresentationCard key={index} {...feature} />)}
                </Box>
            </Stack>
        </Scrollbars>
    );
}
