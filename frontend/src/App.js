import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { theme } from './theme';
import { QueryClientProvider } from 'react-query';
import queryClient from './api/queryClient';
import ILearnAppBar from './components/layout/ILearnAppBar';
import { Box } from '@mui/system';
import { Stack, Toolbar } from '@mui/material';
import Routes from './components/routing/Routes';
import { QueryParamProvider } from 'use-query-params';
import { Route } from 'react-router-dom';

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        <QueryParamProvider ReactRouterRoute={Route}>
          <Stack height="100vh">
            <Box flexShrink={1}>
              <ILearnAppBar />
              <Toolbar />
            </Box>
            <Box flexGrow={1} overflow="auto">
              <Routes />
            </Box>
          </Stack>
        </QueryParamProvider>
      </QueryClientProvider>
    </ThemeProvider>
  );
}