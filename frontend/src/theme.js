import { createTheme } from '@mui/material/styles';

const theme = createTheme({
    colors: {
        appBar: '#232323',
        selectedCard: '#39CD5A',
        draggingCard: '#e0e0e0',
        disabled: {
            bgcolor: '#d4d4d4'
        },
        notSaved: {
            bgcolor: '#d4d4d4'
        },
        test: {
            score: {
                best: '#00D455',
                medium: '#e3c21e',
                low: '#F90000',
            },
            correctAnswers: '#00D455',
            wrongAnswers: '#F90000',
        },
        testHeader: {
            inProgress: '#C4C4C4',
            finished: '#00D455',
            halted: 'black',
        },
        testCardBackground: '#F2F2F2',
    },
    radius: {
        button: '7px',
        general: '7px',
        boxText: '7.5px',
    },
    components: {
        MuiButton: {
            styleOverrides: {
                contained: {
                    borderRadius: '7px',
                }
            }
        }
    },
    typography: {
        button: {
            textTransform: 'none'
        },
        fontFamily: [
            'Inter',
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
          ].join(',')
    },
    palette: {
        primary: {
            main: '#495AF3',
        },
        text: {
            main: 'black',
        },
        bleu: {
            main: '#639cd9',
            contrastText: '#ffffff',
        },
        secondary: {
            main: '#ffffff',
        },
    }
});

export { theme };