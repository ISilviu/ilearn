import React from 'react';
import { useDisciplines } from '../api/createTest';

const disciplineFilterId = 'discipline';
const useDisciplinesFilter = () => {
    const disciplines = useDisciplines(null,
        data => data.data.map(discipline => ({
            id: discipline.id,
            name: discipline.name,
        })),
    );

    return React.useMemo(() => ({
        id: disciplineFilterId,
        isLoading: disciplines.isLoading,
        data: disciplines.data,
        title: 'Discipline',
        expanded: true,
    }),
        [disciplines]
    );
};

const getDisciplinesQueryParams = (filters) => {
    const queryParams = {};
    const selectedDisciplinesIds = filters.filter(f => f.id === disciplineFilterId).map(f => f.choice.id);
    if (selectedDisciplinesIds.length) {
        queryParams[disciplineFilterId] = selectedDisciplinesIds;
    }
    return queryParams;
};

const getFiltersFromQueryParams = queryParams => {
    const disciplineFilters = queryParams.filter(param => param.startsWith(disciplineFilterId));
    return disciplineFilters.map(filter => ({
        id: disciplineFilterId,
        checked: true,
        choice: { id: parseInt(filter.slice(disciplineFilterId.length + 1)) },
    }));
};

const disciplineFilter = {
    id: disciplineFilterId,
    useFilter: useDisciplinesFilter,
    getQueryParams: getDisciplinesQueryParams,
    getFiltersFromQueryParams,
};

export default disciplineFilter;