import React from 'react';
import { useQuery } from "react-query";
import { cacheTime, staleTime } from "../api/constants";

const useDisciplineOrdering = () => {
    const orderings = useQuery({
        queryKey: [`/disciplines/orderings/`],
        staleTime: staleTime,
        cacheTime: cacheTime,
        select: data => {
            return {
                data: {
                    values: data.data,
                    defaultValue: data.data[0] ?? null,
                },
            };
        }
    });

    return React.useMemo(
        () => orderings.isLoading ?
            [[], ''] : [orderings.data.data.values, orderings.data.data.defaultValue],
        [orderings.isLoading, orderings.data]
    );
};

const getDisplayText = ordering => {
    const orderingToDisplayText = {
        name: 'Name',
        description: 'Description',
    };
    return ordering in orderingToDisplayText ? orderingToDisplayText[ordering] : '';
};

const disciplineOrdering = {
    useOrderings: useDisciplineOrdering,
    getDisplayText,
};

export default disciplineOrdering;