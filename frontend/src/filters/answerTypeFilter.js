import React from 'react';
import { useAnswerTypes } from "../api/createTest";

const answerTypeFilterId = 'answer_type';
const useAnswerTypesFilter = () => {
    const answerTypes = useAnswerTypes(
        data => data.data.map(answerType => ({
            id: answerType.id,
            name: answerType.answer_type,
        })),
    );

    return React.useMemo(() => ({
        id: answerTypeFilterId,
        isLoading: answerTypes.isLoading,
        data: answerTypes.data,
        title: 'Answer type',
        expanded: true
    }),
        [answerTypes]
    );
};

const getAnswerTypesQueryParams = (filters) => {
    const queryParams = {};
    const answerTypesIds = filters.filter(f => f.id === answerTypeFilterId).map(f => f.choice.id);
    if (answerTypesIds.length) {
        queryParams[answerTypeFilterId] = answerTypesIds;
    }
    return queryParams;
};

const getFiltersFromQueryParams = queryParams => {
    const difficultyFilters = queryParams.filter(param => param.startsWith(answerTypeFilterId));
    return difficultyFilters.map(filter => ({
        id: answerTypeFilterId,
        checked: true,
        choice: { id: parseInt(filter.slice(answerTypeFilterId.length + 1)) },
    }));
};

const answerTypeFilter = {
    id: answerTypeFilterId,
    useFilter: useAnswerTypesFilter,
    getQueryParams: getAnswerTypesQueryParams,
    getFiltersFromQueryParams,
};

export default answerTypeFilter;