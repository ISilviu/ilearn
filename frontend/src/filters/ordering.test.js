import { renderHook } from "@testing-library/react-hooks";
import { act } from "react-dom/test-utils";
import useOrdering from './ordering';

/* eslint-env jest */

describe('useOrdering', () => {
    test('returns expected results', () => {
        const orderingResult = renderHook(() => useOrdering()).result;
        const [ordering, , direction, ,] = orderingResult.current;

        expect(ordering).toBeNull();
        expect(direction).toBe(true);
    });

    test('changes direction properly', () => {
        const orderingResult = renderHook(() => useOrdering()).result;
        const [,setOrdering , , toggleDirection,] = orderingResult.current;

        act(() => toggleDirection());
        expect(orderingResult.current[2]).toBe(false);

        expect(orderingResult.current[4]()).toStrictEqual(expect.objectContaining({
            ordering: null,
        }));

        act(() => setOrdering('abc'));
        expect(orderingResult.current[4]()).toStrictEqual(expect.objectContaining({
            ordering: '-abc',
        }));
    });

    test('setOrdering changes states', () => {
        const orderingResult = renderHook(() => useOrdering()).result;
        const [ordering, setOrdering, , ,] = orderingResult.current;

        expect(ordering).toBeNull();
        act(() => setOrdering('Some value'));
        expect(orderingResult.current[0]).toBe('Some value');

        expect(orderingResult.current[4]()).toStrictEqual(expect.objectContaining({
            ordering: 'Some value',
        }));
    });
});