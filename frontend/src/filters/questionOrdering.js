import React from 'react';
import { get } from 'lodash';
import { useQuery } from "react-query";
import { cacheTime, staleTime } from "../api/constants";

const useQuestionOrdering = () => {
    const orderings = useQuery({
        queryKey: [`/questions/orderings/`],
        staleTime: staleTime,
        cacheTime: cacheTime,
        select: data => {
            return {
                data: {
                    values: data.data,
                    defaultValue: data.data[0] ?? null,
                },
            };
        }
    });

    return React.useMemo(
        () => orderings.isLoading ?
            [[], ''] : [orderings.data.data.values, orderings.data.data.defaultValue],
        [orderings.isLoading, orderings.data]
    );
};

const getDisplayText = ordering => {
    const orderingToDisplayText = {
        content: 'Content',
        difficulty: 'Difficulty',
        answer_type: 'Answer Type',
    };
    return get(orderingToDisplayText, ordering, '');
};

const questionOrdering = {
    useOrderings: useQuestionOrdering,
    getDisplayText,
};

export default questionOrdering;