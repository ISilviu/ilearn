import React from 'react';
import { useDifficulties } from "../api/question";

const difficultyFilterId = 'difficulty';
const useDifficultiesFilter = () => {
    const difficulties = useDifficulties(
        data => data.data.map(difficulty => ({
            id: difficulty.id,
            name: difficulty.difficulty,
        })),
    );

    return React.useMemo(() => ({
        id: difficultyFilterId,
        isLoading: difficulties.isLoading,
        data: difficulties.data,
        title: 'Difficulty',
        expanded: true,
    }),
        [difficulties]
    );
};

const getDifficultiesQueryParams = (filters) => {
    const queryParams = {};
    const selectedDifficultiesIds = filters.filter(f => f.id === difficultyFilterId).map(f => f.choice.id);
    if (selectedDifficultiesIds.length) {
        queryParams[difficultyFilterId] = selectedDifficultiesIds;
    }
    return queryParams;
};

const getFiltersFromQueryParams = queryParams => {
    const difficultyFilters = queryParams.filter(param => param.startsWith(difficultyFilterId));
    return difficultyFilters.map(filter => ({
        id: difficultyFilterId,
        checked: true,
        choice: { id: parseInt(filter.slice(difficultyFilterId.length + 1)) },
    }));
};

const difficultyFilter = {
    id: difficultyFilterId,
    useFilter: useDifficultiesFilter,
    getQueryParams: getDifficultiesQueryParams,
    getFiltersFromQueryParams,
};

export default difficultyFilter;