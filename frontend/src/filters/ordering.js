import React from 'react';
import { useToggle } from 'react-use';

const useOrdering = (defaultOrdering = null, defaultDirection = true) => {
    const [ordering, setOrdering] = React.useState(defaultOrdering);
    const [direction, toggleDirection] = useToggle(defaultDirection);

    const getQueryParams = React.useCallback(
        () => ({ ordering: ordering ? `${direction ? '' : '-'}${ordering}` : null }),
        [direction, ordering]
    );

    return [ordering, setOrdering, direction, toggleDirection, getQueryParams];
};

export default useOrdering;