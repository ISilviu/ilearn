import { get } from 'lodash';
import React from 'react';
import { useQuery } from "react-query";
import { cacheTime, staleTime } from "../api/constants";

const useTestOrdering = () => {
    const orderings = useQuery({
        queryKey: [`/tests/orderings/`],
        staleTime: staleTime,
        cacheTime: cacheTime,
        select: data => {
            return {
                data: {
                    values: data.data,
                    defaultValue: data.data[0] ?? null,
                },
            };
        }
    });

    return React.useMemo(
        () => orderings.isLoading ?
            [[], ''] : [orderings.data.data.values, orderings.data.data.defaultValue],
        [orderings.isLoading, orderings.data]
    );
};

const getDisplayText = ordering => {
    const orderingToDisplayText = {
        name: 'Test name',
        questions_count: 'Questions count',
        created_at: 'Date created',
    };
    return get(orderingToDisplayText, ordering, '');
};

const testOrdering = {
    useOrderings: useTestOrdering,
    getDisplayText,
};

export default testOrdering;