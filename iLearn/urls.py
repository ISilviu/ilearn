from django import urls
from django.urls import include, path

urlpatterns = [
    path('api/', include('apps.api.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('__debug__', include('debug_toolbar.urls')),
]
