# Master's thesis	

## Requirements

## Tech stack

- Frontend: 
	- React
	- React Material
- Backend:
	- DRF

## DB Schema
https://dbdiagram.io/d/610526c82ecb310fc3b7d10b

## Python virtual environments
https://www.freecodecamp.org/news/python-virtual-environments-explained-with-examples/

## Used links

- https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04
- https://stackoverflow.com/questions/37153322/postgres-user-cant-create-database-when-running-django-tests
- https://tecadmin.net/how-to-install-pgadmin4-on-ubuntu-20-04/

For the above, when facing the error, just grant db creation rights to the user defined in `settings.py`.
`ALTER USER ilearn_user CREATEDB;`
